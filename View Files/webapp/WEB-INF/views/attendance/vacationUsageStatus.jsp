<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/noticeStyle.css">
<title>Opera GroupWare</title>
<style type="text/css">
	#writeBtn:hover{
		cursor: pointer;
	}
	td:hover{
		cursor: pointer;
	}
	.pagingBtn:hover{
		cursor: pointer;
	}
	.pagingSide:hover{
		cursor: pointer;
	}
	#searchBtn:hover{
		cursor:pointer;
	}
</style>
</head>
<body>
   <jsp:include page="../common/operaNav.jsp"/>   
   <jsp:include page="../common/attendAside.jsp"/>   
   <section >
      <div id="mainDiv" style="width:950px;">
         <div id="titleDiv">
            <a id="titleName" style="color: #57585E;">휴가사용현황</a>
         </div>
         <hr style="margin-top: 1%; margin-bottom: 1%;">
         <div style="border: 0.5px solid white; margin: 10px; width: 950px; height: 35px; background: white;">
            <!-- <a style="float:left; padding: 10px; font-weight: 500; margin-left: 5px;">출근통계</a>
            <div style="float:left; margin-left: 4px; margin-right: 5px; border: 2px solid #C4C4C4; width:0.05px; height: 32px; "></div> -->
            <div style="float:left; padding: 8px;">
               <select style="background: white; color: gray; font-size: 15px; width: 60px;">
                  <option>2020</option>
                  <option>2019</option>
                  <option>2018</option>
                  <option>2017</option>
                  <option>2016</option>
               </select>
               년도 휴가정보
            </div>
            
            
            <!-- <div style="float:left; padding: 5px; text-align:center; font-size: 14px; margin-left: 10px; margin-top: 6px; width: 55px; height: 15px; background: #27334C; color: white; "
            id="searchBtn">
               검색
            </div> -->
            <br>
            <div style="width: 1150px; clear: both; text-align: center; ">
               <table id="listTable" style="margin-left:0; border-top: 1px solid gray;">
                 <tr class="head" style="background-color:#E4E4E4; color:#57585E; ">
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray;" rowspan="2">입사일</th>
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E; " rowspan="2">2017년 06월 09일</th>
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray;" >연차휴가일수</th>
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;" >1</th>
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray;" >휴가잔여일</th>
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;" >1일 0시간</th>
                  </tr>
                  <tr class="head" style="background-color:#E4E4E4; color:#57585E;">
                    
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray;">대체휴가일수</th>
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;">0일 0시간</th>
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray;">조퇴차감여부</th>
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;">사용</th>
                  </tr>
                  <tr class="head" style="background-color:#E4E4E4; color:#57585E;">
                    
                     <th width="165px;" style="border: 1px; border-style:solid;  border-color: gray; ">입사년차</th>
                     
                     <th width="165px;" 
                     	style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;" colspan="5">4</th>
                  </tr>
                  <!-- <tr class="row">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">유상무</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">인사팀</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">12</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">3</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0</td>
                  </tr> -->
               </table>
               <br>
               <br>
               <div style="float:left; padding: 8px;">
               휴가사용일수
            </div>  
            <br>
            <br>
               <table id="listTable" style="margin-left:0; border-top: 1px solid gray;">
                 <tr class="head" style="background-color:#E4E4E4; color:#57585E; ">
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;" colspan="2">연차</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E; " colspan="2">0일 0시간</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;" colspan="2">병가</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;" colspan="2">0일 0시간</th>
                  </tr>
                  <tr class="head" style="background-color:#E4E4E4; color:#57585E;">
                    
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;"colspan="2">조퇴</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;"colspan="2">0일 0시간</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;"colspan="2">출산휴가</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;"colspan="2">0일 0시간</th>
                  </tr>
                  <tr class="head" style="background-color:#E4E4E4; color:#57585E;">
                    
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;"colspan="2">반차</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;"colspan="2">0일 0시간</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;"colspan="2">경조사휴가</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;"colspan="2">0일 0시간</th>
                  </tr>
                  <tr class="head" style="background-color:#E4E4E4; color:#57585E;">
                    
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;"colspan="2">대체휴가</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;"colspan="2">0일 0시간</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;"colspan="2">육아휴직</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray; background: white; color:#57585E;"colspan="2">0일 0시간</th>
                  </tr>  
                     
               </table>   
               <br><br>
               <div style="float:left; padding: 8px;">
               휴가사용일수
            </div>
            <br>
            <br>
            <table id="listTable" style="margin-left:0; border-top: 1px solid gray;">
               <tr class="head" style="background-color:#E4E4E4; color:#57585E; ">
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;" colspan="2">휴가분류</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;" colspan="2">소속</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;" colspan="2">직위</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;" colspan="2">시작일</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;" colspan="2">종료일</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;" colspan="2">사용일수</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;" colspan="2">상태</th>
                     <th width="287px;" style="border: 1px; border-style:solid;  border-color: gray;" colspan="2">신청일자</th>
                  </tr>
            </table>     
         </div>
      </div>
      </div>
   </section>
   <script>
      function openAside(){

      }
      $("#listTable tr").click(function(){
         location.href="knowledgeDetail.me";
      })
   </script>
</body>
</html>