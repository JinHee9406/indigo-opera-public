<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/noticeStyle.css">
<title>Opera GroupWare</title>
<style type="text/css">
	#writeBtn:hover{
		cursor: pointer;
	}
	td:hover{
		cursor: pointer;
	}
	.pagingBtn:hover{
		cursor: pointer;
	}
	.pagingSide:hover{
		cursor: pointer;
	}
	#searchBtn:hover{
		cursor:pointer;
	}
</style>
</head>
<body>
   <jsp:include page="../common/operaNav.jsp"/>   
   <jsp:include page="../common/attendAside.jsp"/>   
   <section >
      <div id="mainDiv" style="width:950px;">
         <div id="titleDiv">
            <a id="titleName" style="color: #57585E;">소속근로시간</a>
         </div>
         <hr style="margin-top: 1%; margin-bottom: 2%;">
         <div style="border: 0.5px solid #C4C4C4; margin: 10px; width: 950px; height: 35px; background: white;">
            <a style="float:left; padding: 10px; font-weight: 500; margin-left: 5px;">검색일</a>
            <div style="float:left; margin-left: 4px; margin-right: 5px; border: 2px solid #C4C4C4; width:0.05px; height: 32px; "></div>
            <div style="float:left; padding: 8px;">
               <select style="background: white; color: gray; font-size: 15px; width: 120px;">
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
               </select>
               ~
            </div>
            
            <div style="float:left; padding: 8px;">
               <select style="background: white; color: gray; font-size: 15px; width: 120px;">
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
               </select>
            </div>
            <div style="float:left; padding: 5px; text-align:center; font-size: 14px; margin-left: 10px; margin-top: 6px; width: 55px; height: 15px; background: #27334C; color: white; "
            id="searchBtn">
               검색
            </div>
            <br>
            <br>
            <br>
            <div style="width: 1150px; clear: both; text-align: center; ">
               <table id="listTable" style="margin-left:0;">
                  <tr class="head" style="background-color:#27334C; color:white;">
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;" rowspan="2">성명</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;" rowspan="2">소속부서</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;" colspan="4">누적 근로시간</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;" colspan="2">잔여 근로시간</th>
                  </tr>
                  <tr class="head" style="background-color:#27334C; color:white;">
                    
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">소정근로시간</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">소정외근로시간</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">차감근로시간</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">총근로시간</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">소정근로시간</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">소정외근로시간</th>
                  </tr>
                  <tr class="row">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">유상무</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">인사팀</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">40시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">12시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                  </tr>
                   <tr class="row">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">유상무</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">인사팀</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">40시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">12시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                  </tr>
                   <tr class="row">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">유상무</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">인사팀</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">40시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">12시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0시간</td>
                  </tr>
                  
                  
               </table>
                  
               <br><br>
                 
         </div>
      </div>
      </div>
   </section>
   <script>
      function openAside(){

      }
      $("#listTable tr").click(function(){
         location.href="knowledgeDetail.me";
      })
   </script>
</body>
</html>