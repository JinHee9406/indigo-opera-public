<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <link rel="stylesheet" href="/opera/resources/css/knowledgeStyle.css">
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
   	<style>
   	#mainDiv {
			float: left;
			margin-left: 10px;
			margin-top: 70px;
		}
   	
	#searchContainer{
		border: 2px solid #C4C4C4; 
		margin: 10px; 
		width: 450px; 
		height: 35px; 
		background: #E5E5E5;
	}
	
	#searchEle1{
		float:left; 
		margin-right: 10px; 
		border: 2px solid #C4C4C4; 
		width:0.5px; 
		height: 33px;
	}
	
	#searchBtn{
		cursor: pointer; 
		float:left; 
		padding: 5px; 
		font-size: 14px; 
		margin-left: 10px; 
		margin-top: 6px;
		width: 55px; 
		height: 15px; 
		background: #27334C; 
		color: white; 
	}
	</style>
<title>Opera GroupWare</title>
</head>
<body>

	<jsp:include page="../common/knowledgeAside.jsp"/>	
	<jsp:include page="../common/operaNav.jsp"/>	

	<section>
		<div id="mainDiv">
			<div id="titleDiv">
				<img id="titleIcon" src="/opera/resources/icons/knowledge-black.png" ><a id="titleName">지식공유</a>
			</div>
			<hr>
				<div id="searchContainer">
				<form id="search" action="knowledgeSearch.know" method="post">
					<a style="float:left; padding: 10px; font-weight: 500;">지식검색</a>
					<div id="searchEle1"></div>
					<div style="float:left; padding: 8px;">
						<select name="searchType" style="background: #27334C; color: white; font-size: 15px;">
							<option value="title">제목</option>
							<option value="writer">작성자</option>
						</select>
					</div>
					<div style="float:left; padding: 8px; font-size: 15px;">
						<input type="text" name="searchText" placeholder="검색어를 입력해주세요">
					</div>
				</form>
					<div id="searchBtn" onclick="searchKnow();">
						검색하기
					</div>
				</div>


				<div style="width: 1090px; clear: both; text-align: center;">
					<table id="listTable" >
						<tr class="head">
							<th width="90px;" style="border-left: 3px solid #ccc;">번호</th>
							<th width="130px;">관련부서</th>
							<th width="490px;">글제목</th>
							<th width="110px;">작성자</th>
							<th width="140px;">작성일</th>
							<th width="90px;" style="border-right: 3px solid #ccc;">조회수</th>
						</tr>
						<c:forEach var="know" items="${knowledge}">
						<tr class="row">
							<td>${know.postNum}<div style="display:none;">${know.knowledgeNum}</div></td>
							<td>${know.knowledgeType}</td>
							<td>${know.knowledgeTitle}</td>
							<td>${know.knowledgeUserName}</td>
							<td>${know.knowledgeDate}</td>
							<td>${know.showpostCnt}</td>
						</tr>
						</c:forEach>
					</table>
				</div>

			
			<c:if test="${Typelist == 'not'}">
			<div id="pagingArea" align="center" style="">
				<c:if test="${pi.currentPage <= 1 }">
					[이전] &nbsp;
				
				</c:if>
				<c:if test="${pi.currentPage > 1 }">
					<c:url var="blistBack" value="knowledgeMain.know">
						<c:param name="pageNum" value="${pi.currentPage -1 }"/>
					</c:url>
					<a href="${blistBack }">[이전]</a> &nbsp;
				</c:if>
				<c:forEach var="p" begin="${pi.startPage }" end="${pi.endPage }">
					<c:if test="${p eq pi.currentPage }">
						<font color="red" size="4"><b>[${p}]</b></font>
					</c:if>
					<c:if test="${p ne pi.currentPage }">
						<c:url var="blistCheck" value="knowledgeMain.know">
							<c:param name="pageNum" value="${p }"/>
						</c:url>				
						<a href="${blistCheck }">${p}</a>
					</c:if>
				</c:forEach>
				<c:if test="${pi.currentPage >= pi.maxPage }">
					&nbsp;[다음]
				</c:if>
				<c:if test="${pi.currentPage < pi.maxPage }">
					<c:url var="blistEnd" value="knowledgeMain.know">
						<c:param name="pageNum" value="${pi.currentPage + 1 }"/>
					</c:url>
					&nbsp; <a href="${blistEnd}">[다음]</a>
				</c:if>
			</div>
			</c:if>
			
				<c:if test="${Typelist != 'not'}">
			<div id="pagingArea" align="center" style="">
				<c:if test="${pi.currentPage <= 1 }">
					[이전] &nbsp;
				
				</c:if>
				<c:if test="${pi.currentPage > 1 }">
					<c:url var="blistBack" value="knowledgeType.know?knowType=${Typelist}">
						<c:param name="pageNum" value="${pi.currentPage -1 }"/>
					</c:url>
					<a href="${blistBack }">[이전]</a> &nbsp;
				</c:if>
				<c:forEach var="p" begin="${pi.startPage }" end="${pi.endPage }">
					<c:if test="${p eq pi.currentPage }">
						<font color="red" size="4"><b>[${p}]</b></font>
					</c:if>
					<c:if test="${p ne pi.currentPage }">
						<c:url var="blistCheck" value="knowledgeType.know?knowType=${Typelist}">
							<c:param name="pageNum" value="${p }"/>
						</c:url>				
						<a href="${blistCheck }">${p}</a>
					</c:if>
				</c:forEach>
				<c:if test="${pi.currentPage >= pi.maxPage }">
					&nbsp;[다음]
				</c:if>
				<c:if test="${pi.currentPage < pi.maxPage }">
					<c:url var="blistEnd" value="knowledgeType.know?knowType=${Typelist}">
						<c:param name="pageNum" value="${pi.currentPage + 1 }"/>
					</c:url>
					&nbsp; <a href="${blistEnd}">[다음]</a>
				</c:if>
			</div>
			</c:if>
			
		</div>
		
		
	</section>
	<script>
		function searchKnow(){
			document.getElementById("search").submit();
		}
	</script>
	
		
	<script>
		$("#listTable tr.row").click(function(){
			knowledgeNum = $(this).children("td").eq(0).children("div").text();
			location.href="knowledgeDetail.know?knowledgeNum=" + knowledgeNum;
		})
	</script>
</body>
</html>