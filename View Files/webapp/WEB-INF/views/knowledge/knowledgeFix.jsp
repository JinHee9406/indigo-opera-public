<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script type="text/javascript" src="/opera/resources/se2/js/service/HuskyEZCreator.js" charset="utf-8"></script>
   <link rel="stylesheet" href="/opera/resources/css/knowledgeStyle.css">
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
 
<title>Opera GroupWare</title>
<style>
   	#mainDiv {
			float: left;
			margin-top: 70px;
		}
		
	#addKnowTable{
		width: 100%;
		border-collapse: collapse;
		border-top: 2px solid #848484;
		border-bottom: 2px solid #848484;
		
	}
	
	#addKnowTable th{
		padding: 10px;
		background: #E5E5E5;
		text-align: right;
		padding-right: 15px;
		width: 190px;
	}
	
	#addKnowTable td{
		background: #F9F9F9;
		padding-left: 10px;
	}
	
	.userName{
		font-size: 0.9em;
	}
	
	#saveBtn button{
		width: 100px; 
		height: 40px; 
		background: #27334C; 
		color: white; 
		font-weight: bold; 
		border-radius: 15px; 
		outline: none;
	}
</style>
</head>
<body>
	<jsp:include page="../common/knowledgeAside.jsp"/>	
	<jsp:include page="../common/operaNav.jsp"/>	
	<section>
		<div id="mainDiv">
			<form id="knowledgePost" action="updateKnowledge.know" method="post">
			<div id="titleDiv">
				<input type="hidden" name="knowledgeNum" value="${knowledge.knowledgeNum}">
				<a id="titleName">지식공유 수정</a>
			</div>
			<table id="addKnowTable">
				<tr>
					<th>제목</th>
					<td><input type="text" name="knowledgeTitle" value="${ knowledge.knowledgeTitle }" style="width: 300px; height: 25px;"></td>
				</tr>
				<tr>
					<th>작성자</th>
					<td class="userName"><c:out value="${loginUser.empName} ${loginUser.empJobName}"/><input type="hidden" name="knowledgeUser" value="${loginUser.empNo}">
					<input type="hidden" name="knowledgeUserName" value="${loginUser.empName} ${loginUser.empJobName}"></td>
					
				</tr>
				<tr>
					<th>구분</th>
					<td>
						<select id="fixType" name="knowledgeType" style="height: 25px;">
							<option value="회사관련">회사관련</option>
							<option value="회사외부관련">회사외부관련</option>
							<option value="인사총무부관련">인사총무부관련</option>
							<option value="연구개발부관련">연구개발부관련</option>
							<option value="기획부관련">기획부관련</option>
							<option value="재무회계부관련">재무회계부관련</option>
							<option value="경비부관련">경비부관련</option>
						</select>
					</td>
				</tr>
			</table>
			<textarea name="knowledgeDetail" id="ir1" rows="10" cols="100" style="width:1090px;">
			${knowledge.knowledgeDetail}
			</textarea>
			</form>
			<div id="saveBtn" style="text-align:right;">
				<button onclick="submitInputs();">저장</button>
				<button onclick="location.href='knowledgeDetail.know?knowledgeNum=${knowledge.knowledgeNum}'" style="background: white; color: black;">수정취소</button>
			</div>
		</div>
		<script>
			function typeFix(){

				var type = '${ knowledge.knowledgeType }'

				console.log(type);

					switch(type){
	
						case "회사관련" : 
								$("#fixType").val("회사관련").prop("selected",true);
							break;
						case "회사외부관련" : 
								$("#fixType").val("회사외부관련").prop("selected",true);
							break;
						case "인사총무부관련" : 
								$("#fixType").val("인사총무부관련").prop("selected",true);
							break;
						case "연구개발부관련" : 
								$("#fixType").val("연구개발부관련").prop("selected",true);
							break;
						case "기획부관련" : 
								$("#fixType").val("기획부관련").prop("selected",true);
							break;
						case "재무회계부관련" : 
								$("#fixType").val("재무회계부관련").prop("selected",true);
							break;
	
					}
				
				}
			 typeFix();
		</script>

	    <script type="text/javascript">
	        var oEditors = [];
	        nhn.husky.EZCreator.createInIFrame({
	         oAppRef: oEditors,
	         elPlaceHolder: "ir1",
	         sSkinURI: "/opera/resources/se2/SmartEditor2Skin.html",
	         //se2폴더 경로(프로젝트안에 넣어놓는게 좋음)
	         fCreator: "createSEditor2"
	        });
	    </script>
	    <script>
		    function submitContents(elClickedObj) {
		    	 // 에디터의 내용이 textarea에 적용된다.
		    	 oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
				
		    	 // 에디터의 내용에 대한 값 검증은 이곳에서
		    	 // document.getElementById("ir1").value를 이용해서 처리한다.
		
		    	 try {
		    	     elClickedObj.form.submit();
		    	 } catch(e) {}
		    }
	    </script>
	    <script>
			function submitInputs(){
				var knowledgeForm = document.getElementById("knowledgePost");
				//console.log(editerText);
				oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
		 		console.log(document.getElementById("ir1").value);
		 		var editerArea = document.getElementById("ir1");
				knowledgeForm.submit();
			}
	    </script>
	    <script>
			
	    </script>
	</section>
</body>
</html>