<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
</head>

<style>
	#main-content {
		margin-top: 185px;
	}
	
	#mainTable {
		position: absolute;
		border: 1px solid black;
		left: 350px;
	}
	
	th {
		background: #27334C;
		color: white;
	}
</style>
<body>
	<jsp:include page="../common/mypageAside.jsp" />
	<jsp:include page="../common/operaNav.jsp" />
	<section>
		<div id="main-content">
	        <table id="mainTable">
	            <tr>
	                <th style="width: 70px;">No</th>
	                <th style="width: 100px;">문서종류</th>
	                <th style="width: 600px;">제목</th>
	                <th style="width: 100px;">작성자</th>
	                <th style="width: 130px;">등록일</th>
	            </tr>
	            <tr>
	                <td colspan="5" style="color: lightgray;">등록된 중요업무가 없습니다.</td>
	            </tr>
	        </table>
	    </div>
	</section>
</body>
</html>