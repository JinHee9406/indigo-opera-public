<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<style>
        #main-content{
            margin-left: 300px;
        }
        table {
            display: inline-block;
           
            margin-left: 100px;
            width: 1000px;
        }
        #myinfoTable td{
            width: 400px;
            border: lightgray 1px solid;
        }
        #myinfoTable th{
            width: 200px;
            background-color: #27334C;
            color: lightgray;
        }
        #myinfoTable tr{
            height: 35px;
        }
        #saveBtn{
            display: inline-block;
        }
        #cancelBtn{
            display: inline-block;
        }
        #mypageAside li{
            border: lightgray 1px solid;
            height: 50px;
            font-size: 1.3em;
            
        }
        #imageTable {
            width: 1000px;
            border: lightgray 1px solid;
        }
        #imageTable td{
            border: lightgray 1px solid;
        }
        
        
</style>
</head>
<body>
	<jsp:include page="../common/mypageAside.jsp" />
	<jsp:include page="../common/operaNav.jsp" />
	<section>
		 <!-- main-content -->
	    <div id="main-content">
	        <div>
	            <table id="imageTable" style="margin-top: 100px;">
	                
	                <tr >
	                    <td style="height: 100px;" id="userImg2"></td>
	                </tr>
	                
	                <tr>
	                    <td><input type="button" style="border: none; background: white; margin-left: 23px;" value="사진변경" onclick=""></td>
	                </tr>
	                <tr>
	                    <td><input type="button" style="border: none; background: white; margin-left: 30px;" value="초기화" onclick=""></td>
	                </tr>
	            </table>
	        </div>
	        <table id="myinfoTable">
	            <tr>
	                <th>사번</th>
	                <td><c:out value="${loginUser.empNo}"/></td>
	                <th>이메일</th>
	                <td colspan="3"><c:out value="${loginUser.empEmail}"/></td>
	            </tr>
	            <tr>
	                <th>성명</th>
	                <td><c:out value="${loginUser.empName}"/></td>
	                <th>주민등록번호</th>
	                <td colspan="3"><c:out value="${loginUser.empNumber}"/></td>
	            </tr>
	            <tr>
	                <th>소속법인</th>
	                <td colspan="5"><c:out value="${loginUser.empDept}"/></td>
	            </tr>
	            <tr>
	                <th>소속</th>
	                <td colspan="5"><c:out value="${loginUser.empDept}"/></td>
	            </tr>
	            <tr>
	                <th>입사일</th>
	                <td colspan="5"><c:out value="${loginUser.empJoinDate}"/></td>
	            </tr>
	            <tr>
	                <th>영문성명</th>
	                <td colspan="5"><c:out value="${loginUser.empEngName}"/></td>
	            </tr>
	            <tr>
	                <th>직무</th>
	                <td><c:out value="${loginUser.empJobCode}"/></td>
	                <th>직위</th>
	                <td><c:out value="${loginUser.empJobCode}"/></td>
	                <th>재직여부</th>
	                <td><c:out value="${loginUser.status}"/></td>
	            </tr>
	            <tr>
	                <th>성별</th>
	                <td colspan="5"><c:out value="${loginUser.empGender}"/></td>
	            </tr>
	            <tr>
	                <th>계좌번호</th>
	                <td colspan="5"><c:out value="${loginUser.empBankName} ${loginUser.empBankNumber}"/></td>
	            </tr>
	            <tr>
	                <th>전화번호</th>
	                <td><c:out value="${loginUser.empHomePhone}"/></td>
	                <th>핸드폰</th>
	                <td colspan="3"><c:out value="${loginUser.empPhone}"/></td>
	            </tr>
	            <tr>
	                <th>결혼여부</th>
	                <td colspan="5"><c:out value="${loginUser.empMstatus}"/></td>
	            </tr>
	            <tr>
	                <th>주소</th>
	                <td colspan="5"><c:out value="${loginUser.empAddress}"/></td>
	            </tr>
	            
	
	        </table>
	    </div>
	</section>
	
	<script>
	function userProfileLoad() {
		var userEmpNo = ${loginUser.empNo}
					
		$.ajax({
			url:"loadUserFile.file",
			type:"post",
			data:{userEmpNo: userEmpNo},
			success:function(data) {
				var imgName = data.fileList.changeName;
				$("#userImg2").children().remove()
				$("#userImg2").append("<img width='100' src='${contextPath}/resources/uploadFiles/"+imgName+"'>");
			},
			error:function(){
				console.log("에러!");
			}
		});
		
		return false;
	}
	userProfileLoad();
	</script>
	
	
</body>
</html>