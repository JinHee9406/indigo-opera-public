<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<style>
        #main-content{
            margin-left: 300px;
        }
        table {
            display: inline-block;
           
            margin-left: 100px;
            
        }
        
        #saveBtn{
            display: inline-block;
        }
        #cancelBtn{
            display: inline-block;
        }
        #mypageAside li{
            border: lightgray 1px solid;
            height: 50px;
            font-size: 1.3em;
            
        }
       
        #mainTable {
            margin-top: 100px;
            text-align: center;
        }
        #mainTable th{
            background-color: #27334C;
            color: lightgray;
            height: 30px;
        }
        #mainTable{
            border: lightgray 1px solid;
        }
</style>
</head>
<body>
	<jsp:include page="../common/mypageAside.jsp" />
	<jsp:include page="../common/operaNav.jsp" />
   <!-- main-content -->
   <section>
	    <div id="main-content">
	        <table id="mainTable">
	            <tr>
	                <th style="width: 100px;">구분</th>
	                <th style="width: 100px;">문서종류</th>
	                <th style="width: 600px;">제목</th>
	                <th style="width: 100px;">알림설정</th>
	            </tr>
	            <tr>
	                <td rowspan="2">결재</td>
	                <td>대기</td>
	                <td>대기문서함에 문서가 등록되었을 때</td>
	                <td><input type="checkbox"></td>
	            </tr>
	            <tr>
	                <td>완료</td>
	                <td>문서가 결재 완료 되었을 때</td>
	                <td><input type="checkbox"></td>
	            </tr>
	            <tr>
	                <td rowspan="2">참조</td>
	                <td>결재시작</td>
	                <td>대기문서함에 문서가 등록되었을 때</td>
	                <td><input type="checkbox"></td>
	            </tr>
	            <tr>
	                <td>결재완료</td>
	                <td>문서가 결재 완료 되었을 때</td>
	                <td><input type="checkbox"></td>
	            </tr>
	            <tr>
	                <td>수신</td>
	                <td></td>
	                <td>문서가 수신되었을 때</td>
	                <td><input type="checkbox"></td>
	            </tr>
	            <tr>
	                <td>회람</td>
	                <td></td>
	                <td>문서가 회람되었을 때</td>
	                <td><input type="checkbox"></td>
	            </tr>
	            <tr>
	                <td rowspan="3">기안</td>
	                <td>반려</td>
	                <td>기안한 문서 중에서 결재자 또는 합의자가 반려했을 때</td>
	                <td><input type="checkbox"></td>
	            </tr>
	            <tr>
	                <td>협의요청</td>
	                <td>기안한 문서 중에서 결재자 또는 합의자가 협의 요청했을 때</td>
	                <td><input type="checkbox"></td>
	            </tr>
	            <tr>
	                <td>완료</td>
	                <td>기안한 문서의 모든 결재 및 합의가 완료되었을 때</td>
	                <td><input type="checkbox"></td>
	            </tr>
	        </table>
	    </div>
    </section>
</body>
</html>