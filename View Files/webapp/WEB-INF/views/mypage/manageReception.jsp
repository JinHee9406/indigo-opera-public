<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<style>
   		#main-content{
            margin-left: 300px;
            position: absolute;
            top: 200px;
        }
        
        
        #saveBtn{
            display: inline-block;
        }
        #cancelBtn{
            display: inline-block;
        }
        #mypageAside li{
            border: lightgray 1px solid;
            height: 50px;
            font-size: 1.3em;
            
        }
       
        #mainTable {
            margin-left: 50px;
            text-align: center;
            width: 1000px;
        }
        #mainTable th{
            background-color: #27334C;
            color: lightgray;
            height: 30px;
        }
        #mainTable{
            border: lightgray 1px solid;
        }

</style>
</head>
<body>
	<jsp:include page="../common/mypageAside.jsp" />
	<jsp:include page="../common/operaNav.jsp" />
	<section>
		<div id="main-content">
	        <span style="margin-left: 50px;">개인수신자</span><a style="font-size:0.8em; color: blue;">수신자등록</a>
	        <table id="mainTable">
	            <tr>
	                <th style="width: 850px;">수신자 그룹 명</th>
	                <th style="width: 150px;">관리</th>
	            </tr>
	            <tr>
	                <td colspan="2" style="color: lightgray;">등록된 그룹이 없습니다.</td>
	            </tr>
	           
	        </table>
	    </div>
	</section>
</body>
</html>