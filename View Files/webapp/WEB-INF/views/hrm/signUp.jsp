<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.abc {
	background-color: #27334C;
	color: white;
}

.tac {
	background-color: #27334C;
	color: white;
}

#infoTable {
	width: 90%;
	margin-left: auto;
	margin-right: auto;
}

#detailTable {
	width: 90%;
	margin-left: auto;
	margin-right: auto;
}

#imageTb td {
	border: lightgray 1px solid;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/common/operaNav.jsp" />
	<jsp:include page="/WEB-INF/views/common/hrmAside.jsp" />
	<h1 style="margin-top: 50px; margin-left: 335px;">직원등록</h1>
	<div style="margin-left: 280px; margin-top: 50px;">
		<span style="margin-left: 60px;">▶[기본정보]</span>
		<form action="insert1.me" method="post" enctype="multipart/form-data">
			<table border="1" id="infoTable">
				<tr>
					<td class="abc">사번</td>
					<td><input type="text" name="empNo" id="empNo"></td>
				</tr>
				<tr>
					<td class="abc">이메일</td>
					<td><input type="text" name="empEmail"></td>
				</tr>
				<tr>
					<td class="abc">비밀번호</td>
					<td><input type="password" name="empPwd"></td>
				</tr>
				<tr>
					<td class="abc">성명</td>
					<td><input type="text" name="empName"></td>
				</tr>
				<tr>
					<td class="abc">주민번호</td>
					<td><input type="text" name="empNumber"></td>
				</tr>
				<tr>
					<td class="abc">소속</td>
					<td><input type="text" name="empAff"></td>
				</tr>
				<tr>
					<td class="abc">입사일</td>
					<td><input type="text" name="empJoinDate"></td>
				</tr>
			</table>
			<span style="margin-left: 60px;">▶[상세정보]</span>
			<table border="1" id="detailTable">
				<tr>
					<td class="tac">영문성명</td>
					<td><input type="text" name="empEngName"></td>
				</tr>
				<tr>
					<td class="tac">직무</td>
					<td>
					<select id="DeptName">
							<option value="005">연구개발부서</option>
							<option value="006">기획부서</option>
							<option value="003">인사총무부서</option>
							<option value="002">재무회계부서</option>
							<option value="004">경비부</option>
							<option value="001">경영지원부</option></select></td>
					<td class="tac">직위</td>
					<td><select id="empJobCode">
							<option value="JOB9">사원</option>
							<option value="JOB8">대리</option>
							<option value="JOB7">과장</option>
							<option value="JOB6">차장</option>
							<option value="JOB5">부장</option>
							<option value="JOB4">이사</option>
							<option value="JOB3">상무</option>
							<option value="JOB2">전무</option>
							<option value="JOB1">사장</option>
					</select></td>
					<td class="tac">재직여부</td>
					<td><select id="status">
							<option value="휴직">휴직</option>
							<option value="재직">재직</option>
					</select></td>
				</tr>
				<tr>
					<td class="tac">국적</td>
					<td><input type="text" name="empNation"></td>
				</tr>
				<tr>
					<td class="tac">성별</td>
					<td><select id="empGender">
							<option value="M">남</option>
							<option value="F">여</option>
					</select></td>
				</tr>
				<tr>
					<td class="tac">계좌번호</td>
					<td colspan="5"><select 
						id="empBankName"><option>은행선택</option>
							<option value="카카오뱅크">카카오뱅크</option>
							<option value="국민">국민</option>
							<option value="농협">농협</option>
							<option value="신한">신한</option>
							<option value="SC제일">ＳＣ제일</option></select> 
					<input type="text" name="empBankNumber" id="empBankNumber"></td>
				</tr>
				<tr>
					<td class="tac">예금주명</td>
					<td><input type="text" name="empDipositName"></td>
				<tr>
					<td class="tac">전화번호</td>
					<td><input type="text" name="empHomePhone"></td>
					<td class="tac">핸드폰</td>
					<td><input type="text" name="empPhone"></td>
				</tr>
				<tr>
					<td class="tac">결혼여부</td>
					<td><select id="empMstatus">
							<option value="미혼">미혼</option>
							<option value="기혼">기혼</option>
					</select></td>
				</tr>
				<tr>
					<td class="tac">주소</td>
					<td colspan="5"><input type="text" name="empAddress"
						id="empAddress" class="input_st1"></td>
				</tr>
				<tr>
					<td class="tac">상세주소</td>
					<td colspan="5"><input type="text" name="empAddress"
						id="empAddress"></td>
				</tr>
				</tbody>
			</table>
	</div>
	<div align="center">
		<button type="reset">작성취소</button>
		<button type="submit" id="btn_singUp">가입하기</button>
	</div>
	<input type="hidden" name="empDept" id="realDept">
	<input type="hidden" name="empJobCode" id="realJobCode">
	<input type="hidden" name="status" id="realStatus">
	<input type="hidden" name="empGender" id="realGender">
	<input type="hidden" name="empBankName" id="realBankName">
	<input type="hidden" name="empMstatus" id="realMstatus">
	</form>

	<script>
			function duplicationCheck() {
				var empNo = $("empNo").val();
				
				$.ajax({
					url:"duplicationCheck.me",
					type:"post",
					data:{empNo: empNo},
						success:function(data) {
							console.log(data);
						},
						error:function() {
							console.log("에러!");
						}
				});
				
				return false;
			}
			$(document).on("change", "#DeptName", function(){
				$("#realDept").val($(this).find("option:selected").val());
				console.log($("#realDept").val());
			});
			$(document).on("change", "#empJobCode", function(){
				$("#realJobCode").val($(this).find("option:selected").val());
				console.log($("#realJobCode").val());
			});
			$(document).on("change", "#status", function(){
				$("#realStatus").val($(this).find("option:selected").val());
				console.log($("#realStatus").val());
			});
			$(document).on("change", "#empGender", function(){
				$("#realGender").val($(this).find("option:selected").val());
				console.log($("#realGender").val());
			});
			$(document).on("change", "#empBankName", function(){
				$("#realBankName").val($(this).find("option:selected").val());
				console.log($("#realBankName").val());
			});
			$(document).on("change", "#empMstatus", function(){
				$("#realMstatus").val($(this).find("option:selected").val());
				console.log($("#realMstatus").val());
			});
		</script>
</body>
</html>
