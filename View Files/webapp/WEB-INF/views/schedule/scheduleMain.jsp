<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/calendarStyle.css">
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
<style>   
      #mainDiv {
         float: left;
         width: 1100px;
         margin-left: 50px;
         margin-top: 100px;
      }
</style>
</head>
<body>
   <jsp:include page="../common/operaNav.jsp" />
   <jsp:include page="../common/scheduleAside.jsp" />

   <section>
      <div id="mainDiv">
         <div id="titleDiv">
            <img id="titleIcon"
               src="/opera/resources/icons/meetingIcon-black.png"><a
               id="titleName">예약현황</a>
         </div>
         <hr>
         <div id="reservationIn">
            <div id="meetingrooms" style="float: left;">
               회의실
               <div
                  style="border-radius: 5px; border: 2px solid #8d8a8a; text-align: center; clear: both; width: 200px; height: 500px; background: #E5E5E5">
                  <div>202호 회의실</div>
                  <div>301호 회의실</div>
               </div>
            </div>
            <div id="meetingCal" style="float: left; margin-left: 250px;">
               <div id="calendar">
                      <div class="calmain">
                         <div class="content-wrap">
                           <div class="content-right" style="margin-left: -60px;">
                             <table id="calendar" align="center"  >
                               <thead>
                                 <tr class="btn-wrap clearfix">
                                   <td style="text-align: center;">
                                      <div onclick="prev();">
                                        <label id="prev" >
                                           &#60;
                                        </label>
                                     </div>
                                   </td>
                                   <td align="center" id="current-year-month" colspan="5" ></td>
                                   <td style="text-align: center;">
                                      <div onclick="next();">
                                     <label id="next">
                                         &#62;
                                     </label>
                                     </div>
                                   </td>
                                 </tr>
                                 <tr>
                                     <td class = "sun" align="center">Sun</td>
                                     <td align="center">Mon</td>
                                     <td align="center">Tue</td>
                                     <td align="center">Wed</td>
                                     <td align="center">Thu</td>
                                     <td align="center">Fri</td>
                                     <td class= "sat" align="center">Sat</td>
                                   </tr>
                               </thead>
                               <tbody id="calendar-body" class="calendar-body"></tbody>
                             </table>
                           </div>
                         </div>
                       </div>
                  </div>

            </div>

         </div>
         <div style="clear: both;"><hr></div>

      </div>
   </section>
   <script>
      //예약현황을 출력해주는 스크립트
      function checkReservation(){
         $("#15").append("<div style='padding-top:5px; width: 100%; height: 20px; border-radius: 25px; background: #27334C; color: white; font-weight: normal; font-size: 0.8em;'>" +
               "10:00 ~ 11:00</div>");
      }
   </script>
   
   <script>
      var currentTitle = document.getElementById('current-year-month');
      var calendarBody = document.getElementById('calendar-body');
      var today = new Date();
      var first = new Date(today.getFullYear(), today.getMonth(),1);
      var dayList = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
      var monthList = ['January','February','March','April','May','June','July','August','September','October','November','December'];
      var leapYear=[31,29,31,30,31,30,31,31,30,31,30,31]; //윤년
      var notLeapYear=[31,28,31,30,31,30,31,31,30,31,30,31]; //윤년 아님
      var pageFirst = first; 
      var firstMonth = today.getMonth();
      var pageMonth = today.getMonth();
      console.log(pageMonth);
      var pageYear;
      if(first.getFullYear() % 4 === 0){
          pageYear = leapYear;
      }else{
          pageYear = notLeapYear;
      }

      //달력을 출력하는 함수
      function showCalendar(){
          let monthCnt = 100;
          let cnt = 1;
          for(var i = 0; i < 6; i++){
              var $tr = document.createElement('tr');
              $tr.setAttribute('id', monthCnt);   
              for(var j = 0; j < 7; j++){
                  if((i === 0 && j < first.getDay()) || cnt > pageYear[first.getMonth()]){
                      var $td = document.createElement('td');
                      $tr.appendChild($td);     
                  }else{
                      var $td = document.createElement('td');
                      var $tdEle = document.createElement('div');
                      $tdEle.textContent = cnt;
                      
                      $tdEle.setAttribute('id', cnt); 
                      //표 칸마다 id를 붙이며 id값는 숫자
                      
                      $tdEle.setAttribute('style','width: 90px; height: 60px; border: 1px solid black;')
                      //90x60 스타일을 적용하는 setAttribute
                      
                      $td.appendChild($tdEle);
                  //td부분에 요소 추가
                                     
                      $tr.appendChild($td);
                      cnt++;
                  }
              }
              monthCnt++;
              currentTitle.innerHTML = monthList[first.getMonth()] + '&nbsp;&nbsp;&nbsp;&nbsp;'+ first.getFullYear();
              today = new Date(today.getFullYear(), today.getMonth() + 1, today.getDate());
              calendarBody.appendChild($tr);
          }
      }

      showCalendar();

      //달력 달수를 변경할때 달력을 전부 지우고 다시불러오기 위해 지우는 함수
      function removeCalendar(){
          let catchTr = 100;
          for(var i = 100; i< 106; i++){
              var $tr = document.getElementById(catchTr);
              $tr.remove();
              catchTr++;
          }
      }

      //전달로 이동하는 함수
      function prev(){
          const $divs = document.querySelectorAll('#input-list > div');
          $divs.forEach(function(e){
            e.remove();
          });
          const $btns = document.querySelectorAll('#input-list > button');
          $btns.forEach(function(e1){
            e1.remove();
          });
          if(pageFirst.getMonth() === 1){
              pageFirst = new Date(first.getFullYear()-1, 12, 1);
              first = pageFirst;
              if(first.getFullYear() % 4 === 0){
                  pageYear = leapYear;
              }else{
                  pageYear = notLeapYear;
              }
          }else{
              pageFirst = new Date(first.getFullYear(), first.getMonth()-1, 1);
              first = pageFirst;
          }
          today = new Date(today.getFullYear(), today.getMonth()-1, today.getDate());
          pageMonth = today.getMonth();
          currentTitle.innerHTML = monthList[first.getMonth()] + '&nbsp;&nbsp;&nbsp;&nbsp;'+ first.getFullYear();
          removeCalendar();
          showCalendar();
           showToday();
      }

      //다음달로 이동하는 함수
      function next(){
          const $divs = document.querySelectorAll('#input-list > div');
          $divs.forEach(function(e){
            e.remove();
          });
          const $btns = document.querySelectorAll('#input-list > button');
          $btns.forEach(function(e1){
            e1.remove();
          });
          if(pageFirst.getMonth() === 12){
              pageFirst = new Date(first.getFullYear()+1, 1, 1);
              first = pageFirst;
              if(first.getFullYear() % 4 === 0){
                  pageYear = leapYear;
              }else{
                  pageYear = notLeapYear;
              }
          }else{
              pageFirst = new Date(first.getFullYear(), first.getMonth()+1, 1);
              first = pageFirst;
          }
          today = new Date(today.getFullYear(), today.getMonth() + 1, today.getDate());
          pageMonth = today.getMonth();
          currentTitle.innerHTML = monthList[first.getMonth()] + '&nbsp;&nbsp;&nbsp;&nbsp;'+ first.getFullYear();
          removeCalendar();
          showCalendar(); 
           showToday();
      }

      //오늘인 부분은 오페라 색상으로 변경(연도 구분없음 )
      function showToday(){
         if(firstMonth == pageMonth){
             clickedDate1 = document.getElementById(today.getDate());
             clickedDate1.classList.add('active');
         }
           checkReservation();
      }
        showToday();
   </script>

</body>
</html>