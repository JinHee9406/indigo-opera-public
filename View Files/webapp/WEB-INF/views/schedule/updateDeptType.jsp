<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
</head>
<body>
	<jsp:include page="../common/scheduleAside.jsp" />
	<jsp:include page="../common/operaNav.jsp" />
	<section>
	    <!-- main-content -->
	    <div id="main-content">
		    <div style="margin-bottom: 100px">
		            <span style="font-size: 2em;">일정구분 관리</span>
		        </div>
		        <div>
		        <table>
		           <tr>
		               <th>캘린더 종류</th>
		               <th>캘린더 목록</th>
		               <th>공유상태</th>
		               <th>권한</th>
		               <th>생성자</th>
		               <th>수정</th>
		               <th>삭제</th>
		           </tr>
		           <tr>
		               <td>부서캘린더</td>
		               <td style="background-color: yellow;">회의실</td>
		               <td>나만사용</td>
		               <td>읽기+쓰기</td>
		               <td>김승렬 프로</td>
		               <td><input type="button" value="수정" onclick=""></td>
		               <td><input type="button" value="삭제" onclick=""></td>
		           </tr>
		           <tr>
		            <td>부서캘린더</td>
		            <td style="background-color: yellowgreen;">휴가공유</td>
		            <td>나만사용</td>
		            <td>읽기+쓰기</td>
		            <td>김승렬 프로</td>
		            <td><input type="button" value="수정" onclick=""></td>
		            <td><input type="button" value="삭제" onclick=""></td>
		            </tr>
		            <tr>
		            <td>부서캘린더</td>
		            <td style="background-color: magenta;">디자인실</td>
		            <td>나만사용</td>
		            <td>읽기+쓰기</td>
		            <td>김승렬 프로</td>
		            <td><input type="button" value="수정" onclick=""></td>
		            <td><input type="button" value="삭제" onclick=""></td>
		            </tr>
		        </table>
		    </div>
    	</div>
	
	</section>
</body>
</html>