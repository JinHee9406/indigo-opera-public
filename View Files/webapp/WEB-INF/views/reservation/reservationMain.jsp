<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/calendarStyle.css">
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
   
<style>	
		#mainDiv {
			float: left;
			width: 1100px;
			margin-left: 20px;
			margin-top: 70px;
		}
		
		.meetingRoomsList{
			border-radius: 5px; 
			border: 2px solid #8d8a8a; 
			text-align: center; 
			clear: both; 
			width: 200px; 
			height: 600px; 
			background: #F6F6F6;
			font-weight: bold;
			margin-top: 15px;
		}
		
		.meetroom{
			margin-top: 15px;
			margin-bottom: 15px;
			color: #626262;
			font-size: 1em;
			cursor: pointer;
		}
		
		#meetingtitle a{
			margin-top: 25px; 
			font-size: 1.4em;
			font-weight: bold;
			margin-left: 15px;
			
		}
		#on{
			color: #E61358;
		}
		#meetroomInfo{
			font-size: 1.4em;
			margin-top: 10px; 
			font-size: 1.4em;
			font-weight: bold;
			margin-left: 130px;
		}
		
		.content-wrap{
		  border: 2px solid #EAEAEA; 
		}
		
		#meetroomImg{
			float: left;
			margin-top: 80px;;
			margin-left: 90px;
		}
		
		#meetroomData{
			float: left;
			margin-top: 100px;;
			margin-left: 80px;
		}
		
		#meetroomData textarea{
			border: none;
			outline: none;
			width: 200px;
			height: 200px;
			resize:none;
		
		}
		
		
		#roomDataTable th, #roomDataTable td{
			padding: 10px;
		}
		
		#addroom{
			margin-left: 48px;
			border-radius: 5px; 
			padding-top: 5px;
			width: 100px; 
			height: 20px; 
			font-size: 0.8em; 
			
			background: #FFFFFF;
			border: 1px solid #767676;
			box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
			
			cursor: pointer;
		}
		
		
		.meetroom_wrap {
			display: none;
			position: fixed;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			z-index: 10000
		}
		
		.dark_bg {
			position: absolute;
			width: 100%;
			height: 100%;
			background: #000;
			filter: alpha(opacity = 60);
			opacity: .6;
			-moz-opacity: .6
		}
		
		.meetroom_box {
			position: relative;
			top: 5%;
			width: 600px;
			height: 450px;
			background: #fff;
			margin: 0 auto;
			border-radius: 8px;
			background: #fff;
			border: 1px solid #ccc
		}
		
		.meetroom_box .close {
			position: absolute;
			right: 6px;
			top: 6px;
			width: 16px;
			height: 16px;
			cursor: pointer
		}
		
		#addWindowTitle{
			float:left; 
			font-weight: bold; 
			margin-left: 80px; 
			margin-top: 45px;
			color: #626262;
		}
		
		#windowIcon{
			width: 35px;
			height: 35px;
			padding-right: 10px;
			margin-bottom: -5px;
		}
		
		
		.closeBtn{
			float:right; 
			margin-left: 250px; 
			font-size: 1.25em; 
			font-weight: bold;
			cursor: pointer;
		}
		
		#wrap_container{
			text-align: center;
		}
		
		#addMeetTable tr td input{
			width: 100%;
		}
		
		#addMeetTable tr td textarea{
			width: 100%;
			height: 100%;
			resize: none;
		}
		
		#submitBtn{
			width: 100px; 
			height: 25px; 
			border-radius: 5px; 
			background: #27334C; 
			font-weight: bold; 
			color: white;
			margin-left: 240px;
			padding-top: 7px;
		}
</style>
</head>
<body>	
	<div class="meetroom_wrap" style="display: none;">
			<div class="dark_bg" onclick="jQuery('.meetroom_wrap').fadeOut('slow')"></div>
			<div class="meetroom_box">
			<div id="addWindowTitle">
				<div style="margin-bottom: 15px;">
					<div class="closeBtn" onclick="jQuery('.meetroom_wrap').fadeOut('slow')">X</div>
					<img id="windowIcon" src="/opera/resources/icons/meetingIcon-black.png"><a style="font-size: 1.6em;">회의실 추가</a>
					</div>
				</div>
				<br>
				<hr style="clear: both; width: 500px; margin-left: 55px;">
				<div id="wrap_container">
					<form id="meetingRoomForm" action="insertMeetRoom.res" method="post" enctype="multipart/form-data">
						<div style="margin-left: 100px;">
							<table id="addMeetTable" style="width: 400px;  height: 270px;">
								<tr>
									<th>회의실 이름</th>
									<td><input type="text" name="roomName" placeholder="회의실 이름을 적어주세요"></td>
								</tr>
								<tr>
									<th>회의실 설명</th>
									<td>
									<textarea name="roomInfo" placeholder="회의실에 있는 시설 등 회의실을 설명해주세요"></textarea></td>
								</tr>
								<tr>
									<th>회의실 이미지</th>
									<td><input type="file" name="photo"></td>
								</tr>
							</table>			
						</div>
						<div id="submitBtn" onclick="addMeetingRoom()">저장</div>
					</form>
				</div>
			</div>
			
	</div>


	<jsp:include page="../common/reservationAside.jsp" />
	<jsp:include page="../common/operaNav.jsp" />
	<section>
		<div id="mainDiv">
			<div id="titleDiv">
				<img id="titleIcon"
					src="/opera/resources/icons/meetingIcon-black.png"><a
					id="titleName">예약현황</a>
			</div>
			<hr>
			<div id="reservationIn">
				<div id="meetingtitle" style="float: left; margin: 15px;">
					<a>회의실</a>
					<div class="meetingRoomsList">
					<c:forEach var="room" items="${meetingRoom}">
						<div class="meetroom">${room.roomName}<input type="hidden" value="${room.roomNo}"></div>
					</c:forEach>
						<div id="addroom" onclick="openWindow();">회의실 추가</div>
					</div>
	
				</div>
				<div id="meetingCal" style="float: left; margin-left: 250px;">
					<div id="calendar">
							 <div class="calmain">
							    <div class="content-wrap">
							      <div class="content-right" style="margin-left: -60px;">
							        <table id="calendar" align="center"  >
							          <thead>
							            <tr class="btn-wrap clearfix">
							              <td style="text-align: center;">
							              	<div onclick="prev();">
								                <label id="prev" >
								                   &#60;
								                </label>
							                </div>
							              </td>
							              <td align="center" id="current-year-month" colspan="5" ></td>
							              <td style="text-align: center;">
							              	<div onclick="next();">
							                <label id="next">
							                    &#62;
							                </label>
							                </div>
							              </td>
							            </tr>
							            <tr>
							                <td class = "sun" align="center">Sun</td>
							                <td align="center">Mon</td>
							                <td align="center">Tue</td>
							                <td align="center">Wed</td>
							                <td align="center">Thu</td>
							                <td align="center">Fri</td>
							                <td class= "sat" align="center">Sat</td>
							              </tr>
							          </thead>
							          <tbody id="calendar-body" class="calendar-body"></tbody>
							        </table>
							      </div>
							    </div>
							  </div>
						</div>
							<div style="clear: both;">
							<br>
							<br>
								<div id="meetroomInfo">
									회의실 안내
								</div>
								<div style="margin-top:25px; margin-left: -110px; width: 600px; height: 350px; border: 2px solid #EAEAEA; border-radius: 5px;" >
									<div id="meetroomImg">
										
									</div>
									<div id="meetroomData">
										<table id="roomDataTable">
											<tr>
												<th><textarea readonly></textarea></th>
											</tr>
										</table>
									</div>
								</div>
								<br>
								<br>
							</div>
				</div>

			</div>

		</div>
	</section>
	
	<script>
		function loadReservation(roomNo){

			$(".resEle").remove();
			
			var loginUser = ${loginUser.empNo};
			$.ajax({
				url:"loadReservationMain.res",
				type:"post",
				data:{loginUser: loginUser,
					  roomNo : roomNo
					},
				success:function(data) {
					var reservList = data.Reservation;
					var index = 0;
					reservList.forEach(function(){
						var selectYear = reservList[index].resDay.substr(0,4);
						var selectMonth = parseInt(reservList[index].resDay.substr(5,2));

						var selectDate = parseInt(reservList[index].resDay.substr(8,2));
						var selectedId = selectYear + "" + selectMonth + "" + selectDate
						$("#"+selectedId).append("<div class='resEle' style='padding-top:5px; width: 100%; height: 20px; border-radius: 25px; background: #27334C; color: white; font-weight: normal; font-size: 0.8em;'>" +
						reservList[index].startDate+"~"+reservList[index].endDate+"</div>");	
						index += 1;
					})
				},
				error:function(){
					console.log("에러!");
				}
			});
			
			return false;
		}
	</script>
	
	<script>

		function meetingRoomSelect(roomNum){
			var roomNumber = roomNum;	
						
			$.ajax({
				url:"loadRoomInfo.res",
				type:"post",
				data:{roomNumber: roomNumber},
				success:function(data) {
					$("#roomDataTable").children().remove()
					$("#roomDataTable").append("<tr><th><textarea readonly>"+data.Room.roomInfo+"</textarea></th></tr>");
				},
				error:function(){
					console.log("에러!");
				}
			});
			
			return false;
		}


	
		function meetingRoomload(roomNum) {
			var roomNumber = roomNum;
						
			$.ajax({
				url:"loadRoomFile.file",
				type:"post",
				data:{roomNumber: roomNumber},
				success:function(data) {
					var imgName = data.fileList.changeName;
					$("#meetroomImg").children().remove()
					$("#meetroomImg").append("<img style='border-radius: 10px; width:175px; height: 175px; margin-top: 20px;' src='${contextPath}/resources/uploadFiles/"+imgName+"'>");
					loadReservation(roomNum);
				},
				error:function(){
					console.log("에러!");
				}
			});
			
			return false;
		}

	</script>
	
	
	<script>

	var clickedRoom = '101';
	
	$('.meetroom').click(function(){
		clickedRoom = $(this).children('input').val();
		$('.meetroom').removeAttr('id');
		$(this).attr('id', 'on');
		meetingRoomSelect(clickedRoom);
		meetingRoomload(clickedRoom);
	})

	function selectFirstRoom(){
		$(".meetroom").eq(0).attr('id','on');
		meetingRoomload($(".meetroom").eq(0).children('input').val());
		meetingRoomSelect($(".meetroom").eq(0).children('input').val());
	}
	
	function openWindow(){
		//회의실추가 가능  권한 확인후 창 팝업
		jQuery('.meetroom_wrap').fadeIn('slow');
	}
	function addMeetingRoom(){
		document.getElementById("meetingRoomForm").submit();
	}

	selectFirstRoom();
	
	</script>
	<script>
		tree_menu();//Aside에 있는 메뉴 
		$(function(){
				$("#reservationState").addClass('selected');
				$("#reservationAdd").removeClass();
				$("#reservationLog").removeClass();
			})
	</script>
	
	<script>
		var currentTitle = document.getElementById('current-year-month');
		var calendarBody = document.getElementById('calendar-body');
		var today = new Date();
		var first = new Date(today.getFullYear(), today.getMonth(),1);
		var dayList = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
		var monthList = ['January','February','March','April','May','June','July','August','September','October','November','December'];
		var leapYear=[31,29,31,30,31,30,31,31,30,31,30,31]; //윤년
		var notLeapYear=[31,28,31,30,31,30,31,31,30,31,30,31]; //윤년 아님
		var pageFirst = first; 
		var firstMonth = today.getMonth();
		var pageMonth = today.getMonth();
		var todayYear = today.getFullYear();
		var todayDate = today.getDate();
		var calYear = todayYear;		
		var pageYear;
		if(first.getFullYear() % 4 === 0){
		    pageYear = leapYear;
		}else{
		    pageYear = notLeapYear;
		}

		//달력을 출력하는 함수
		function showCalendar(){
		    let monthCnt = 100;
		    let cnt = 1;
		    for(var i = 0; i < 6; i++){
		        var $tr = document.createElement('tr');
		        $tr.setAttribute('id', monthCnt);   
		        for(var j = 0; j < 7; j++){
		            if((i === 0 && j < first.getDay()) || cnt > pageYear[first.getMonth()]){
		                var $td = document.createElement('td');
		                $tr.appendChild($td);     
		            }else{
		                var $td = document.createElement('td');
		                var $tdEle = document.createElement('div');
		                $tdEle.textContent = cnt;

						var eleId = calYear +""+ (pageMonth+1) +""+ cnt  
		                
		                $tdEle.setAttribute('id', eleId); 
		                //표 칸마다 id를 붙이며 id값는 숫자
		                
		                $tdEle.setAttribute('style','width: 85px; height: 55px; border: 1px solid black;')
		                //90x60 스타일을 적용하는 setAttribute
		                
		                $td.appendChild($tdEle);
						//td부분에 요소 추가
		                               
		                $tr.appendChild($td);
		                cnt++;
		            }
		        }
		        monthCnt++;
		        currentTitle.innerHTML = monthList[first.getMonth()] + '&nbsp;&nbsp;&nbsp;&nbsp;'+ first.getFullYear();
		        today = new Date(today.getFullYear(), today.getMonth() + 1, today.getDate());
		        calendarBody.appendChild($tr);
		    }
		}

		showCalendar();

		//달력 달수를 변경할때 달력을 전부 지우고 다시불러오기 위해 지우는 함수
		function removeCalendar(){
		    let catchTr = 100;
		    for(var i = 100; i< 106; i++){
		        var $tr = document.getElementById(catchTr);
		        $tr.remove();
		        catchTr++;
		    }
		}

		//전달로 이동하는 함수
		function prev(){
		    const $divs = document.querySelectorAll('#input-list > div');
		    $divs.forEach(function(e){
		      e.remove();
		    });
		    const $btns = document.querySelectorAll('#input-list > button');
		    $btns.forEach(function(e1){
		      e1.remove();
		    });
		    if(pageFirst.getMonth() === 1){
		        pageFirst = new Date(first.getFullYear()-1, 12, 1);
		        first = pageFirst;
		        if(first.getFullYear() % 4 === 0){
		            pageYear = leapYear;
		        }else{
		            pageYear = notLeapYear;
		        }
		    }else{
		        pageFirst = new Date(first.getFullYear(), first.getMonth()-1, 1);
		        first = pageFirst;
		    }
		    pageMonth = pageFirst.getMonth();
		    calYear = pageFirst.getFullYear();
		    currentTitle.innerHTML = monthList[first.getMonth()] + '&nbsp;&nbsp;&nbsp;&nbsp;'+ first.getFullYear();
		    removeCalendar();
		    showCalendar();
	        showToday();
			meetingRoomSelect(clickedRoom);
			meetingRoomload(clickedRoom);
		}

		//다음달로 이동하는 함수
		function next(){
		    const $divs = document.querySelectorAll('#input-list > div');
		    $divs.forEach(function(e){
		      e.remove();
		    });
		    const $btns = document.querySelectorAll('#input-list > button');
		    $btns.forEach(function(e1){
		      e1.remove();
		    });
		    if(pageFirst.getMonth() === 12){
		        pageFirst = new Date(first.getFullYear()+1, 1, 1);
		        first = pageFirst;
		        if(first.getFullYear() % 4 === 0){
		            pageYear = leapYear;
		        }else{
		            pageYear = notLeapYear;
		        }
		    }else{
		        pageFirst = new Date(first.getFullYear(), first.getMonth()+1, 1);
		        first = pageFirst;
		    }
		    pageMonth = pageFirst.getMonth();
		    calYear = pageFirst.getFullYear();
		    currentTitle.innerHTML = monthList[first.getMonth()] + '&nbsp;&nbsp;&nbsp;&nbsp;'+ first.getFullYear();
		    removeCalendar();
		    showCalendar(); 
	        showToday();
			meetingRoomSelect(clickedRoom);
			meetingRoomload(clickedRoom);
		}

		//오늘인 부분은 오페라 색상으로 변경(연도 구분없음 )
		function showToday(){
		
			if(firstMonth == pageMonth && todayYear == calYear){

			    clickedDate1 = document.getElementById(calYear+""+(pageMonth+1)+""+todayDate);
			    clickedDate1.classList.add('active');
			}
		}
        showToday();
	</script>
	

</body>
</html>