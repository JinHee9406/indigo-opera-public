<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
   <style>	
		#mainDiv {
			float: left;
			width: 1100px;
			margin-left: 20px;
			margin-top: 70px;
		}
		
		#reslistTable th{
			border-bottom: 2px solid #ccc;
			padding: 8px;
		}
		
		#reslistTable td{
			padding-top: 4px;
			padding-bottom: 4px;
			border-bottom: 1px solid #ccc;
			font-size: 0.9em;
		}
		
		#reslistTable tr.head{
			background: #ECECEC;
		}
		
		#reslistTable tr.row:hover {
			background: #27334C;
			color: white;
		}
		#reslistTable {
			border-collapse: collapse; 
			border-spacing: 1px; 
			border-top: 2px solid #ccc;
			margin: 3px;
		}
	
		#containerDiv{
			width: 890px; 
			clear: both; 
			text-align: center;
			margin-left: 100px;
		}
	</style>
</head>
<body>
	<jsp:include page="../common/reservationAside.jsp" />
	<jsp:include page="../common/operaNav.jsp" />
	<section>
		<div id="mainDiv">
			<div id="titleDiv">
				<img id="titleIcon"
					src="/opera/resources/icons/meetingIcon-black.png"><a
					id="titleName">예약이력</a>
			</div>
			<hr>
			<div id="containerDiv" style="">
					<table id="reslistTable" >
						<tr class="head">
							<th width="150px;">번호</th>
							<th width="400px;">예약시간</th>
							<th width="150px;">회의실</th>
							<th width="100px;">신청 인원</th>
							<th width="90px;">이용 여부</th>
						</tr>
						<c:forEach var="reserv" items="${resList}">
						<tr class="row">
							<td>${reserv.listNum}</td>
							<td>${reserv.resDay} ${reserv.startDate} ~ ${reserv.resDay} ${reserv.endDate}</td>
							<td>${reserv.roomName}</td>
							<td>${reserv.personNum}명</td>
							<td>완료</td>
						</tr>
						</c:forEach>
					</table>
				</div>
		</div>
		
			<div id="pagingArea" align="center" style="">
				<c:if test="${pi.currentPage <= 1 }">
					[이전] &nbsp;
				
				</c:if>
				<c:if test="${pi.currentPage > 1 }">
					<c:url var="blistBack" value="reservationList.res">
						<c:param name="pageNum" value="${pi.currentPage -1 }"/>
					</c:url>
					<a href="${blistBack }">[이전]</a> &nbsp;
				</c:if>
				<c:forEach var="p" begin="${pi.startPage }" end="${pi.endPage }">
					<c:if test="${p eq pi.currentPage }">
						<font color="red" size="4"><b>[${p}]</b></font>
					</c:if>
					<c:if test="${p ne pi.currentPage }">
						<c:url var="blistCheck" value="reservationList.res">
							<c:param name="pageNum" value="${p }"/>
						</c:url>				
						<a href="${blistCheck }">${p}</a>
					</c:if>
				</c:forEach>
				<c:if test="${pi.currentPage >= pi.maxPage }">
					&nbsp;[다음]
				</c:if>
				<c:if test="${pi.currentPage < pi.maxPage }">
					<c:url var="blistEnd" value="reservationList.res">
						<c:param name="pageNum" value="${pi.currentPage + 1 }"/>
					</c:url>
					&nbsp; <a href="${blistEnd}">[다음]</a>
				</c:if>
			</div>
			
	</section>
	<script>
		tree_menu2();//Aside에 있는 메뉴 
		$(function(){
				$("#reservationLog").addClass('selected');
				$("#reservationAdd").removeClass();
				$("#reservationState").removeClass();
			})
	</script>
</body>
</html>