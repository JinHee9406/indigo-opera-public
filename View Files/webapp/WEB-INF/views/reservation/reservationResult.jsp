<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/knowledgeStyle.css">
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
   <style>
	   #okBtn{
	   		width: 100px;
	   		height: 30px;
	   		border-radius: 5px;
	   		background: #27334C;
	   		font-weight: bold;
	   		color: white;
	   }
   
   </style>
</head>
<body>
	<jsp:include page="../common/reservationAside.jsp" />
	<jsp:include page="../common/operaNav.jsp"/>
	<section>
		<div style="text-align: center; margin-left: 100px; margin-top: 100px;">
			<c:out value="${ message }"/><br>
			<c:if test="${key == 'addmeetRoom'}">
				<button id="okBtn" onclick="location.href='reservationMain.res'">확인</button>
			</c:if>
			<c:if test="${key == 'addReservation'}">
				<button id="okBtn" onclick="location.href='reservationAdd.res'">확인</button>
			</c:if>
			<c:if test="${key == 'removeReservation'}">
				<button id="okBtn" onclick="location.href='reservationAdd.res'">확인</button>
			</c:if>
		</div>
	</section>
</body>
</html>