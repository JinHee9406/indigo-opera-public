<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/noticeStyle.css">
<title>Opera GroupWare</title>
<style type="text/css">
	#writeBtn:hover{
		cursor: pointer;
	}
	td:hover{
		cursor: pointer;
	}
	.pagingBtn:hover{
		cursor: pointer;
	}
	.pagingSide:hover{
		cursor: pointer;
	}
	#searchBtn:hover{
		cursor:pointer;
	}
	.insertBtn:hover{
		cursor:pointer;
	}
	.debtn:hover{
		cursor:pointer;
	}
</style>
</head>
<body>
   <jsp:include page="../common/operaNav.jsp"/>   
   <jsp:include page="../common/noticeAside.jsp"/>   
   <section >
      <div id="mainDiv" style="width:950px;" >
         <div id="titleDiv">
            <a id="titleName" style="color: #57585E;">일반 게시판</a>
         </div>
         <hr style="margin-top: 1%; margin-bottom: 2%;">
         <!-- <div style="border: 0.5px solid #C4C4C4; margin: 10px; width: 950px; height: 35px; background: white;">
            <a style="float:left; padding: 10px; font-weight: 500; margin-left: 5px;">출근통계</a>
            <div style="float:left; margin-left: 4px; margin-right: 5px; border: 2px solid #C4C4C4; width:0.05px; height: 32px; "></div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray; margin-left: 5px;">당일</div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray;margin-left: 5px;">3일</div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray;margin-left: 5px;">7일</div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray;margin-left: 5px;">30일</div>
            <div style="float:left; padding: 8px;">
               <select style="background: white; color: gray; font-size: 15px; width: 120px;">
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
               </select>
               ~
            </div>
            
            <div style="float:left; padding: 8px;">
               <select style="background: white; color: gray; font-size: 15px; width: 120px;">
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
               </select>
            </div>
            <div style="float:left; padding: 5px; text-align:center; font-size: 14px; margin-left: 10px; margin-top: 6px; width: 55px; height: 15px; background: #27334C; color: white; "
            id="searchBtn">
               검색
            </div> -->
            <!-- <br>
            <br>
            <br> -->
            <div style="width: 1150px; clear: both; text-align: center; ">
               <table id="listTable" style="margin-left:0;">
                  <tr class="head" style="background-color:#27334C; color:white;">
                     <th width="143px" style="border: 2px; border-style:solid;  border-color: lightgray; ">작성자</th>
                     <th  width="429px" style="border: 2px; border-style:solid;  border-color: lightgray; text-align:left; margin-left: 10px; background: white; color:#57585E;" colspan="3">${notice.empName }</th>
                     <th width="143px" style="border: 2px; border-style:solid;  border-color: lightgray;">작성일</th>
                     <th width="429px" style="border: 2px; border-style:solid;  border-color: lightgray; text-align:left; margin-left: 10px; background: white; color:#57585E;" colspan="3">${notice.boardDate }</th>
                  </tr>
                  <tr class="head" style="background-color:#27334C; color:white;">
                     <th width="143px" style="border: 2px; border-style:solid;  border-color: lightgray; ">제목</th>
                     <th width="1007px" style="border: 2px; border-style:solid;  border-color: lightgray; text-align:left; margin-left: 10px; background: white; color:#57585E;" colspan="7">${notice.boardTitle }</th>
                  </tr>
                  <tr >
                     <th width="1150px" height="400px" style="border: 2px; border-style:solid;  border-color: lightgray; text-align:left;vertical-align:top;  margin-left: 10px; background: white; color:#57585E;" colspan="8" rowspan="7">
                     <div>
                     ${notice.boardContent}
                     </div>
                     </th>
                  </tr>
                 
               </table>
              <div style="width:1142px; height:200px; border:1.5px solid lightgray;">
              <div>
	              	<div 
	              	style="width:900px; height:40px; border:1.5px solid lightgray; 
	              	border-left:0; float: left; background-color:#27334C; color:white;">
	              	<p style="margin-top: 10px;">파일명</p>
	              	</div>
	              	<div 
	              	style="width:239.5px; height:40px; border:1.5px solid lightgray; 
	              	border-left:0; border-right:1px solid lightgray; float: left; 
	              	background-color:#27334C; color:white;"><p style="margin-top: 10px;">크기</p>
	              	</div>
	              	<div style="width:900px; height:160px; border:1.5px solid lightgray; 
	              	border-left:0; float: left; background-color:white; color:#57585E;">
	              	<c:forEach var="fileList" items="${fileList }">
                        <c:if test="${not empty fileList.fileCode}">
                           <div onclick="downloadFile(this);" style=" height: 25px; cursor: pointer; font-size: 0.9em;"><i class='fas fa-file'></i>&nbsp;&nbsp;<input id="fileCode" type="hidden" name="fileCode" value="${fileList.fileCode}">${fileList.originName}</div>
                        </c:if>
                     </c:forEach>
	              	</div>
	              	<div 
	              	style="width:239.5px; height:160px; border:1.5px solid lightgray; 
	              	border-left:0; border-right:1px solid lightgray; float: left; 
	              	background-color:white; color:#57585E;">
	              	<c:forEach var="fileList" items="${fileList }">
                        <c:if test="${not empty fileList.fileCode}">
                           <div style=" height: 25px; cursor: pointer; font-size: 0.9em;"><i class='fas fa-file'></i>&nbsp;&nbsp;${fileList.fileSize / 1000} KB</div>
                        </c:if>
                     </c:forEach>
	              	</div>
	              	
	              	
              </div>
              </div>
              <br><br>
              <c:if test="${not empty reply}">
              
              <div style="width:1142px; height:150px;">
              <div>
              		<div 
	              	style="width:298px; height:40px; border:1.5px solid lightgray; 
	              	border-left:0; float: left; background-color:#27334C; color:white;">
	              	<p style="margin-top: 10px;">작성자</p>
	              	</div>
	              	<div 
	              	style="width:600px; height:40px; border:1.5px solid lightgray; 
	              	border-left:0; float: left; background-color:#27334C; color:white;">
	              	<p style="margin-top: 10px;">내용</p>
	              	</div>
	              	<div 
	              	style="width:239.5px; height:40px; border:1.5px solid lightgray; 
	              	border-left:0; border-right:1px solid lightgray; float: left; 
	              	background-color:#27334C; color:white;"><p style="margin-top: 10px;">작성일</p>
	              	</div>
	              	
	              	<c:forEach var="rep" items="${reply}">
	              	<div style="width:295px; height:20px; border:1.5px solid lightgray; 
	              	 float: left; background-color:white; color:#57585E;">
	              	${rep.empName }
	              	</div>
	              	<div style="width:600px; height:20px; border:1.5px solid lightgray; border-left:0;
	              	 float: left; background-color:white; color:#57585E;">
	              	${rep.replyDetail} 
	              	</div>
	              	<c:if test="${loginUser.empNo ne rep.empNo}">
	              	<div 
	              	style="width:239.5px; height:20px; border:1.5px solid lightgray; border-left:0;
	              	 border-right:1px solid lightgray; float: left; 
	              	background-color:white; color:#57585E;">
	              	${rep.replyDate }
	              	</div>
	              	</c:if>
	              	<c:if test="${loginUser.empNo eq rep.empNo}">
	              	<div 
	              	style="width:239.5px; height:20px; border:1.5px solid lightgray; border-left:0;
	              	 border-right:1px solid lightgray; float: left; 
	              	background-color:white; color:#57585E;">
	              	${rep.replyDate } <a onclick="location.href='deleteReply.not?replyNo=${rep.replyNo}&boardNo=${notice.boardNo}'" style="color:red;" class="debtn">삭제</a> 
	              	</div>
	              	</c:if>
	              	</c:forEach>
	              	
              </div>
              <br>
              <br>
              
              </div>
              <div id="pagingArea" align="center" style="">
				<c:if test="${pi.currentPage <= 1 }">
					[이전] &nbsp;
				
				</c:if>
				<c:if test="${pi.currentPage > 1 }">
					<c:url var="blistBack" value="commonDetail.not?pageNo=${pi.currentPage - 1 }&boardNo=${notice.boardNo}">
						<c:param name="pageNum" value="${pi.currentPage -1 }"/>
					</c:url>
					<a href="${blistBack }">[이전]</a> &nbsp;
				</c:if>
				<c:forEach var="p" begin="${pi.startPage }" end="${pi.endPage }">
					<c:if test="${p eq pi.currentPage }">
						<font color="red" size="4"><b>[${p}]</b></font>
					</c:if>
					<c:if test="${p ne pi.currentPage }">
						<c:url var="blistCheck" value="commonDetail.not?pageNo=${p}&boardNo=${notice.boardNo}">
							<c:param name="pageNum" value="${p }"/>
						</c:url>				
						<a href="${blistCheck }">${p}</a>
					</c:if>
				</c:forEach>
				<c:if test="${pi.currentPage >= pi.maxPage }">
					&nbsp;[다음]
				</c:if>
				<c:if test="${pi.currentPage < pi.maxPage }">
					<c:url var="blistEnd" value="commonDetail.not?pageNo=${pi.currentPage + 1 }&boardNo=${notice.boardNo}">
						<c:param name="pageNum" value="${pi.currentPage + 1 }"/>
					</c:url>
					&nbsp; <a href="${blistEnd}">[다음]</a>
				</c:if>
			</div>
			</c:if>
              <br>
              <br>
              <form method="post" action="insertReply.not?boardNo=${notice.boardNo}" style="width:980px; height:100px;" id="insertForm">
              	 <div 
	              	style="width:120.5px; height:40px; border:1.5px solid lightgray; 
	              	border-right:1px solid lightgray; float: left; 
	              	background-color:white; color:#57585E; margin-left: 175px; text-align: center; line-height: 40px;">
	              	${loginUser.empName }
	              	</div>
	              	<div 
	              	style="width:500.5px; height:40px; border:1.5px solid lightgray; 
	              	border-right:1px solid lightgray; float: left; 
	              	background-color:white; color:#57585E; margin-left: 5px; text-align: center; line-height: 40px;">
	              		<input type="text" size="70" style="padding: 10.5px;" name="replyDetail">
	              	</div>
	              	<div 
	              	style="width:70.5px; height:40px; border:1.5px solid lightgray; 
	              	border-right:1px solid lightgray; float: left; 
	              	background-color:white; color:#57585E; margin-left: 5px; text-align: center; line-height: 40px;" onclick="submit();"  class="insertBtn">
	              		제출
	              	</div>
              </form>
              
              <div style="width: 100px; height:30px; border:1.5px solid lightgray; background: #27334C; color:white; float: right; margin-right: 10px; margin-top: 10px;">
              	<p style="margin-top: 6px;" onclick="location.href='commonNotice.not'">목록으로</p>
              </div>    
               <br><br>    
               <br><br>
                 
         </div>
      </div>
   </section>
   <!-- <script>
      function openAside(){

      }
      $("#listTable tr").click(function(){
         location.href="knowledgeDetail.me";
      })
   </script> -->
   
   <script type="text/javascript">
   function downloadFile(da){
	      var fileNum = da.childNodes[2].value;
	      location.href="fileDownload.file?fileNo="+fileNum;
	   }
   
   function submit() {
		
		$("#insertForm").submit();
	}
   </script>
</body>
</html>