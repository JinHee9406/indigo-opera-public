<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
<link rel="stylesheet" href="/opera/resources/css/progressgraphStyle.css">
<style>
		#mainDiv {
			float: left;
			width: 1100px;
			margin-left: 10px;
			margin-top: 70px;
		}
		
		.shareEle{
			cursor: pointer;
			transition: all ease 0.35s 0s;
		}
		
		.shareEle tr.row:hover, .shareEle:hover {
			background: #E5E5E5;
			border: 2px solid #E61358;
		}
		
		.num{
			color: #E61358;
		}
</style>
</head>
<body>
	<script>
		function updateDonutChart (el, percent, donut) {
		    percent = Math.round(percent);
		    if (percent > 100) {
		        percent = 100;
		    } else if (percent < 0) {
		        percent = 0;
		    }
		    var deg = Math.round(360 * (percent / 100));
	
		    if (percent > 50) {
		        $(el + ' .pie').css('clip', 'rect(auto, auto, auto, auto)');
		        $(el + ' .right-side').css('transform', 'rotate(180deg)');
		    } else {
		        $(el + ' .pie').css('clip', 'rect(0, 1em, 1em, 0.5em)');
		        $(el + ' .right-side').css('transform', 'rotate(0deg)');
		    }
		    if (donut) {
		        $(el + ' .right-side').css('border-width', '0.1em');
		        $(el + ' .left-side').css('border-width', '0.1em');
		        $(el + ' .shadow').css('border-width', '0.1em');
		    } else {
		        $(el + ' .right-side').css('border-width', '0.5em');
		        $(el + ' .left-side').css('border-width', '0.5em');
		        $(el + ' .shadow').css('border-width', '0.5em');
		    }
		    $(el + ' .num').text(percent);
		    $(el + ' .left-side').css('transform', 'rotate(' + deg + 'deg)');
		}
	
		// Pass in a number for the percent
		//updateDonutChart('#specificChart', 55, true);
		//Ignore the rest, it's for the input and checkbox
	
		$('#percent').change(function () {
		    var percent = $(this).val();
		    var donut = $('#donut input').is(':checked');
		    updateDonutChart('#specificChart', percent, donut);
		}).keyup(function () {
		    var percent = $(this).val();
		    var donut = $('#donut input').is(':checked');
		    updateDonutChart('#specificChart', percent, donut);
		});;
	
		$('#donut input').change(function () {
		    var donut = $('#donut input').is(':checked');
		    var percent = $("#percent").val();
		    if (donut) {
		        $('#donut span').html('Donut');
		    } else {
		        $('#donut span').html('Pie');
		    }
		    updateDonutChart('#specificChart', percent, donut);
		});
	</script>
	<jsp:include page="../common/shareAside.jsp" />
	<jsp:include page="../common/operaNav.jsp" />
	<section>
		<div id="mainDiv">
			<div id="titleDiv">
				<img id="titleIcon" src="/opera/resources/icons/shareworks-black.png">
				<a id="titleName">업무공유</a>
			</div>
			<hr>
			<div>
				<div style="background:#EFEFEF; margin-left: 315px; margin-top: 10px; margin-bottom: 10px; text-align: center; padding: 15px; width: 420px; height: 25px; border: 2px solid #C4C4C4;">
					<a style="font-weight: bold">업무검색</a>&nbsp;&nbsp;|&nbsp;&nbsp;
					<input type="text" name="searchText" style=" padding: 5px; border-radius: 5px; width: 220px;" placeholder="검색할 업무명을 입력해주세요">
					<button style="padding: 5px; color: white; background: #27334C">검색하기</button>
				</div>
			</div>
			<div>
				<div style="background: #ECECEC; margin-left: 50px; width: 1000px; height: 40px; border: 1px solid #606060; border-radius: 5px;">
				<table style="text-align:center; margin-top: 10px;">
					<tr>
						<td width="60px">번호</td>
						<td width="100px">업무기간</td>
						<td width="620px">업무명 및 진행도</td>
						<td width="120px">개설자</td>
						<td width="100px">참여자 수</td>
					</tr>
				</table>
				</div>
			</div>
			<div id="shareBody">
			<!-- 여기서부터 append -->
			<c:forEach var="share" items="${workList}">
				<div onclick="location.href='shareworksDetail.sha?workNum=${share.workNo}'" class="shareEle" style="margin-left: 50px; margin-top: 15px; width: 1000px; height: 270px; border: 1px solid black; border-radius: 10px;">
					<div>
						<table style="text-align:center; margin-top: 10px;">
							<tr>
								<td width="60px">${share.postNum}</td>
								<td width="100px">${share.workSDate} ~<br> ${share.workEDate}</td>
								<td width="620px"><br>${share.workName}
								<div id="specificChart${share.postNum}" class="donut-size" style="margin-top: 25px;">
									  <div class="pie-wrapper">
									    <span class="label">
									    	<div style="margin-top: -10px; font-size: 0.5em; margin-bottom: -75px;">진행중</div>
									        <span class="num">0</span><span class="smaller">%
									        </span>
										</span>
									    <div class="pie">
									      <div class="left-side half-circle"></div>
									      <div class="right-side half-circle"></div>
									    </div>
										<div class="shadow"></div>
									  </div>
								</div></td>
								<td width="120px">${share.empUserName} ${share.empUserJob}</td>
								<td width="100px">${share.userNum} 명</td>
							</tr>
						</table>
					</div>
				</div>
						<script>
							updateDonutChart('#specificChart'+${share.postNum}, ${share.workProg}, true);
						</script>
				</c:forEach>
				<!-- append끝 -->
				

			</div>
		</div>
	</section>

</body>
</html>