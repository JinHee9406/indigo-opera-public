<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/knowledgeStyle.css">
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet">
<title>Opera GroupWare</title>
<style>
   	#mainDiv {
			float: left;
			margin-left: 10px;
			margin-top: 120px;
		}
	.detailBtn{
		width: 90px;
		height: 20px;
		border-radius: 5px;
		border: 1px solid #ccc;
		margin-right: 8px;
		margin-bottom: 5px;
		text-align: center;
		font-size: 0.9em;
		font-weight: bold;
		float: right;
		padding-top: 3px;
		border: 1px solid #9F9F9F;
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
		border-radius: 5px;
		cursor: pointer;
	}
	
	#replyContainer{
		border-radius: 5px; 
		margin-top: 10px; 
		margin-bottom: 10px; 
		margin-left: 25px; 
		width: 95%; 
		background:#DCDCDC;
	}
	
	#replyText{
		float: left;
		margin: 40px; 
		margin-bottom: 20px; 
		width: 800px; 
		height: 80px; 
		resize: none;
	}
	#addReplyBtn{
		float: left;
		width: 90px; 
		margin-top: 40px;
		height: 60px; 
		background: #27334C;
		color: white;
		font-weight: bold;
	}
	
	.reply{
		width: 950px; 
		height: 100%;
		min-height: 40px;
		border: 1px solid #616161; 
		border-radius: 5px; 
		background:#F0F0F0;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	
	.reply a{
		margin: 10px;
		margin-left: 40px;
		margin-bottom: 10px;
		float: left;
	}
	.replyName{
		font-weight: bold;
		color: #555555;
	}
	.replyContent{
		margin-top: 10px;
		width: 60%;
		outline: none;
		resize: none;
		background: none;
		border: none;
	}
	
	.replyDate{
		font-size: 0.8em;
	}
	

</style>
</head>
<body>
	<jsp:include page="../common/shareAside.jsp" />
	<section>
		<div id="mainDiv">
				<div id="titleDiv">
					<a id="titleName">내용 보기</a>
				</div>
				<div style="margin-bottom: 15px;">
					<div class="detailBtn" onclick="location.href='shareworksDetail.sha?workNum=${noticeDetail.workNo}'">뒤로</div>
					<div class="detailBtn" onclick="removeNotice();">글 지우기</div>
						<form id="fixElements" action="noticeFix.sha" method="post">
							<input type="hidden" name="noticeNo" value="${noticeDetail.noticeNo}">
							<input type="hidden" name="noticeTitle" value="${noticeDetail.noticeTitle}">
							<input type="hidden" name="noticeDetail" value='<c:out value="${noticeDetail.noticeDetail}"/>'>
							<input type="hidden" name="noticeDate" value="${noticeDetail.noticeDate}">
							<input type="hidden" name="fileCode" value="${noticeDetail.fileCode}">
							<input type="hidden" name="workNo" value="${noticeDetail.workNo}">
							<input type="hidden" name="noticeUser" value="${noticeDetail.noticeUser}">
							<input type="hidden" name="noticeUserName" value="${noticeDetail.noticeUserName}">
							<input type="hidden" name="userJobName" value="${noticeDetail.userJobName}">
						</form>
					<div class="detailBtn" onclick="fixNotice();">글 수정</div>
				</div>
				<br>
				<div id="detailHeader" style="width: 100%; height: 100px; border-top: 2px solid #848484; border-bottom: 2px solid #848484; background: #F9F9F9;">
					<div id="detailTitle" style="margin-top: 15px; margin-left: 40px; font-size: 1.2em;"><h2><c:out value="${ noticeDetail.noticeTitle }"/></h2></div>
					<div id="detailName" style="margin-left: 40px; font-size: 0.9em; margin-top: 10px; color: #909090;"><c:out value="${ noticeDetail.noticeUserName} ${ noticeDetail.userJobName }"/><br>
						<div style="float:right; margin-right: 55px; color: black;">조회수&nbsp;&nbsp;&nbsp;<c:out value="${ noticeDetail.noticeCnt }"/></div>
						<div style="float:right; margin-right: 35px; color: black;"><c:out value="${ noticeDetail.noticeDate }"/></div>
						<div style="float:right; margin-right: 35px; color: black;">공지사항</div>	
					</div>
				</div>
				<div id="knowledgeDetail" style="width: 100%; border: 1px solid #B0B0B0; border-radius: 5px; margin-bottom: 50px;">
					<div id="detailCon" style="height: 100%; padding: 10px;">
						${noticeDetail.noticeDetail}
					</div>
				</div>
				<c:if test="${noticeDetail.fileCode != null}">
					<div onclick="downloadFile();" style="cursor: pointer; font-size: 0.9em; margin-top: -30px;"><i class='fas fa-file'></i>&nbsp;&nbsp;<input id="fileCode" type="hidden" name="fileCode" value="${noticeDetail.fileCode}">${noticeDetail.fileName}</div>
				</c:if>
				<br><br>
		</div>

	</section>
	<jsp:include page="../common/operaNav.jsp"/>
		
	<script>
		function fixNotice(){
			if('${loginUser.empNo}' == '${noticeDetail.noticeUser}'){
				if(confirm("공지사항을 수정하시겠습니까?") == true){
					document.getElementById("fixElements").submit();
				}
			} else {
				alert("글을 수정할수 있는 권한이 없습니다.");
			}
			
		}

		function removeNotice(){
			if('${loginUser.empNo}' == '${noticeDetail.noticeUser}'){
				if(confirm("공지사항을 지우시겠습니까?") == true){
					location.href = "removeNotice.sha?noticeNum=${noticeDetail.noticeNo}&&workNum=${noticeDetail.workNo}"
				}
			} else {
				alert("글을 지울수 있는 권한이 없습니다.");
			}
		}

		function downloadFile(){
			var fileNum = document.getElementById("fileCode").value;
			location.href="fileDownload.file?fileNo="+fileNum;
		}
	</script>
</body>
</html>