<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script type="text/javascript" src="/opera/resources/se2/js/service/HuskyEZCreator.js" charset="utf-8"></script>
   <link rel="stylesheet" href="/opera/resources/css/knowledgeStyle.css">
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
 
<title>Opera GroupWare</title>
<style>
   	#mainDiv {
			float: left;
			margin-top: 70px;
		}
		
	#addKnowTable{
		width: 100%;
		border-collapse: collapse;
		border-top: 2px solid #848484;
		border-bottom: 2px solid #848484;
		
	}
	
	#addKnowTable th{
		padding: 10px;
		background: #E5E5E5;
		text-align: right;
		padding-right: 15px;
		width: 190px;
	}
	
	#addKnowTable td{
		background: #F9F9F9;
		padding-left: 10px;
	}
	
	.userName{
		font-size: 0.9em;
	}
	
	#saveBtn button{
		width: 100px; 
		height: 40px; 
		background: #27334C; 
		color: white; 
		font-weight: bold; 
		border-radius: 15px; 
		outline: none;
	}
</style>
</head>
<body>
	<jsp:include page="../common/shareAside.jsp"/>
	<jsp:include page="../common/operaNav.jsp"/>	
	<section>
		<div id="mainDiv">
			<div id="titleDiv">
				<img id="titleIcon" src="/opera/resources/icons/shareworks-black.png">
				<a id="titleName">${workName} 공지사항 작성</a>
			</div>
			<form id="shareWorkNotice" action="shareWorkNotice.sha" method="post" enctype="multipart/form-data">
			<div id="titleDiv">
				<a id="titleName"></a>
			</div>
			<table id="addKnowTable">
				<tr>
					<th>제목</th>
					<td><input type="text" name="noticeTitle" placeholder="제목을  입력해주세요." style="width: 300px; height: 25px;"></td>
				</tr>
				<tr>
					<th>작성자</th>
					<td class="userName"><c:out value="${loginUser.empName} ${loginUser.empJobName}"/><input type="hidden" name="noticeUser" value="${loginUser.empNo}">
					<input type="hidden" name="knowledgeUserName" value="${loginUser.empName} ${loginUser.empJobName}"></td>
					
				</tr>
				<tr>
					<th>구분</th>
					<td>
						공지사항
						<input type="hidden" name="workNo" value="${getWorkNo}">
					</td>
				</tr>
			</table>
			<textarea name="noticeDetail" id="ir1" rows="10" cols="100" style="width:1090px;"></textarea>
			
			<input type="file" name="fileCode">
			</form>
			<div id="saveBtn" style="text-align:right;">
				<button onclick="submitInputs();">저장</button>
				<button onclick="back();" style="background: white; color: black;">작성취소</button>
			</div>
		</div>


	    <script type="text/javascript">
	        var oEditors = [];
	        nhn.husky.EZCreator.createInIFrame({
	         oAppRef: oEditors,
	         elPlaceHolder: "ir1",
	         sSkinURI: "/opera/resources/se2/SmartEditor2Skin.html",
	         //se2폴더 경로(프로젝트안에 넣어놓는게 좋음)
	         fCreator: "createSEditor2"
	        });
	    </script>
	    <script>
		    function submitContents(elClickedObj) {
		    	 // 에디터의 내용이 textarea에 적용된다.
		    	 oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
				
		    	 // 에디터의 내용에 대한 값 검증은 이곳에서
		    	 // document.getElementById("ir1").value를 이용해서 처리한다.
		
		    	 try {
		    	     elClickedObj.form.submit();
		    	 } catch(e) {}
		    }
	    </script>
	    <script>
			function submitInputs(){
				var noticeForm = document.getElementById("shareWorkNotice");
				oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
		 		console.log(document.getElementById("ir1").value);
		 		var editerArea = document.getElementById("ir1");
		 		noticeForm.submit();
			}

			function back(){
				location.href='shareworksDetail.sha?workNum=${getWorkNo}';
				
			}
	    </script>
	</section>
</body>
</html>