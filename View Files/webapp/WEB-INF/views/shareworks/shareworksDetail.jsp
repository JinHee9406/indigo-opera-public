<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
<link rel="stylesheet" href="/opera/resources/css/progressgraphStyle.css">
<link rel="stylesheet" href="/opera/resources/css/shareWorkDetailStyle.css">
<style>
		#mainDiv {
			float: left;
			width: 1100px;
			margin-left: 20px;
			margin-top: 90px;
		}
		
		.saveBtn{
			width: 50px;
			height: 15px;
			background: #E5E5E5;
			font-weight: bold;
			border-radius: 5px;
			text-align: center;
			border: 1px solid #838080;
			cursor: pointer;
			font-size: 0.8em;
		}
		
		.filePart{
			font-size: 0.8em;
			margin-left: -30px;
			cursor: pointer;
		}
</style>
</head>
<body>
	<script>
		function updateDonutChart (el, percent, donut) {

		    percent = Math.round(percent);
		    if (percent > 100) {
		        percent = 100;
		    } else if (percent < 0) {
		        percent = 0;
		    }
		    var deg = Math.round(360 * (percent / 100));
		
		    if (percent > 50) {
		        $(el + ' .pie').css('clip', 'rect(auto, auto, auto, auto)');
		        $(el + ' .right-side').css('transform', 'rotate(180deg)');
		    } else {
		        $(el + ' .pie').css('clip', 'rect(0, 1em, 1em, 0.5em)');
		        $(el + ' .right-side').css('transform', 'rotate(0deg)');
		    }
		    if (donut) {
		        $(el + ' .right-side').css('border-width', '0.1em');
		        $(el + ' .left-side').css('border-width', '0.1em');
		        $(el + ' .shadow').css('border-width', '0.1em');
		    } else {
		        $(el + ' .right-side').css('border-width', '0.5em');
		        $(el + ' .left-side').css('border-width', '0.5em');
		        $(el + ' .shadow').css('border-width', '0.5em');
		    }
		    $(el + ' .num').text(percent);
		    $(el + ' .left-side').css('transform', 'rotate(' + deg + 'deg)');
		}

		function changeProg(){
		    var percent = $('#percent').val();
		    var donut = $('#donut input').is(':checked');
		    updateDonutChart('#specificChart', percent, donut);
		}
		
		$('#donut input').change(function () {
		    var donut = $('#donut input').is(':checked');
		    var percent = $("#percent").val();
		    if (donut) {
		        $('#donut span').html('Donut');
		    } else {
		        $('#donut span').html('Pie');
		    }
		    updateDonutChart('#specificChart', percent, donut);
		});

	</script>
	<jsp:include page="../common/shareAside.jsp" />
	<jsp:include page="addMember.jsp"/>
		<div class="report_wrap" style="display: none;">
			<div class="dark_bg" onclick="jQuery('.report_wrap').fadeOut('slow')"></div>
			<div class="report_box">
				<div id="addWindowTitle2">
				<div style="margin-bottom: 15px;">
					<div class="closeBtn2" onclick="jQuery('.report_wrap').fadeOut('slow')">X</div>
					<a style="font-size: 1.3em;">업무보고</a>
					</div>
				</div>
				<br>
				<div style="clear:both; width: 700px; height: 250px; margin-left: 25px;">
				<form id="reportForm" action="addWorkReport.sha" method="post" enctype="multipart/form-data">
					<table id="reportTable">
						<tr style="height: 50px;">
							<th>구분</th>
							<td>
								<input name="workNo" type="hidden" value="${workDetail.workNo}">
								<input name="empNo" type="hidden" value="${loginUser.empNo}">
								<select name="reportType">
									<option value="요청">요청</option>
									<option value="결과">결과</option>
									<option value="피드백">피드백</option>
								</select>
							</td>
						</tr>					
						<tr>
							<th>내용</th>
							<td>
								<textarea name="reportDetail" style="width:100%; height: 150px; resize: none;"></textarea>
							</td>
						</tr>					
						<tr style="height: 50px;" >
							<th>첨부파일</th>
							<td>
								<input type="file" name="reportFile">
							</td>
						</tr>
					</table>
					</form>
					<div id="saveBtn2">
						<button onclick="saveReport();">저장</button>
					</div>
				</div>
			</div>
		</div>
	
	
	
	<section>
		<div id="mainDiv">
			<div id="titleDiv">
				<img id="titleIcon" src="/opera/resources/icons/shareworks-black.png">
				<a id="titleName">${workDetail.workName}</a>
			</div>
			<hr>
			<div style="margin-left: 100px;">
				<form id="workDetailForm" action="updateWork.sha" method="post">
				<div id="progressContainer" class="containerEle">
				<input name="workNo" type="hidden" value="${workDetail.workNo}">
					<div id="specificChart" class="donut-size" style="margin-top: 25px;">
							<div class="pie-wrapper">
									    <span class="label">
									    	<div style="margin-top: -10px; font-size: 0.5em; margin-bottom: -75px;">진행중</div>
									        <span class="num">${workDetail.workProg}</span><span class="smaller">%
									        </span>
										</span>
									    <div class="pie">
									      <div class="left-side half-circle"></div>
									      <div class="right-side half-circle"></div>
									    </div>
										<div class="shadow"></div>
							</div>
					</div>
					<div style="text-align: center; margin-top: 10px;">업무 진행도</div>
					<c:if test="${loginUser.empNo == workDetail.empNo}">
					<input type="number" name="workProg" id="percent" onclick="changeProg();" value="${workDetail.workProg}">
					</c:if>
					<c:if test="${loginUser.empNo != workDetail.empNo}">
					<input type="number" id="percent" onclick="changeProg();" value="${workDetail.workProg}" readonly>
					</c:if>
					
				 	<label id="donut" style="clear:both; display:none;"><input type="checkbox" checked> <span>Donut</span></label>
					<c:if test="${loginUser.empNo == workDetail.empNo}">
						<div class="saveBtn" style="margin-left: 95px;">저장</div>
					</c:if>
				</div>
				<div id="detailContainer" class="containerEle">
					<div>
						<table id="detailData" style="width: 100%; margin-top: -0.5px;">
							<tr>
								<th>
									업무 개설자
								</th>
								<td>
								${workDetail.empUserName} ${workDetail.empUserJob}
								</td>
								<td></td>
							</tr>
							<tr>
								<th>
									업무기간
								</th>
								<c:if test="${loginUser.empNo == workDetail.empNo}">
								<td>
									<input  name="workSDate" type="date" value="${workDetail.workSDate}" > ~ <input  name="workEDate" type="date" value="${workDetail.workEDate}">
								</td>
									<td><div class="saveBtn" onclick="updateWork();">저장</div></td>
								</c:if>
								<c:if test="${loginUser.empNo != workDetail.empNo}">
								<td>
									<input  type="date" value="${workDetail.workSDate}" readonly> ~ <input type="date" value="${workDetail.workEDate}" readonly>
								</td>
								</c:if>
							</tr>
							<tr>
								<th>
									참여자
								</th>
								<td>	
									<input id="workUser" name="workUser" type="hidden" value="${memberListNo}">
									<input type="text" id="workUserName" value="${workDetail.workUser}" style="margin-top: 10px; resize: none; width: 310px; height: 60px;">
								</td>
								<c:if test="${loginUser.empNo == workDetail.empNo}">
									<td><div class="saveBtn" onclick="openWindow()">수정</div>
										<div class="saveBtn"  onclick="updateWork();" style="margin-top: 5px;" >저장</div>
									</td>
								</c:if>
							</tr>
							<tr>
								<th>
									 업무 설명
								</th>
								<c:if test="${loginUser.empNo == workDetail.empNo}">
								<td>
									<textarea name="workDetail" style="resize: none; width: 312px; height: 160px; margin-top: 3px;">${workDetail.workDetail}</textarea>
								</td>
									<td><div style="resize: none;" class="saveBtn"  onclick="updateWork();">저장</div></td>
								</c:if>
								<c:if test="${loginUser.empNo != workDetail.empNo}">
								<td>
									<textarea style="resize: none; width: 310px; height: 160px;" readonly>${workDetail.workDetail}</textarea>
								</td>
								</c:if>
							</tr>
						</table>				
					</div>
				</div>
				</form>
				<div style="clear: both;"><br><hr style="width: 870px;"></div>
				<div id="workContainer" class="containerEle"  style="width: 880px;">
					<div id="workNotice" onclick="selectNoticeMenu();">
						업무공지사항
					</div>
					<div id="workList" onclick="selectWorkMenu();">
						업무수행목록
					</div>
					<div id="noticeContainer">
							<br><br>
							<div>
								<div class="addPostButton" style="float: right;">
									<form id="hiddenWorkData" action="showWorkNotice.sha">
										<input type="hidden" name="workTitle" value="${workDetail.workName}">
										<input type="hidden" name="workNo" value="${workDetail.workNo}">
									</form>
									<c:if test="${loginUser.empNo == workDetail.empNo}">
										<button onclick="addWorkNotice();">추가</button>
									</c:if>
								</div>
								<div id="serachNoticeCon">
									
									<select style="height:30px; border-radius: 5px; border: 2px solid black;">
										<option>제목</option>
										<option>내용</option>
									</select>
									<input type="text" name="searchText" style=" padding: 5px; border-radius: 5px; width: 220px;" placeholder="검색어를 입력해주세요">
									<button id="searchNoticeBtn">검색하기</button>
								</div>

							</div>
							<div>
								<div id="noticeList">
								<table style="text-align:center; margin-top: 10px;">
									<tr>
										<td width="140px">번호</td>
										<td width="400px">글 제목</td>
										<td width="120px">작성일</td>
										<td width="140px">조회수</td>
									</tr>
								</table>
								<table id="noticeEle">
								
								</table>
								</div>
							</div>
					</div>
					
					<div id="workListContainer">
							<br><br>
							<div>
								<div class="addPostButton" style="float: right;">
									<button onclick="addWorkReport();">추가</button>
								</div>
								<div id="dateContainer" style="margin-top: 10px; margin-left: 325px;">
									 <a style="cursor: pointer;" onclick="prevDate();">&#60;</a><a id="dateyear" style="padding-left: 15px; padding-right: 5px;"></a>. 
									 <a id="dateMonth"></a>. 
									 <a id="dateDay"></a> 
									 <a style="cursor: pointer;" onclick="nextDate();">&#62;</a>
								</div>
							
							
								<div id="workLists">
									<table style="text-align:center; margin-top: 10px;">
										<tr>
											<td width="100px">번호</td>
											<td width="120px">구분</td>
											<td width="480px">업무내용</td>
											<td width="100px">작성자</td>
										</tr>
									</table>
								</div>
									<div id="workReportContainer" style="margin-bottom: 30px;">
									<!-- 업무공유보고서 시작 -->
										<div class="workListEle" style="margin-left: 35px; margin-top: 20px; margin-bottom: 20px; width: 800px; height: 180px; border: 1px solid black;">
											<div style="margin-top: 30px;">
												<table style="text-align:center; margin-top: 10px;">
													<tr>
														<td width="100px">1</td>
														<td width="120px"><div class="workType">요청</div></td>
														<td width="480px"><textarea rows="6" cols="65" readonly>오전 회의때도 말씀드렸지만 잊으실것같아 다시 말씀드립니다.
																				오페라 프로젝트 개발에 대한 자료가 다소 부족한것 같아 
																				타사 그룹웨어를 분석하고 해당 자료를 정리해주시면 됩니다.
														</textarea></td>
														<td width="100px">작성자</td>
													</tr>
												</table>
											</div>
										</div>
										<!-- 업무공유보고서 끝 -->
										
									</div>
							</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>

      
	<jsp:include page="../common/operaNav.jsp" />
	
	<script>

		var memberListFirst = "${memberListNo}"
		
		memberList = memberListFirst.split(',');
	
		loadJoindMember();

		function addWorkReport(){
			jQuery('.report_wrap').fadeIn('slow');
		}

		function addWorkNotice(){
			if(${loginUser.empNo} == ${workDetail.empNo}){
				document.getElementById("hiddenWorkData").submit();
			} else {
				alert("공지사항 작성 권한이 없습니다.");
			}
	
		}
		
		function downloadFile(code){
			location.href="fileDownload.file?fileNo="+code;
		}
		
	</script>
	<script>
		updateDonutChart('#specificChart', ${workDetail.workProg}, true);
		var yearText = document.getElementById('dateyear');
		var monthText = document.getElementById('dateMonth');
		var dayText = document.getElementById('dateDay');
		var today = new Date();
		var maxDate = 0;
	
		checkMonth(today.getMonth()+1, today.getFullYear());
		
		yearText.innerText = today.getFullYear();
		
		function monthInner(Month){
			if(Month < 10){
				monthText.innerText = "0" + Month;
				}
			else {
				monthText.innerText = Month;
				}
			}
	
		function dayInner(Day){
			if( Day < 10){
				dayText.innerText = "0" + Day;
				}
			else {
				dayText.innerText = Day;
				}
			}
		
		monthInner(today.getMonth()+1);
		dayInner(today.getDate());


		function loadReports(){
				
			$("#workReportContainer").children().remove();
			
			var workNo = "${workDetail.workNo}";
			var dateText = yearText.innerText.substring(2,4)+"/"+monthText.innerText+"/"+dayText.innerText;
			
			$.ajax({
				url:"loadShareReport.sha",
				type:"post",
				data:{workNo: workNo,
					date: dateText},
				success:function(data) {
					var reportLists = data.reportList;
					var index = 0;
					var boardNum = reportLists.length;
					reportLists.forEach(function(){
						switch(reportLists[index].reportType){
						case "결과":
							$("#workReportContainer").append("<div class='workListEle' style='margin-left: 35px; margin-top: 20px; margin-bottom: 20px; width: 800px; height: 180px; border: 1px solid black;'>"+
									"<div style='margin-top: 30px;'>"+
									"<table style='text-align:center; margin-top: 10px;'>"+
										"<tr>"+
											"<td width='100px'>"+boardNum+"</td>"+
											"<td width='120px'><div class='workTypeResults'>"+reportLists[index].reportType+"</div></td>"+
											"<td width='480px'><textarea rows='6' cols='65' readonly>"+reportLists[index].reportDetail+
											"</textarea><div id='filePart"+reportLists[index].reportNo+"' class='filePart'></div></td>"+
											"<td width='100px'>"+reportLists[index].empName+"<br>"+reportLists[index].jobName+"</td>"+
										"</tr></table></div></div>");
							break;
						case "요청":
							$("#workReportContainer").append("<div class='workListEle' style='margin-left: 35px; margin-top: 20px; margin-bottom: 20px; width: 800px; height: 180px; border: 1px solid black;'>"+
									"<div style='margin-top: 30px;'>"+
									"<table style='text-align:center; margin-top: 10px;'>"+
										"<tr>"+
											"<td width='100px'>"+boardNum+"</td>"+
											"<td width='120px'><div class='workTypeRequest'>"+reportLists[index].reportType+"</div></td>"+
											"<td width='480px'><textarea rows='6' cols='65' readonly>"+reportLists[index].reportDetail+
											"</textarea><div id='filePart"+reportLists[index].reportNo+"'  class='filePart'></div></td>"+
											"<td width='100px'>"+reportLists[index].empName+"<br>"+reportLists[index].jobName+"</td>"+
										"</tr></table></div></div>");
							break;
						case "피드백":
							$("#workReportContainer").append("<div class='workListEle' style='margin-left: 35px; margin-top: 20px; margin-bottom: 20px; width: 800px; height: 180px; border: 1px solid black;'>"+
									"<div style='margin-top: 30px;'>"+
									"<table style='text-align:center; margin-top: 10px;'>"+
										"<tr>"+
											"<td width='100px'>"+boardNum+"</td>"+
											"<td width='120px'><div class='workTypeFeedback'>"+reportLists[index].reportType+"</div></td>"+
											"<td width='480px'><textarea rows='6' cols='65' readonly>"+reportLists[index].reportDetail+
											"</textarea><div id='filePart"+reportLists[index].reportNo+"' class='filePart'></div></td>"+
											"<td width='100px'>"+reportLists[index].empName+"<br>"+reportLists[index].jobName+"</td>"+
										"</tr></table></div></div>");
							break;
						}
						if(reportLists[index].reportFile != null){
							$("#filePart"+reportLists[index].reportNo).append("<i class='fas fa-file'></i>&nbsp;&nbsp;<a onclick='downloadFile("+reportLists[index].reportFile+")'>"+reportLists[index].reportFileName+"</a>")
						}
						
						boardNum -= 1;
						index += 1;
					})
				},
				error:function(){
					console.log("에러!");
				}
			});
		}

		loadReports();

		
		function prevDate(){
			var nextYear = parseInt(yearText.innerText);
			var nextMonth = parseInt(monthText.innerText);
			var nextDate = parseInt(dayText.innerText);
			nextDate -= 1;
			if(nextDate <= 0){
	
					nextDate = maxDate;
					nextMonth -= 1;
	
					if(nextMonth <= 0) {
						nextMonth = 12;
						nextYear -= 1;
						checkMonth(nextMonth, nextYear);
						nextDate = maxDate;
					} else {
						checkMonth(nextMonth, nextYear);
						nextDate = maxDate;
					}
				}
			monthInner(nextMonth);
			dayInner(nextDate);
			yearText.innerText = nextYear;
			loadReports();
			}
	
		function nextDate(){
			var nextYear = parseInt(yearText.innerText);
			var nextMonth = parseInt(monthText.innerText);
			var nextDate = parseInt(dayText.innerText);
			nextDate += 1;
			if(nextDate > maxDate){
	
					nextDate = 1;
					nextMonth += 1;
	
					if(nextMonth > 12) {
						nextMonth = 1;
						nextYear += 1;
						checkMonth(nextMonth, nextYear);
						nextDate = 1;
					} else {
						checkMonth(nextMonth, nextYear);
						nextDate = 1;
					}
				}
			monthInner(nextMonth);
			dayInner(nextDate);
			yearText.innerText = nextYear;
			loadReports();
			}
		
		function checkMonth(Month, Year){
				console.log("달 확인: " + Month);
				switch(Month){
					case 1 :
					case 3 :
					case 5 :
					case 7 :
					case 8 : 
					case 10 :
					case 12 : maxDate = 31; break;
					case 4 :
					case 6 :
					case 9 :
					case 11 : maxDate = 30; break;
					case 2 :
						if(Year % 4 == 0) {
							maxDate = 29; break; }
						else { maxDate = 28; break; }
				}
				console.log("최고일수: " + maxDate);
			}

	</script>
	<script>
	
		function selectNoticeMenu(){

			var workNo = "${workDetail.workNo}";

			$("#noticeEle tr").remove();
			$("#workList").css({"border":"1px solid black", "font-weight" : "normal"});
			$("#workNotice").css({"border":"0px" ,"font-weight":"bold"});
			$("#noticeContainer").css("display","block");
			$("#workListContainer").css("display","none");

			$.ajax({
				url:"loadShareNotice.sha",
				type:"post",
				data:{workNo: workNo},
				success:function(data) {
					var noticeLists = data.noticeList;
					var index = 0;
					var boardNum = noticeLists.length;
					noticeLists.forEach(function(){
						$("#noticeEle").append("<tr class='row'><td width='140px'><div style='display:none;'>"+noticeLists[index].noticeNo+"</div>"+boardNum+"</td><td width='400px'>"+noticeLists[index].noticeTitle+
												"</td><td width='120px'>"+noticeLists[index].noticeDate+"</td><td width='140px'>"+noticeLists[index].noticeCnt+"</td></tr>");	
						index += 1;
						boardNum -= 1;
					})

					$("#noticeEle tr.row").click(function(){
						var noticeNum = $(this).children("td").eq(0).children("div").text();
						location.href="workShareDetail.sha?noticeNo=" + noticeNum;
					})
				},
				error:function(){
					console.log("에러!");
				}
			});

		}

		function selectWorkMenu(){
			
			$("#workList").css({"border": "0px","border-right":"1px solid black","font-weight":"bold"});
			$("#workNotice").css({"border-bottom":"1px solid black","border-right":"1px solid black" ,"font-weight" : "normal"});
			$("#noticeContainer").css("display","none");
			$("#workListContainer").css("display","block");



		}
		 selectNoticeMenu();

		 
		 
		function saveReport(){
			document.getElementById("reportForm").submit();
		}

		function updateWork(){
			document.getElementById("workDetailForm").submit();
		}

		
	</script>
</body>
</html>