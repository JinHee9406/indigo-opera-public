<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
<style>
		#mainDiv {
			float: left;
			width: 1100px;
			margin-left: 20px;
			margin-top: 90px;
		}
		
		#addTable{
			width: 950px; 
			height: 400px;
			background: #F6F6F6;
			border-top: 2px solid #828282;
			border-bottom: 2px solid #828282;
			 border-collapse: collapse;
		}
		
		#addTable th{
			background: #27334C;
			color: white;
			font-weight: bold;
			
		}
		
		#addPartic{
			margin-left: 15px;
			background: #27334C;
			color: white;
			font-wegiht: bold;
			border-radius: 10px;
			width: 60px;
			height: 25px;
			outline: none;
		}
		#addTable input{
			border: 1px solid #828282;
			border-radius: 5px;
			height: 25px;
		}
		
		#addWorks{
			float: right;
			margin-right: 70px;
			margin-top: 20px;
			background: #27334C;
			color: white;
			font-wegiht: bold;
			border-radius: 10px;
			width: 100px;
			height: 35px;
			outline: none;
		}
</style>
</head>
<body>
<jsp:include page="../common/shareAside.jsp" />
	<jsp:include page="addMember.jsp"/>
	<section> 
		<div id="mainDiv">
			<div id="titleDiv">
				<img id="titleIcon" src="/opera/resources/icons/shareworks-black.png">
				<a id="titleName">업무 공유하기</a>
			</div>
			<hr>
			<br>
			<div style="margin-left: 80px;">
			<form id="workShare" action="workShareStart.sha" method="post">
				<table id="addTable">
					<tr>
						<th >업무명</th>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;<input name="empNo" type="hidden" value="${loginUser.empNo}">
						<input name="empUserName" type="hidden" value="${loginUser.empName} ${loginUser.empJobName}">
						<input name="workName" type="text" style="width: 250px;" placeholder="업무명을 입력해주세요"></td>
					</tr>
					<tr>
						<th>업무기간</th>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;<input  name="workSDate" type="date"> ~ <input name="workEDate" type="date"></td>
					</tr>
					<tr>
						<th>업무상태</th>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;진행 중</td>
					</tr>
					<tr>
						<th>업무진행도</th>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;<input name="workProg" type="number" style="width: 70px; text-align:center;" value="0">%</td>
					</tr>
					<tr>
						<th>참여자</th>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;<input id="workUser" type="hidden" name="workUser"><input id="workUserName" name="workUserName" type="text" style="width: 250px;"  readonly><div style="float:right; margin-right: 530px; text-align:center;" id="addPartic"  onclick="openWindow()">선택</div></td>
					</tr>
					<tr>
						<th style="height: 150px; ">업무설명</th>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;<textarea name="workDetail" style="width: 90%; height: 100%;" placeholder="업무에 대한 설명을 간단히 입력해 주세요" style="resize: none;"></textarea></td>
					</tr>
				</table>
			</form>
				<button onclick="submitWorkAdd();" id="addWorks">작성완료</button>
			</div>
		</div>
	</section>
	<jsp:include page="../common/operaNav.jsp" />
	<script>
		function submitWorkAdd(){
			document.getElementById("workShare").submit();
		}
	</script>
</body>
</html>