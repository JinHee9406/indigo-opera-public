<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/calendarStyle.css">
<style>
	.mainEleParent{
		margin: 10px; 
		float: left;
	}


	#myInfoCon{
		margin: 10px; 
		width: 350px; 
		height: 420px;
		background: #FFFFFF;
		border: 2px solid #AAAAAA; 
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	}
	
	#attendBtn button{
		width: 90px;
		height: 30px;
		background: #27334C;
		color: white;
		font-weight: bold;
		border-radius: 5px;
		cursor: pointer;
		margin-top: 10px;
	}
	
	#submitBtn{
		float: right;
		width: 50px; 
		height: 22px; 
		border-radius: 5px; 
		background: #27334C; 
		font-weight: bold; 
		padding-top: 5px;
		color: white;
		font-size: 0.8em;
		cursor: pointer;
	}
	
	#userProfile img{
		width: 110px; 
		height: 110px; 
		border-radius: 70%;
		overflow: hidden;
	}
	
	#timeBoard{
		clear: both;
		font-size: 0.8em;
		padding-top: 10px;
		margin-bottom: 10px;
		margin-left: 45px;
	}
	.mainEleTitle{
		font-weight: bold;
		font-size: 0.9em;
		margin-top: 5px;
		margin-bottom: 5px;
		margin-left: 10px;
	}
	
	#mainAddress{
		margin: 10px; 
		width: 350px; 
		height: 120px; 
		background: white;
		border: 2px solid #AAAAAA; 
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	}
	
	#mainBoard{
		margin: 10px; 
		width: 350px; 
		height: 120px; 
		border: 2px solid #AAAAAA; 
		background: white;
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	}
	
	#mainSchedule{
		margin: 10px; 
		width: 500px; 
		height: 450px; 
		border: 2px solid #AAAAAA; 
		background: white;
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	}
	
	#mainNotice{
		margin: 10px; 
		width: 500px; 
		height: 220px; 
		border: 2px solid #AAAAAA; 
		background: white;
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	}
	
	#mainApproval{
		margin: 10px; 
		width: 350px; 
		height: 180px; 
		border: 2px solid #AAAAAA; 
		background: white;
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	}
	
	#mainReserv{
		margin: 10px; 
		width: 350px; 
		height: 160px; 
		border: 2px solid #AAAAAA; 
		background: white;
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	}
	
	#mainWorks{
		margin: 10px; 
		width: 350px; 
		height: 310px; 
		border: 2px solid #AAAAAA; 
		background: white;
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	}
	
	.mainEleCont{
		margin-left: 10px; 
		margin-right: 10px;
	}
	
	.mainEleCont table{
		width: 100%;
		border-top: 1px solid #C4C4C4;
		border-bottom: 2px solid #C4C4C4;
		border-collapse: collapse;
	}
	.mainEleCont table th{
		font-size: 0.8em; 
		font-weight: bold;
		border-top: 2px solid #C4C4C4;
		border-bottom: 2px solid #C4C4C4;
		background: #27334C;
		padding: 1px;
		color: white;
	}
	.mainEleCont table td{
		text-align: center;
		padding: 2px;
		font-size: 0.8em; 
		font-weight: 100;
		border-top: 1px solid #C4C4C4;
		border-bottom: 1px solid #C4C4C4;
	}
	#knowTable tr{
		cursor: pointer;
		transition: all ease 0.35s 0s;
	}
	#knowTable tr.head{
		background: #ECECEC;
	}
	
	#knowTable tr.row:hover {
		background: #27334C;
		color: white;
	}
	#noticeTable tr{
		cursor: pointer;
		transition: all ease 0.35s 0s;
	}
	#noticeTable tr.row:hover {
		background: #27334C;
		color: white;
	}
	
	#documentTable tr{
		cursor: pointer;
		transition: all ease 0.35s 0s;
	}
	#documentTable tr.row:hover {
		background: #27334C;
		color: white;
	}
	
	.shareEle{
		margin-top: 10px; 
		width: 100%; 
		height: 60px; 
		border: 1px solid black; 
		border-radius: 10px;
		background: white;
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
		transition: all ease 0.35s 0s;
	}
	
	.shareEle tr{
		cursor: pointer;
	}
	
	.shareEle tr.row:hover, .shareEle:hover {
		background: #27334C;
		border: 2px solid #E61358;
		font-weight: bold;
		color: white;
	}
		
	 .moreBtn{
		float: right; 
		padding-right: 10px; 
		font-size: 0.8em; 
		color: #A5A5A5;
		cursor: pointer;
		padding-top: 10px;
		font-weight: bold;
	}	
	
	#addressTable{
		width: 100%; 
		text-align: center; 
		font-size: 0.9em; 
		margin-top: 10px;
	}
	
	#addressTable td i{
		font-size: 2.5em; 
		margin-bottom: 10px;
		color:  #27334C;
	}
</style>
</head>
<body>
	<jsp:include page="../common/operaNav.jsp"/>	
	<div style="height: 700px; margin-left: 60px; margin-top: 35px;">
		<div style="width: 1350px; height: 700px;">
						<div class="mainEleParent">
				<div id="myInfoCon">
					<div style="margin: 10px; font-weight: bold;">내 정보</div>
					<div id="myInfo" style="text-align: center;">
						<div id="userProfile">
							<img src="/opera/resources/profiles/empty.jpg">
						</div>
						<br>
						<c:out value="${loginUser.empName} ${loginUser.empJobName}"/>님<br>
						환영합니다.		
						<h5 id="clock" style="margin-top: 10px; color:gray;">00:00</h5>
						
						<div id="attendBtn" style="float: left; margin-left: 45px;">
							<c:if test="${attendanceCheck == 1}">
								<button style="margin-right: 75px;" onclick="clickGotoWork();">출근</button>
							</c:if>
							<c:if test="${attendanceCheck != 1}">
								<button style="margin-right: 75px;">출근</button>
							</c:if>
							<button onclick="clickGotoHome();">퇴근</button>
						</div>
						<div id="timeBoard">
							<div id="workTime" style="float: left; text-align: center;">
								<c:if test="${not empty attendanceChecklist || attendanceChecklist != null}">
									<c:forEach var="attendanceChecklist" items="${attendanceChecklist }" varStatus="status">
										<c:if test="${status.last }">
											<label style="color: gray;">
												<fmt:formatDate value="${attendanceChecklist.workTime}" pattern="MM월 dd일 E요일"/><br>
												<fmt:formatDate value="${attendanceChecklist.workTime}" pattern="HH:mm:ss"/>
											</label>
										</c:if>
									</c:forEach>
								</c:if>
							</div>
							<div id="homeTime" style="float: left; margin-left: 85px; text-align: center;">
								<c:if test="${not empty attendanceChecklist || attendanceChecklist != null}">
									<c:forEach var="attendanceChecklist" items="${attendanceChecklist }" varStatus="status">
										<c:if test="${status.last }">
											<label style="color: gray;">
												<fmt:formatDate value="${attendanceChecklist.homeTime}" pattern="MM월 dd일 E요일"/><br>
												<fmt:formatDate value="${attendanceChecklist.homeTime}" pattern="HH:mm:ss"/>
											</label>
										</c:if>
									</c:forEach>
								</c:if>
							</div>
						</div>
						<form id="userProfileForm" action="insertProfileImg.main" method="post" enctype="multipart/form-data">
						<div style="position: absolute; margin-top: 55px; margin-left: 10px; float: left;">
							<input type="hidden" name="userEmpNo" value="${loginUser.empNo}">
							<input style="float: left;" type="file" name="photo">
							<div id="submitBtn" onclick="uploadProfileImg()">저장</div>
						</div>
						</form>
					</div>
				</div>
				<div id="mainAddress">
					<div class="mainEleTitle">자주찾는 주소록</div>
					<table id="addressTable" style="">
						<tr>
							<td><i class="far fa-address-book"></i><br>김이박</td>
							<td><i class="far fa-address-book"></i><br>박최정</td>
							<td><i class="far fa-address-book"></i><br>인효근</td>
						</tr>
					</table>
				</div>
				<div id="mainBoard">
					<div class="mainEleTitle">최근 지식공유글 <a class="moreBtn" onclick="location.href='knowledgeMain.know'">> more</a></div>
					<div class="mainEleCont">
						<table id="knowTable">
							<tr>
								<th style="width: 40px;">번호</th>
								<th style="width: 90px;">관련부서</th>
								<th>글제목</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="mainEleParent">
				<div id="mainSchedule">
					<div class="mainEleTitle">일정</div>
					
						<div id="calendar">
							 <div class="calmain">
							    <div class="content-wrap" style="margin-left: 0px; border: none; height: 360px; margin-top: 0px;">
							      <div class="content-right" style="margin-left: -60px;">
							        <table id="calendar" align="center">
							          <thead>
							            <tr class="btn-wrap clearfix">
							              <td style="text-align: center;">
							              	<div onclick="prev();">
								                <label id="prev" >
								                   &#60;
								                </label>
							                </div>
							              </td>
							              <td align="center" id="current-year-month" colspan="5" ></td>
							              <td style="text-align: center;">
							              	<div onclick="next();">
							                <label id="next">
							                    &#62;
							                </label>
							                </div>
							              </td>
							            </tr>
							            <tr>
							                <td class = "sun" align="center">Sun</td>
							                <td align="center">Mon</td>
							                <td align="center">Tue</td>
							                <td align="center">Wed</td>
							                <td align="center">Thu</td>
							                <td align="center">Fri</td>
							                <td class= "sat" align="center">Sat</td>
							              </tr>
							          </thead>
							          <tbody id="calendar-body" class="calendar-body"></tbody>
							        </table>
							      </div>
							    </div>
							  </div>
						</div>
					
				</div>
				<div id="mainNotice">
					<div class="mainEleTitle">공지사항<a class="moreBtn" onclick="location.href='allNotice.not'">> more</a></div>
					<div class="mainEleCont">
						<table id="noticeTable">
							<tr>
								<th style="width: 100px;">번호</th>
								<th>제목</th>
								<th style="width: 130px;">작성일</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="mainEleParent">
				<div id="mainApproval">
					<div style="margin: 10px; font-weight: bold;">내 서류결재 <a class="moreBtn" onclick="location.href='approvalInProgress.app'">> more</a></div>
					<div class="mainEleCont">
						<table id="documentTable">
							<tr>
								<th style="width: 90px;">승인여부</th>
								<th>문서코드</th>
								<th style="width: 80px;">문서진행상태</th>
							</tr>
						</table>
					</div>
				</div>
				<div id="mainReserv">
					<div style="margin: 10px; font-weight: bold;">금일 예약목록 <a class="moreBtn" onclick="location.href='reservationMain.res'">> more</a></div>
					<div class="mainEleCont">
						<table id="resTable">
							<tr>
								<th style="width: 110px;">장소</th>
								<th>예약시간</th>
							</tr>
						</table>
					</div>
				</div>
				<div id="mainWorks">
					<div class="mainEleTitle">작업중인 업무</div>
					<div class="mainEleCont">
						<table id="WorkTable">
							<tr>
								<th style="width: 50px;">번호</th>
								<th style="width: 90px;">개설자</th>
								<th>업무명</th>
							</tr>
						</table>
						<div id="workEles">
						
						</div>
						<a class="moreBtn" onclick="location.href='shareworksMain.sha?loginUser=${loginUser.empNo}'">> more</a>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	<form action="insertAttend.att" method="post" id="iaForm">
		<input type="hidden" value="${loginUser.empNo }" name="empNo">
	</form>
	
	<form action="updateAttend.att" method="post" id="uaForm">
		<input type="hidden" value="${loginUser.empNo }" name="empNo">
	</form>
	
	<script>
		function approvalLoad() {

			var loginUser = ${loginUser.empNo};
								
			$.ajax({
				url:"loadApp.main",
				type:"post",
				data:{
					loginUser : loginUser
					},
				success:function(data) {
					var appLists = data.Approval;
					
					var num = 3
					var indexNum = 3;
					if(appLists.length < 3) {
						num = appLists.length;
						indexNum = num;
					}
					
					for(var i = 0; i < indexNum; i++){
						$("#documentTable").append("<tr class='row'><td style='padding: 10px;'><div style='display:none;'>"+appLists[i].draftNo+"</div><a style=' font-weight: bold; color: #E61358'>"+appLists[i].loStatus+"</a></td><td>"+appLists[i].draftNo+"</td><td>"+appLists[i].draftStatus+"</td></tr>");
						num -= 1;
					}
					
					$("#documentTable tr.row").click(function(){
						var docuNum = $(this).children("td").eq(0).children("div").text();
						location.href="selectApprovalOne.app?oneDocu=" + docuNum;
					})
				},
				error:function(){
					console.log("에러!");
				}
			});
			
			return false;
		}
	
		approvalLoad();

	</script>
	
	<script>
		function noticeLoad() {					
			$.ajax({
				url:"loadNotice.main",
				type:"post",
				data:{},
				success:function(data) {
					var noticeLists = data.Notices;
					var num = 4
					var indexNum = 4;
					if(noticeLists.length < 4) {
						num = noticeLists.length;
						indexNum = num;
					}

					
					for(var i = 0; i < indexNum; i++){
						$("#noticeTable").append("<tr class='row'><td style='padding: 10px;'><div style='display:none;'>"+noticeLists[i].boardNo+"</div>"+num+"</td><td>"+noticeLists[i].boardTitle+"</td><td>"+noticeLists[i].boardDate+"</td></tr>");
						num -= 1;
					}
					
					$("#noticeTable tr.row").click(function(){
						noticeNum = $(this).children("td").eq(0).children("div").text();
						//location.href="knowledgeDetail.know?knowledgeNum=" + knowledgeNum;
					})
				},
				error:function(){
					console.log("에러!");
				}
			});
			
			return false;
		}
	
		noticeLoad();

	</script>
	<script>
		function knowledgeLoad() {					
			$.ajax({
				url:"loadKnowledges.main",
				type:"post",
				data:{},
				success:function(data) {
					var knowlists = data.Knows;
					var num = 3;
					for(var i = 0; i < 3; i++){
						$("#knowTable").append("<tr class='row'><td><div style='display:none;'>"+knowlists[i].knowNo+"</div>"+num+"</td><td>"+knowlists[i].knowType+"</td><td>"+knowlists[i].knowTitle+"</td></tr>");
						num -= 1;
					}
					
					$("#knowTable tr.row").click(function(){
						console.log("클릭");
						knowledgeNum = $(this).children("td").eq(0).children("div").text();
						location.href="knowledgeDetail.know?knowledgeNum=" + knowledgeNum;
					})
				},
				error:function(){
					console.log("에러!");
				}
			});
			
			return false;
		}
	
		knowledgeLoad();

	</script>
	
	<script>
		var todayText;
	
		function ReservationLoad() {		
			var user =  ${loginUser.empNo};

			console.log(todayText);
						
			$.ajax({
				url:"loadRes.main",
				type:"post",
				data:{
					loginUser:user,
					today:todayText
					},
				success:function(data) {
 					var reslists = data.Reservation;
					var num = 3;
					var indexNum = 3;
					if(reslists.length < 3) {
						num = reslists.length;
						indexNum = num;
					}
					
					for(var i = 0; i < indexNum; i++){
						$("#resTable").append("<tr><td style='padding: 10px;'>"+reslists[i].roomName+"</td><td>"+reslists[i].sDate+" ~ "+reslists[i].eDate+"</td></tr>");
						num -= 1;
					}
				},
				error:function(){
					console.log("에러!");
				}
			});
			return false;
		}

	</script>
	
	
	<script>
		function moveWorkDetail(workNo){
			location.href='shareworksDetail.sha?workNum=' + workNo;
		}
	
		function shareWorkLoad(loginUser) {					
			$.ajax({
				url:"loadWorks.main",
				type:"post",
				data:{loginUser:loginUser},
				success:function(data) {
					var workLists = data.Works;
					var num = 3;
					var indexNum = 3;
					if(workLists.length < 3) {
						num = workLists.length;
						indexNum = num;
					}
					for(var i = 0; i < indexNum; i++){
						$("#workEles").append("<div onclick='moveWorkDetail("+workLists[i].workNo+")'"+
						"class='shareEle'>"+
						"<div><table style='text-align:center; margin-top: 10px; border:none; '>"+
						"<tr style='border: none;'><td width='50px'  style='border: none;'>"+num+"</td>"+
						"<td width='90px'  style='border: none;'>"+workLists[i].empUserName+"<br>"+workLists[i].empUserJob+"</td>"+
						"<td  style='border: none;'>"+workLists[i].workName+"</td></tr></table></div></div>");
						num -= 1;
					}
				},
				error:function(){
					console.log("에러!");
				}
			});
			
			return false;
		}
	
		shareWorkLoad(${loginUser.empNo});

	</script>
	
	<script>
		var currentTitle = document.getElementById('current-year-month');
		var calendarBody = document.getElementById('calendar-body');
		var today = new Date();
		var first = new Date(today.getFullYear(), today.getMonth(),1);
		var dayList = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
		var monthList = ['January','February','March','April','May','June','July','August','September','October','November','December'];
		var leapYear=[31,29,31,30,31,30,31,31,30,31,30,31]; //윤년
		var notLeapYear=[31,28,31,30,31,30,31,31,30,31,30,31]; //윤년 아님
		var pageFirst = first; 
		var firstMonth = today.getMonth();
		var pageMonth = today.getMonth();
		var todayMonth = firstMonth+1;
		var todayYear = today.getFullYear();
		var todayDate = today.getDate();
		var calYear = todayYear;		
		var pageYear;

		if(todayMonth<10){
			todayMonth = "0"+todayMonth;
		}
		
		todayText = todayYear + "/" + todayMonth + "/" + todayDate;
		
		if(first.getFullYear() % 4 === 0){
		    pageYear = leapYear;
		}else{
		    pageYear = notLeapYear;
		}

		//달력을 출력하는 함수
		function showCalendar(){
		    let monthCnt = 100;
		    let cnt = 1;
		    for(var i = 0; i < 6; i++){
		        var $tr = document.createElement('tr');
		        $tr.setAttribute('id', monthCnt);   
		        for(var j = 0; j < 7; j++){
		            if((i === 0 && j < first.getDay()) || cnt > pageYear[first.getMonth()]){
		                var $td = document.createElement('td');
		                $tr.appendChild($td);     
		            }else{
		                var $td = document.createElement('td');
		                var $tdEle = document.createElement('div');
		                $tdEle.textContent = cnt;

						var eleId = calYear +""+ (pageMonth+1) +""+ cnt  
		                
		                $tdEle.setAttribute('id', eleId); 
		                //표 칸마다 id를 붙이며 id값는 숫자
		                
		                $tdEle.setAttribute('style','text-align: center;')
		                //90x60 스타일을 적용하는 setAttribute
		                
		                $td.appendChild($tdEle);
						//td부분에 요소 추가
		                               
		                $tr.appendChild($td);
		                cnt++;
		            }
		        }
		        monthCnt++;
		        currentTitle.innerHTML = monthList[first.getMonth()] + '&nbsp;&nbsp;&nbsp;&nbsp;'+ first.getFullYear();
		        today = new Date(today.getFullYear(), today.getMonth() + 1, today.getDate());
		        calendarBody.appendChild($tr);
		    }
		}

		showCalendar();

		//달력 달수를 변경할때 달력을 전부 지우고 다시불러오기 위해 지우는 함수
		function removeCalendar(){
		    let catchTr = 100;
		    for(var i = 100; i< 106; i++){
		        var $tr = document.getElementById(catchTr);
		        $tr.remove();
		        catchTr++;
		    }
		}

		//전달로 이동하는 함수
		function prev(){
		    const $divs = document.querySelectorAll('#input-list > div');
		    $divs.forEach(function(e){
		      e.remove();
		    });
		    const $btns = document.querySelectorAll('#input-list > button');
		    $btns.forEach(function(e1){
		      e1.remove();
		    });
		    if(pageFirst.getMonth() === 1){
		        pageFirst = new Date(first.getFullYear()-1, 12, 1);
		        first = pageFirst;
		        if(first.getFullYear() % 4 === 0){
		            pageYear = leapYear;
		        }else{
		            pageYear = notLeapYear;
		        }
		    }else{
		        pageFirst = new Date(first.getFullYear(), first.getMonth()-1, 1);
		        first = pageFirst;
		    }
		    pageMonth = pageFirst.getMonth();
		    calYear = pageFirst.getFullYear();
		    currentTitle.innerHTML = monthList[first.getMonth()] + '&nbsp;&nbsp;&nbsp;&nbsp;'+ first.getFullYear();
		    removeCalendar();
		    showCalendar();
	        showToday();
		}

		//다음달로 이동하는 함수
		function next(){
		    const $divs = document.querySelectorAll('#input-list > div');
		    $divs.forEach(function(e){
		      e.remove();
		    });
		    const $btns = document.querySelectorAll('#input-list > button');
		    $btns.forEach(function(e1){
		      e1.remove();
		    });
		    if(pageFirst.getMonth() === 12){
		        pageFirst = new Date(first.getFullYear()+1, 1, 1);
		        first = pageFirst;
		        if(first.getFullYear() % 4 === 0){
		            pageYear = leapYear;
		        }else{
		            pageYear = notLeapYear;
		        }
		    }else{
		        pageFirst = new Date(first.getFullYear(), first.getMonth()+1, 1);
		        first = pageFirst;
		    }
		    pageMonth = pageFirst.getMonth();
		    calYear = pageFirst.getFullYear();
		    currentTitle.innerHTML = monthList[first.getMonth()] + '&nbsp;&nbsp;&nbsp;&nbsp;'+ first.getFullYear();
		    removeCalendar();
		    showCalendar(); 
	        showToday();
		}

		//오늘인 부분은 오페라 색상으로 변경(연도 구분없음 )
		function showToday(){
		
			if(firstMonth == pageMonth && todayYear == calYear){

			    clickedDate1 = document.getElementById(calYear+""+(pageMonth+1)+""+todayDate);
			    clickedDate1.classList.add('active');
			}
		}
        showToday();
	</script>
	
	
	<script>
			function uploadProfileImg(){
				console.log("업로드");
				document.getElementById('userProfileForm').submit();
			}


			function userProfileLoad() {
				var userEmpNo = ${loginUser.empNo}
							
				$.ajax({
					url:"loadUserFile.file",
					type:"post",
					data:{userEmpNo: userEmpNo},
					success:function(data) {
						var imgName = data.fileList.changeName;
						$("#userProfile").children().remove()
						$("#userProfile").append("<img src='${contextPath}/resources/uploadFiles/"+imgName+"'>");
					},
					error:function(){
						console.log("에러!");
					}
				});
				
				return false;
			}
			userProfileLoad();

			
			var clockTarget = document.getElementById("clock");		
			function clock() {
			    var date = new Date();	
			    // date Object를 받아오고 
			    var month = date.getMonth();
			    // 달을 받아옵니다 
			    var clockDate = date.getDate();
			
			    // 몇일인지 받아옵니다 
			    var day = date.getDay();
			
			    // 요일을 받아옵니다. 
			    var week = ['일', '월', '화', '수', '목', '금', '토'];
			
			    // 요일은 숫자형태로 리턴되기때문에 미리 배열을 만듭니다. 
			    var hours = date.getHours();
				
			    // 시간을 받아오고 
			    var minutes = date.getMinutes();
			
			    // 분도 받아옵니다.
			    var seconds = date.getSeconds();
	
				var hourString = "";
				var minuteString = "";
				var secondString = "";
				
				if(hours<10) {
						hourString = '0' + hours;
					} else {
						hourString = hours;
						}

				if(minutes<10){
						minuteString = '0' + minutes;
					} else {
						minuteString = minutes;
						}

				if(seconds < 10){
						secondString = '0' + seconds;
					} else {
						secondString = seconds;
						}
			    // 초까지 받아온후 
 			    clockTarget .innerText = (month+1) + '월' + clockDate+ '일 ' + week[day] + '요일' + "\n"+
				hourString + " : " + minuteString + " : " + secondString; 

    			// 월은 0부터 1월이기때문에 +1일을 해주고 
			    // 월은 0부터 1월이기때문에 +1일을 해주고 
			
			    // 시간 분 초는 한자리수이면 시계가 어색해보일까봐 10보다 작으면 앞에0을 붙혀주는 작업을 3항연산으로 했습니다. 
			}
			
			function init() {
			
				clock();
			
				setInterval(clock, 1000);
			}
			init();

			ReservationLoad();
	</script>
	
	<script>
		var clickedWork = false;
		var clickedHome = true;
		var empNo = "${loginUser.empNo}";
		function clickGotoWork(){
			
			console.log(empNo);
			console.log(123123123);
			//$("#iaForm").submit();
			$.ajax({
				type:"POST",
				url:"insertAttend.att",
				data:{
					"empNo":empNo
				},
				success:function(data){
					console.log("다녀왔습니다.");
					if(clickedWork == false){
						var checkTime = $("#clock").text();

						var dateString = checkTime.substr(0,9);
						var timeString = checkTime.substr(9,16);

						$("#workTime").append(dateString + "<br>" + timeString);
						clickedHome = false;
						clickedWork = true;
					
					} 
				}
			});
			 
		}

		function clickGotoHome(){
			
			$("#uaForm").submit();

			if(clickedHome == false){
				var checkTime = $("#clock").text();

				var dateString = checkTime.substr(0,9);
				var timeString = checkTime.substr(9,16);

				$("#homeTime").append(dateString + "<br>" + timeString);
				clickedHome = true;
			}
	
		}
	</script>
</body>


</html>