<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<style>
   #knowledgeAside{
      float:left; 
      padding-left:60px; 
      padding-top: 50px; 
      width: 220px; 
      height: 800px; 
      background: #E5E5E5;
   }
   
    /* #AsideFirstBtn{
      
      padding-top: 10px; 
      width:100%; 
      height: 30px;
      background: #627192; 
      color: white; 
      text-align: center;
      cursor: pointer;
   }  */
   
    .AsideBtn{
      padding-top: 10px; 
      width:94%; 
      height: 40px;
      background: #FFFFFF; 
      color: 57585E; 
      text-align: left;
      cursor: pointer;
      border: 1px solid lightgray;
      padding-top: 20px;      
      font-size: 1.25em;
      padding-left: 10px;
   }
   
   .ddtag1{
   	font-size: 18px;
	font-weight: bold;
	margin-left: 20px;
	margin-top: 13px;
   } 
   
   .ddtag1:hover{
   	cursor: pointer;
   }
   #AsideFirstBtnChild{
   	bold:0.5px solid red;
   }
</style>
</head>
<body>
      <div id="knowledgeAside">
      <div class="AsideBtnBtn">
         <div id="AsideFirstBtn" class ="AsideBtn" style="background: #627192; color: white; " onclick="goAttendMain();">근태관리
          
         
         </div>
         <div id="AsideFirstBtnChild" class ="AsideBtn2" style="background: #E5E5E5; color: #57585E; height: 200px;" onclick="location.href='#'">
         <div style="padding-bottom: 20px;">
            <div style="color: #565656; font-weight: 600;">
              <!--  <button id="spread">
               </button> -->
            </div>
            <div id="ddta1">
               <div class="ddtag1" id="first-tag" onclick="goAttendMain();">출근기록</div>
               <div class="ddtag1" id="tag2" onclick="goAttendSta();">소속출근통계</div>
               <div class="ddtag1" onclick="location.href='attendWorkingHours.att'">소속근로시간통계</div>
            </div>
         </div>
         </div>
         </div>
         
         <div class="AsideBtnBtn">
         <div id="AsideFirstBtn2" class ="AsideBtn" style="background: #FFFFFF; color: #57585E; " onclick="location.href='vacationMain.att'">휴가관리
          
         
         </div>
         <div id="AsideFirstBtnChild2" class ="AsideBtn2" style="background: #E5E5E5; color: #57585E; height: 199.9px;" onclick="location.href='#'">
         <div style="padding-bottom: 20px;">
            <div style="color: #565656; font-weight: 600;">
              <!--  <button id="spread">
               </button> -->
            </div>
            <div id="ddta2">
               <div class="ddtag1" id="first-tag" onclick="location.href='vacationMain.att'">휴가사용현황</div>
               <div class="ddtag1" onclick="location.href='vacationUsage.att'">소속휴가통계</div>
            </div>
         </div>
         </div>
         </div>
         
         
         
         	<!-- <div style="margin-left: 25px;margin-top: 25px; width: 150px; height: 80%; background: white;">
         </div> -->
      </div>
      
      <script type="text/javascript">
      
      var aside = '${aside}';
      
      
      $(document).ready(function () {
    	  /* $("#ddta2").slideUp(1);
    	  $("#AsideFirstBtnChild2").hide(); */
    	  $("#first-tag").css({"color":"#E61358"});
    	  
		});
      
      
      	$(".AsideBtnBtn").click(function () {
      		$(this).parent().children().children(".AsideBtn").css({"background":"#FFFFFF","color":"#57585E"});
			$(this).children(".AsideBtn").css({"background":"#627192","color":"white"});
		});
      	
      	$(document).ready(function() {
            
               if (aside == "1") {
                  $("#ddta2").slideUp(1);
                  $("#ddta1").slideDown(100);
                  $("#AsideFirstBtnChild2").css({"height":"199.9px"});
                  $("#AsideFirstBtnChild").css({"height":"200px"});
                  $("#AsideFirstBtnChild").parent().children().children().children().children(".ddtag1").css({"color":"#57585E"});
                  $("#AsideFirstBtnChild").parent().children().children().children().children("#first-tag").css({"color":"#E61358"});
                  $("#AsideFirstBtnChild").show();
                  $("#AsideFirstBtnChild2").hide();
               } 
               
               if (aside == "2") {
                   $("#ddta2").slideUp(1);
                   $("#ddta1").slideDown(100);
                   $("#AsideFirstBtnChild2").css({"height":"199.9px"});
                   $("#AsideFirstBtnChild").css({"height":"200px"});
                   $("#AsideFirstBtnChild").parent().children().children().children().children(".ddtag1").css({"color":"#57585E"});
                   $("#AsideFirstBtnChild").parent().children().children().children().children("#tag2").css({"color":"#E61358"});
                   $("#AsideFirstBtnChild").show();
                   $("#AsideFirstBtnChild2").hide();
                } /* else {
                   $("#ddta1").slideUp(1);
                   $("#AsideFirstBtnChild").css({"height":"199.9px"});
                   $("#AsideFirstBtnChild").hide();

                } */
            
           
               /* if (aside == "2") {
                  $("#ddta1").slideUp(1);
                  $("#ddta2").slideDown(100);
                  $("#AsideFirstBtnChild").css({"height":"199.9px"});
                  $("#AsideFirstBtnChild2").css({"height":"200px"});
                  $("#AsideFirstBtnChild2").parent().children().children().children().children(".ddtag1").css({"color":"#57585E"});
                  $("#AsideFirstBtnChild2").parent().children().children().children().children("#first-tag").css({"color":"#E61358"});
                  $("#AsideFirstBtnChild2").show();
                  $("#AsideFirstBtnChild").hide();
               } else {
                  $("#ddta2").slideUp(1);
                  $("#AsideFirstBtnChild2").css({"height":"199.9px"});
                  $("#AsideFirstBtnChild2").hide();

               } */
            
            
            $(".ddtag1").click(function () {
				$(this).parent().parent().children().children(".ddtag1").css({"color":"#57585E"});
				$(this).css({"color":"#E61358"});
            	
            	
			});
            
            
            

            
         });
      	
      </script>
      
      <script type="text/javascript">
   var today = new Date();
   var dd = today.getDate();
   var mm = today.getMonth()+1; // Jan is 0
   var yyyy = today.getFullYear();
   
   var empNo = '${loginUser.empNo}';

   if(dd<10){
       dd = '0'+dd
   }
   if(mm<10){
       mm = '0'+mm
   }

   today = ""+yyyy +  mm +  dd ;
  /* $("#today").text(today); */
   
   	function goAttendMain() {
		location.href="attendMain.att?date="+today+"&empNo="+empNo;
	}
   </script>
   <script type="text/javascript">
   var today2 = new Date();
   var dd2 = today2.getDate();
   var mm2 = today2.getMonth()+1; // Jan is 0
   var yyyy2 = today2.getFullYear();
   
   var deptCode = '${loginUser.empDept}';

   if(dd2<10){
       dd2 = '0'+dd2
   }
   if(mm2<10){
       mm2 = '0'+mm2
   }

   today2 = ""+yyyy2 +  mm2 ;
  /* $("#today").text(today); */
   
   	function goAttendSta() {
		location.href="attendStatistics.att?date="+today2+"&deptCode="+deptCode;
	}
   </script>
</body>
</html>