<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<link rel="stylesheet" href="/opera/resources/css/asideStyle.css">
<link rel="stylesheet" href="/opera/resources/css/treemenuStyle.css">
<style>
	.detailMenu {
		margin: 25px;
		color: #545454;
		font-weight: bold;
		font-size: 0.9em;
		cursor: pointer;
	}
	
	.detailMenu:hover{
		color: #E61358;
	}
</style>
</head>
<body>
		<div id="asideBack">
			<div id="AsideFirstBtn" onclick="location.href='shareworksAdd.sha'">업무 공유하기</div>
			<br>
			<hr style="margin-left: 40px; width: 120px;">
					
			<div class="detailMenu" onclick="selectMenu1();"> 전체 업무목록 </div>
			<div class="detailMenu" onclick="selectMenu2();"> 개설 업무목록 </div>
			<div class="detailMenu" onclick="selectMenu3();"> 공유받은 업무목록</div>
		</div>
		
		<script>
			$(".detailMenu").click(function(){
				$(".detailMenu").removeAttr("id");
				$(this).attr("id","selected");
			})
			
			function selectMenu1(){
				location.href="shareworksMain.sha?loginUser=${loginUser.empNo}";
			}

			function selectMenu2(){
				location.href="shareworksMine.sha?loginUser=${loginUser.empNo}";
			}

			function selectMenu3(){
				location.href="shareworksShared.sha?loginUser=${loginUser.empNo}";
			}
		</script>

</body>
</html>