<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="/opera/resources/css/menuIconStyle.css">
	<title>Opera GroupWare</title>
	<style>
        html {
            width: 1420px;
        }
        * {
            margin: 0;
            padding: 0;
            font-family: 'Gothic A1', sans-serif;
            text-decoration: none;
        }
	
		nav{
			z-index: 10;
		}
        .mainNav{
            position: fixed; 
            width: 1600px;
            height: 50px;
            background: #27334C; 
            color: white;
			z-index: 2;
        }

        .mainAside{
            position: fixed; 
            margin-top: 60px;
            width: 60px; 
            background: #27334C; 
            color: white;
            z-index: 2;
        }

        .class_category_list li .asideIcon{
            text-align: center; 
            font-size: 0.6em;
            margin: 10px;
        }

		.asideIcon{
			cursor: pointer;
		}

        .asideIcon img{
            width: 25px; 
            height: 25px;
        }
        .titleImg{
            margin-left: 40px;
            width: 155px;
            margin-top: 8px;
        }

        #navPart{
            margin-top: 8px; 
            margin-right: 220px;
        }

        #userName{
            float: right; 
            margin-top: 8px;
            margin-left: 20px;
            cursor: pointer;
        }

        #userImg{
            float: right;
            margin-left: 40px;
        }

        #userImg img{
            width: 40px; 
            height: 40px; 
            border-radius: 70%; 
            overflow: hidden;
        }        

        .alertNum{
            position: absolute; 
            margin-left: 40px;
            margin-top: -25px;
            text-align: center; 
            font-size:0.8em; 
            border-radius: 70%; 
            font-weight: bold;
            overflow: hidden;
            width: 15px; 
            height: 15px; 
            background: white; 
            color: #27334C;
        }
        #messengerNav{
        	position: fixed; 
        	border-radius: 70%; 
        	border: 1px solid white;
        	padding: 10px 5px 15px 15px; 
        	bottom: 8%; 
        	right: 5%; 
        	width: 45px; 
        	height: 40px; 
        	background: #27334C; 
        	color: white;
        }
    </style>
</head>
<body>
 	<c:set var="contextPath" value="${ pageContext.servletContext.contextPath }" scope="application"/>
    <nav>
    	<div id="messengerNav"><div class="asideIcon" style="font-size: 0.6em; margin-left: 4px;" onclick="openChat();"><img src="/opera/resources/icons/Message.png"><br>메신저</div></div>
	    <div class="mainNav">
	         <div style="background-color: #27334C; width: 50px; padding-left: 10px;height: 60px; margin-top: 10px; float: left;">
	                <a class="menu-trigger" href="#">
	                    <span></span>
	                    <span></span>
	                    <span></span>
	                </a>
	            </div>
	            <div id="navTitle" style="float: left;"><img class="titleImg" src="/opera/resources/icons/TitleIcon.png"></div>
	            <div id="navPart">
	                <div style="cursor: pointer; margin-left: 20px; margin-right: 10px;  color: white; float:right;" onclick="location.href='logout.me'"><img width="35" height="35" src="/opera/resources/icons/exit.png"></div>
	                <div id="userName" onclick="location.href='myPageMain.my'"><c:out value="${loginUser.empName} ${loginUser.empJobName}님"/></div>
	                <div id="userImg" onclick="location.href='myPageMain.my'"></div>
	                <div>
	                    <div style=" float: right; margin-left: 5px;"><img style="width: 30px; height: 35px; margin-left: 20px;" src="/opera/resources/icons/BellIcon.png">
	                        <div class="alertNum">1</div>
	                    </div>  
	                </div>
	                <div style="float: right; margin-top: 10px;"><input type="text" placeholder="직원검색"></div>
	            </div>
	        </div>
	    <div class="mainAside"><div class="mainAside_container">
	        <ul class="class_category_list">
	            <li><div class="asideIcon" onclick="location.href='goMain.main'"><img src="/opera/resources/icons/homeIcon.png"><br>HOME</div></li>
	            <li><div class="asideIcon" onclick="location.href='approvalInProgress.app'"><img src="/opera/resources/icons/docuIcon.png"><br>전자결재</div></li>
	            <li><div class="asideIcon" onclick="location.href='scheduleMain.sch'"><img src="/opera/resources/icons/CalIcon.png"><br>일정관리</div></li>
	           	<c:if test="${loginUser.empDept == '003'}">
					<li><div class="asideIcon" onclick="location.href='insertForm.me'"><img src="/opera/resources/icons/team.png"><br>인사관리</div></li>
				</c:if>
	            <li><div class="asideIcon" style="font-size: 0.6em;"><img src="/opera/resources/icons/PhoneListIcon.png"><br>주소록</div></li>
	            <li><div class="asideIcon" onclick="location.href='reservationMain.res'"><img src="/opera/resources/icons/meetingIcon.png"><br>예약관리</div></li>
	            <li><div class="asideIcon" onclick="goAttendMain();"><img src="/opera/resources/icons/clockIcon.png"><br>근태관리</div></li>
	            <li><div class="asideIcon" onclick="location.href='allNotice.not'"><img src="/opera/resources/icons/NoticeIcon.png"><br>공지사항</div></li>
	            <li><div class="asideIcon" onclick="location.href='knowledgeMain.know'"><img src="/opera/resources/icons/knowledge.png"><br>지식공유</div></li>
	            <li><div class="asideIcon" onclick="location.href='shareworksMain.sha?loginUser=${loginUser.empNo}'"><img src="/opera/resources/icons/shareworks.png"><br>업무공유</div></li>
	            <c:if test="${loginUser.empDept == '003'}">
	            <li><div class="asideIcon" style="margin-top: 75px;"><img src="/opera/resources/icons/SettingIcon.png"><br>환경설정</div></li>
	            </c:if>
	            <c:if test="${loginUser.empDept != '003'}">
	            <li><div class="asideIcon" style="margin-top: 115px;"><img src="/opera/resources/icons/SettingIcon.png"><br>환경설정</div></li>
	            </c:if>
	            <li><div style="height: 150px;"></div></li>
	        </ul>
	        </div>
	    </div>
    </nav>>
    <script>
        var burger = $('.menu-trigger');
        burger.each(function(index){

            var $this = $(this);

            $this.on('click', function(e){
                console.log($('#knowledgeAside'));
                e.preventDefault();
                $(this).toggleClass('active-' + (index+1));
            })
        });
        
      
        function openChat(){
        	window.open("messenger.me?loginUser=${loginUser.empNo}", "childForm", "width=300, height=550, resizable = no, scrollbars = no");
        }


		function userProfileLoad() {
			var userEmpNo = ${loginUser.empNo}
						
			$.ajax({
				url:"loadUserFile.file",
				type:"post",
				data:{userEmpNo: userEmpNo},
				success:function(data) {
					var imgName = data.fileList.changeName;
					$("#userImg").children().remove()
					$("#userImg").append("<img src='${contextPath}/resources/uploadFiles/"+imgName+"'>");
				},
				error:function(){
					console.log("에러!");
				}
			});
			
			return false;
		}
		userProfileLoad();
    </script>
    <script type="text/javascript">
   var today2 = new Date();
   var dd = today2.getDate();
   var mm = today2.getMonth()+1; // Jan is 0
   var yyyy = today2.getFullYear();
   
   var empNo = '${loginUser.empNo}';

   if(dd<10){
       dd = '0'+dd
   }
   if(mm<10){
       mm = '0'+mm
   }

   today2 = ""+yyyy +  mm +  dd ;
  /* $("#today").text(today); */
   
   	function goAttendMain() {
		location.href="attendMain.att?date="+today2+"&empNo="+empNo;
	}
   </script>
</body>
</html>