<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<style>
   #knowledgeAside{
      float:left; 
      padding-left:60px; 
      padding-top: 50px; 
      width: 220px; 
      height: 800px; 
      background: #E5E5E5;
   }
   
    /* #AsideFirstBtn{
      
      padding-top: 10px; 
      width:100%; 
      height: 30px;
      background: #627192; 
      color: white; 
      text-align: center;
      cursor: pointer;
   }  */
   
    .AsideBtn{
      padding-top: 10px; 
      width:94%; 
      height: 40px;
      background: #FFFFFF; 
      color: 57585E; 
      text-align: left;
      cursor: pointer;
      border: 1px solid lightgray;
      padding-top: 20px;      
      font-size: 1.25em;
      padding-left: 10px;
   } 
</style>
</head>
<body>
      <div id="knowledgeAside">
         <div id="AsideFirstBtn" class ="AsideBtn" style="background: #627192; color: white; " onclick="location.href='allNotice.not'">공지사항</div>
         <div id="AsideFirstBtn2" class ="AsideBtn" onclick="location.href='deptNotice.not?loginUser=${loginUser.empDept}'">부서 게시판</div>
         <div id="AsideFirstBtn3" class ="AsideBtn" onclick="location.href='commonNotice.not'">일반 게시판</div>
         	<!-- <div style="margin-left: 25px;margin-top: 25px; width: 150px; height: 80%; background: white;">
         </div> -->
      </div>
      
      
      
      <script type="text/javascript">
      	$(".AsideBtn").click(function () {
      		$(this).parent().children().css({"background":"#FFFFFF","color":"#57585E"});
			$(this).css({"background":"#627192","color":"white"});
		});
      	
      	<%-- $(document).ready(function () {
      		<% String Typelist = request.getParameter("Typelist"); %>
			var boardType = '<%=Typelist%>';
			
			if(boardType == 'com'){
				$("#AsideFirstBtn3").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn3").css({"background":"#627192","color":"white"});
			}
			
			if(boardType == 'dept'){
				$("#AsideFirstBtn2").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn2").css({"background":"#627192","color":"white"});
			}
			
			if(boardType == 'not'){
				$("#AsideFirstBtn").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn").css({"background":"#627192","color":"white"});
			}
			
		}); --%>
      	
      	$(function () {
			var boardType = '${Typelist}';
			if(boardType == "com"){
				$("#AsideFirstBtn3").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn3").css({"background":"#627192","color":"white"});
			}
			
			if(boardType == "dept"){
				$("#AsideFirstBtn2").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn2").css({"background":"#627192","color":"white"});
			}
			
			if(boardType == "not"){
				$("#AsideFirstBtn").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn").css({"background":"#627192","color":"white"});
			}
			
		});
      </script>
</body>
</html>