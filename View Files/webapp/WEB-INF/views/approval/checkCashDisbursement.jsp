<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera</title>
<head>
<meta charset="UTF-8">
<link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="/opera/resources/se2/js/service/HuskyEZCreator.js" charset="utf-8"></script>
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<title>Insert title here</title>

<style>
body {
	font-family: Gothic A1;
}

.mainbutton {
	width: 200px;
	height: 49px;
	border-radius: 5px;
	color: white;
	background-color: #27334C;
	font-size: 22px;
}

.mainfont {
	font-size: 20px;
}

.mainbarfont {
	display: inline-block;
	width: 45%;
	font-size: 22px;
	font-weight: 600;
	padding-bottom: 5px;
}

.mainbarfont2 {
	display: inline-block;
	width: 45%;
	font-size: 14px;
	font-weight: 600;
}

.mainbarfont3 {
	display: inline-block;
	font-size: 18px;
	width: 60%;
}

.ddtag1 {
	padding-top: 10px;
	padding-left: 25px;
	color: #27334C;
	font-weight: 300;
	font-size: 16px;
	cursor: pointer;
	width: 40px;
}

button {
	border: 0;
	outline: 0;
}

.btn1 {
	font-size: 10px;
	width: 50px;
	height: 23px;
	margin-left: 5px;
	color: white;
	background-color: #27334C;
	border-radius: 1px;
}

#apporjoin td, th {
	border: 1px solid black;
	font-size: 13px;
	border: 2px solid #81858B;
}

#apporjoin th {
	background-color: #BEC2C9;
	font-weight: bold;
}

#apporjoin td {
	background-color: #EEEFF1;
	font-weight: 500;
}

.addprobtn {
	border: 1px solid #81848A;
	font-size: 10px;
	height: 20px;
	width: 70px;
	margin-left: 5px;
	text-align: center;
	vertical-align: middle;
}

#font .addtext {
	text-align: center;
	width: 100px;
	height: 26px;
	background-color: #B9BDC4;
	border-right: 1px solid #81848A;
	border-left: 0px;
	font-weight: 600;
	bottom-left:0px;
}

#font .addtext2 {
	text-align: center;
	width: 80px;
	height: 26px;
	background-color: #E8E8E8;
	font-weight: 600;
	border-right: 1px solid #81848A;
}

#font .adddtext2 {
	text-align: center;
	width: 170px;
	height: 26px;
	background-color: #E8E8E8;
	font-weight: 600;
	border-right: 1px solid gray;
}

#font .addtext3 {
	text-align: center;
	width: 100px;
	height: 40px;
	background-color: #B9BDC4;
	border: 1px solid #81848A;
	font-weight: 600;
	border-left: 0px;
}

#font .addtext4 {
	text-align: center;
	width: 100px;
	height: 50px;
	background-color: #B9BDC4;
	border: 1px solid #81848A;
	font-weight: 600;
	border-left: 0px;
}

#font {
	font-size: 10px;
}

#font tr {
	border: 1px solid #81848A;
}

.radiobtn {
	vertical-align: middle;
	margin-left: 10px;
	margin-right: 3px;
	width: 13px;
}
/* The Modal (background) */
.modal {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 1; /* Sit on top */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0, 0, 0); /* Fallback color */
	background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
	background-color: #fefefe;
	margin: 15% auto; /* 15% from the top and centered */
	padding: 20px;
	border: 1px solid #888;
	width: 70%; /* Could be more or less, depending on screen size */
	height: 55%
}
.modal-content2 {
	background-color: #fefefe;
	margin: 15% auto; /* 15% from the top and centered */
	padding: 20px;
	border: 1px solid #888;
	width: 50%; /* Could be more or less, depending on screen size */
	height: 55%
}
.modal-content3 {
	background-color: #fefefe;
	margin: 15% auto; /* 15% from the top and centered */
	padding: 20px;
	border: 1px solid #888;
	width: 30%; /* Could be more or less, depending on screen size */
	height: 20%
}
.modal-content4 {
	background-color: #fefefe;
	margin: 15% auto; /* 15% from the top and centered */
	padding: 20px;
	border: 1px solid #888;
	width: 33%; /* Could be more or less, depending on screen size */
	height: 50%
}
.adddocubtn {
	background-color: #27334C;
	color: white;
	font-size: 12px;
	height: 20px;
	width: 80px;
	border-radius: 2px;
}
.che2:hover{
		color: white;
		background-color: #27334C;
}
.che{
	color:black; background-color: #B9BDC4;
}

.che2{
	color:black; background-color: white; border: 1px solid #81848A;"
}
.ccc{
	color:black; background-color: white; border: 1px solid #81848A;"
}
.ccc:visit{
	color: white;
	background-color: #27334C;
}
.prolist{
	 border-radius:3px; 
	 vertical-align: middle; 
	 margin-left:5px; 
	 border:1px solid #81848A; 
	 width:60px; height:20px; color:white; background-color: #27334C; padding-left:5px;padding-right:5px;
	 display: inline-block;
}
.removebtn{
	 border:0px; color:red; background-color: #27334C; height: 15px; font-size:10px; margin-left:5px; margin-top:3px;
}

.addclientlisttd{
	vertical-align:middle; 
	width: 100px; 
	background-color: #C4C4C4; 
	border: 1px solid #81848A; 
	font-size: 10px; 
	padding-left:10px; 
	height:20px; 
	font-weight: 600;
}
.btn{
font-size: 10px; width:35px; height:25px; color:white;border-radius: 2px;
}

#submitform3{
	font-size: 10px; width:35px; height:25px; color:white;border-radius: 2px;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/common/operaNav.jsp" />
	<jsp:include page="/WEB-INF/views/approval/cashNav.jsp" />
	
	<c:set var="loop" value="false" />
	<div style="padding-top: 70px; padding-left: 350px;">
		<div style=" width:1100px; border-bottom: 0px;  border: 1px solid gray;">
			<div align="center"
				style="margin-top: 50px; font-size: 30px; font-weight: 500; ">
				지출결의서
				<hr>
				<c:set var="check" value="1"/>
				<c:forEach var="finish" items="${plist}">
				<c:if test="${not loop}">
					<c:if test="${finish.lostatus eq '미승인'}">
						<c:set var="check" value="0"/>
						<c:set var="loop" value="true"/>
					</c:if>
				</c:if>
				</c:forEach>
				
				<c:forEach var="plistCheck" items="${plist }" varStatus="status">
					<c:if test="${plistCheck.lostatus eq '선결'}">
						<c:set var="check" value="1"/>
					</c:if>
				</c:forEach>
				
				<c:set var="check2" value="1"/>
				<c:if test="${check == 1}">
					<c:forEach var="finish2" items="${plist}">
						<c:if test="${finish2.lostatus eq '반려'}">
							<c:set var="check2" value="0"/>
						</c:if>
						<c:if test="${finish2.lostatus eq '선결'}">
							<c:set var="check2" value="2"/>
						</c:if>
					</c:forEach>
				</c:if>
				
				<c:if test="${check == 1 }"> 
					<c:if test="${check2 == 1 }">
						<div style="float: right; margin-right:20px; color: blue; font-size:16px; padding-top: 15px; font-weight: 900; padding-bottom: 5px;">결제완료</div>
					</c:if>
					<c:if test="${check2 == 0 }">
						<div style="float: right; margin-right:20px; color: #E61358; font-size:14px; padding-top: 15px; font-weight: 900; padding-bottom: 5px;">결제반려</div>
					</c:if>
					<c:if test="${check2 == 2 }">
						<div style="float: right; margin-right:20px; color: #009D9D; font-size:14px; padding-top: 15px; font-weight: 900; padding-bottom: 5px;">선결승인</div>
					</c:if>
				</c:if>
				<c:if test="${check == 0 }">
					<div style="float: right; margin-right:20px; color: black; font-size:14px; padding-top: 15px; font-weight: 900;padding-bottom: 5px;">결재진행중</div>
				</c:if>
				<c:if test="${cdm.share eq 'O'}">
					<c:forEach var="plist" items="${plist}"> 
						<c:if test="${plist.loSort eq '기안'}">
							<div style="float: left; margin-left:20px; color: blue; font-size:12px; padding-top: 15px; font-weight: 900;  height: 40px;">[ ${plist.deptName} 공유문서]</div>
						</c:if>
					</c:forEach>
					
				</c:if>
				
			</div>
			<br><br>
			<!-- 결재나 협조 공간 -->
			<div id="apporjoin" style="float: right; display: block;">
				<div>
					<table id="appline"
						style="border-collapse: collapse; border: 1px solid #81848A; text-align: center; margin-right: 20px; margin-bottom: 10px;">
						<tr>
							<th style="width: 60px;" rowspan="2">결재</th>
							<th style="width: 70px; height: 20px;">작성자</th>
							<c:forEach var="plist" items="${plist}">
								<c:if test="${plist.loSort eq '결재' }">
									<th style="width: 70px; height: 20px;">${plist.empJobName}</th>
								</c:if>
							</c:forEach>
							
						</tr>
						<tr>
						<c:forEach var="plist" items="${plist}">
							
							<c:if test="${plist.loSort eq '기안' }">
									<td style="width: 70px; height: 60px;"><img style="width:50px;"src="/opera/resources/icons/approvalImages/제출도장.png"><br>${plist.empName }</td>
							</c:if>
							</c:forEach>
							<c:forEach var="plist" items="${plist}">
								
								<c:if test="${plist.loSort eq '결재' }">
									<td>
									<c:if test="${plist.lostatus eq '승인'}">
										<img style="width:50px;"src="/opera/resources/icons/approvalImages/승인도장.png"><br>
									</c:if>
									<c:if test="${plist.lostatus eq '반려' }">
										<img style="width:50px;"src="/opera/resources/icons/approvalImages/반려도장.png"><br>
									</c:if>
									<c:if test="${plist.lostatus eq '선결' }">
										<img style="width:50px;"src="/opera/resources/icons/approvalImages/선결도장.png"><br>
									</c:if>
									${plist.empName}</td>
								</c:if>
							</c:forEach>
						</tr>
					</table>
				</div>
				<div style="float: right;">
					<table id="appline2"
						style="border-collapse: collapse; border: 1px solid black; text-align: center; margin-right: 20px;">
						<tr>
							<th style="width: 60px; height: 80px;" rowspan="2">협조</th>
							<c:forEach var="plist" items="${plist}">
								<c:if test="${plist.loSort eq '협조' }">
									<th style="width: 70px; height: 20px;">${plist.empJobName}</th>
								</c:if>
							</c:forEach>
						</tr>
						<tr>
							<c:forEach var="plist" items="${plist}">
								<c:if test="${plist.loSort eq '협조' }">
									<td>
									<c:if test="${plist.lostatus eq '승인'}">
										<img style="width:50px;"src="/opera/resources/icons/approvalImages/협조도장.png"><br>
									</c:if>
									<c:if test="${plist.lostatus eq '반려' }">
										<img style="width:50px;"src="/opera/resources/icons/approvalImages/반려도장.png"><br>
									</c:if>
									<c:if test="${plist.lostatus eq '선결' }">
										<img style="width:50px;"src="/opera/resources/icons/approvalImages/선결도장.png"><br>
									</c:if>
									${plist.empName}</td>
								</c:if>
							</c:forEach>
						</tr>
						
					</table>
				</div>
			</div>
			<div id="font" style="margin-top: 220px; width: 100%">
				<table id="asd"
					style="width: 100%; border: 1px solid #81848A; border-collapse: collapse;">
					<tr>
						<td class="addtext">
							<div>수신참조</div>
						</td>
						<td style="background-color: #FAFAFA; vertical-align: middle;" colspan="9" >
						<div style="margin-left:5px; display:inline-block; " id="listpro">
							<c:if test="${not empty cdm.receiver}">
								<c:set var="receiverArr" value="${fn:split(cdm.receiver, ',')}"/>
								<c:forEach var="receiver" items="${receiverArr}">
									<div class="prolist" style="display:inline-block; text-align: center;border:0px; margin-left:5px;width:60px; height:20px; padding-top:2px;; margin-bottom: auto;">
										${receiver}<c:if test="${cdm.empNo eq loginUser.empNo && dr.drStatus eq '진행중'}"><button class="removebtn">X</button></c:if>
									</div>
								</c:forEach>
							</c:if>
						</div>
							<c:if test="${cdm.empNo eq loginUser.empNo && dr.drStatus eq '진행중'}">
								<button class="addprobtn" onclick="open_modal2();" style="">수신자 추가</button>
							</c:if>
						</td>
					</tr>
					<tr>
						<td class="addtext">
							<div>지출형식</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="9">
						<!-- 지출형식 -->
						<div style="margin-left: 20px; width: 200px;">
							<c:out value="${cdm.expendform}"/>
						</div>
						</td>
					</tr>
					<tr>
						<td class="addtext">
							<div>기안일</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="2">
							<div style="width: 250px; display: inline-block; margin-left:20px;">
								<fmt:formatDate value="${cdm.date}" pattern="yyyy-MM-dd HH시 mm분"/>
							</div>
						</td>
						<td class="addtext" colspan="1">부서</td>
						<td style="background-color: #FAFAFA;" colspan="2">
							<div style="width: 200px; display: inline-block; text-align: center;">
								<c:forEach var="plist" items="${plist}"> 
									<c:if test="${plist.loSort eq '기안'}">
										${plist.deptName}
									</c:if>
								</c:forEach>
							</div>
						</td>
						<td class="addtext" colspan="1">이름</td>
						<td style="background-color: #FAFAFA;" colspan="3">
							<div style="width: 200px; display: inline-block; text-align: center;">
								<c:forEach var="plist" items="${plist}"> 
									<c:if test="${plist.loSort eq '기안'}">
										${plist.empName}
									</c:if>
								</c:forEach>
							</div>
						</td>
					</tr>
					<tr>
						<td class="addtext">
							<div>제목</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="9">
							<div style="margin-left: 20px; ">
								<c:if test="${cdm.empNo eq loginUser.empNo && dr.drStatus eq '진행중'}">
									<input type="text" id="faketitle" style="margin-left: 0px; width: 98%; font-size: 10px; padding-left:5px; ;"value="${cdm.title}">
								</c:if>
								<c:if test="${cdm.empNo ne loginUser.empNo || (dr.drStatus eq '승인완료' && cdm.empNo eq loginUser.empNo)}">
									<c:out value="${cdm.title }"/>
								</c:if>
							</div>
						</td>
					</tr>
					<tr>
						<td class="addtext">
							<div>지급유형</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="4">
							<div style="width: 200px; display: inline-block; margin-left: 20px;">
								<c:out value="${cdm.paytype }"/>
							</div>
						</td>
						<td class="addtext" colspan="1">지급번호</td>
						<td style="background-color: #FAFAFA;" colspan="4">
							<div style="width: 330px; display: inline-block; margin-left:20px;">
								<c:out value="${cdm.paynumber}"/>
							</div>
						</td>
					</tr>

					<tr>
						<td class="addtext" colspan="2">
							<div>년월일</div>
						</td>
						<td class="addtext">
							<div>내용</div>
						</td>
						<td class="addtext">
							<div>금액</div>
						</td>
						<td class="addtext">
							<div>업체명</div>
						</td>
						<td class="addtext">
							<div>지급은행</div>
						</td>
						<td class="addtext" colspan="2">
							<div>계좌번호</div>
						</td>
						<td class="addtext">
							<div>예금주</div>
						</td>
						<td class="addtext">
							<div>비고</div>
						</td>

					</tr>
					<c:set var="paydateArr" value="${fn:split(cdm.paydate,',')}"/>
					<c:set var="paycontentArr" value="${fn:split(cdm.paycontent,',')}"/>
					<c:set var="paypriceArr" value="${fn:split(cdm.payprice,'/')}"/>
					<c:set var="paycompanyArr" value="${fn:split(cdm.paycompany,',')}"/>
					<c:set var="paybankArr" value="${fn:split(cdm.paybank,',')}"/>
					<c:set var="banknumberArr" value="${fn:split(cdm.banknumber,',')}"/>
					<c:set var="bankholderArr" value="${fn:split(cdm.bankholder,',')}"/>
					<c:set var="remarkArr" value="${fn:split(cdm.remark,',')}"/>
					
					<c:forEach var="paydate" items="${paydateArr }" begin="0" varStatus="status">
						<tr class="last">
							<td class="addtext2" colspan="2">
								<div>
									${paydate}
								</div>
							</td>
							<td class="addtext2">
								<div>
									${paycontentArr[status.index]}
								</div>
							</td>
							<td class="addtext2">
								<div>
									${paypriceArr[status.index] } 원
								</div>
							</td>
							<td class="adddtext2">
								<div>
									${paycompanyArr[status.index]}
								</div>
							</td>
							<td class="addtext2">
								<div>${paybankArr[status.index]}</div>
							</td>
							<td class="addtext2" colspan="2">
								<div>${banknumberArr[status.index]}</div>
							</td>
							<td class="addtext2">
								<div>${bankholderArr[status.index]}</div>
							</td>
							<td class="addtext2">
								<div>${remarkArr[status.index]}</div>
							</td>
						</tr>
					</c:forEach>
					
					<tr>
						<td colspan="11" style="height:2px;">
							
						</td>
					</tr>
					
					<tr>
						<td class="addtext">
							<div>합계</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="10">
							<div style="margin-left: 20px;">
								<fmt:formatNumber value="${cdm.deposit }" groupingUsed="true"/> 원
							</div>
						</td>
					</tr>
					<tr>
						<td class="addtext">
							<div>내용요약</div>
						</td>
						<td style="background-color: #FAFAFA; padding-left: 10px;" colspan="10">
							<c:if test="${cdm.empNo eq loginUser.empNo && dr.drStatus eq '진행중'}">
								<input type="text" class="fakesummary" 
							type="text" style="margin-left: 5px; width: 98%; padding-left:5px; font-size: 10px;"value="${cdm.summary}">
							</c:if>
							<c:if test="${cdm.empNo ne loginUser.empNo || (dr.drStatus eq '승인완료' && cdm.empNo eq loginUser.empNo)}">
								<c:out value="${cdm.summary}"/>
							</c:if>
						</td>
					</tr>
					<tr>
						<td class="addtext4" colspan="1" >
							<div>세부내용</div>
						</td>
						<td colspan="10">
							<c:if test="${cdm.empNo eq loginUser.empNo && dr.drStatus eq '진행중'}">
								<textarea name="ir1" id="ir1" rows="10" cols="100" style="width:980px; height: 300px;">
									${cdm.details }
								</textarea>
							</c:if>
							<c:if test="${cdm.empNo ne loginUser.empNo || (dr.drStatus eq '승인완료' && cdm.empNo eq loginUser.empNo)}">
								<div id="content" style="padding-left: 10px; padding-bottom: 10px;">
									${cdm.details}
								</div>
							</c:if>
						</td>
					</tr>
					<tr>
						<td class="addtext" colspan="7">
							<div style="display: inline-block;">파일명</div>
						</td>
						<td class="addtext" colspan="3">
							<div>크기</div>
						</td>
						
					</tr>
					<tr style="text-align: center;">
						<td colspan="7" style="padding-top: 9px;">
							<c:forEach var="fileList" items="${fileList }">
								<c:if test="${not empty fileList.fileCode}">
									<div onclick="downloadFile(this);" style=" height: 20px; cursor: pointer; font-size: 0.9em;"><i class='fas fa-file'></i>&nbsp;&nbsp;<input id="fileCode" type="hidden" name="fileCode" value="${fileList.fileCode}">${fileList.originName}</div>
								</c:if>
							</c:forEach>
						</td>
						<td colspan="3" style="padding-top: 9px;">
							<c:forEach var="fileList" items="${fileList }">
								<c:if test="${not empty fileList.fileCode}">
									<div style="cursor: pointer; font-size: 0.9em; height: 20px;"><i class='fas fa-file'></i>&nbsp;&nbsp;<input id="fileCode" type="hidden" name="fileCode">${fileList.fileSize}</div>
								</c:if>
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td class="addtext4" colspan="1" >
							<div>결재로그</div>
						</td>
						<td colspan="10">
							<div style="font-size: 10px; margin-left:10px; padding-top: 10px; padding-bottom: 5px;">
								<c:forEach var="plist" items="${plist}" varStatus="status">
								<div style="height: 18px;">
									<c:if test="${plist.loDate!=null}">
									<label style="font-weight: 600;">
										[<fmt:formatDate value="${plist.loDate}" pattern="yyyy-MM-dd HH:mm"/>]
									</label>
									</c:if>
									<c:if test="${plist.loDate==null}">
										<label style="font-weight: 600;">
											[0000-00-00 00:00]
										</label>
									</c:if>
									<c:if test="${plist.loSort eq '협조' }">
										<label style="color: blue; font-weight: 600;">
											[협조부서]
										</label>
									</c:if>
									<c:if test="${plist.loSort eq '결재' }">
										<label style="color: red; font-weight: 600;">
											[결재부서]
										</label>
									</c:if>
									  ${plist.deptName} &nbsp; ${plist.empName} &nbsp; ${plist.empJobName}&nbsp;
									 <c:if test="${plist.lostatus eq '승인' }">
										 <label style="color: red; font-weight: 600">${plist.loSort}</label>
									 </c:if>
									 <c:if test="${plist.lostatus eq '반려' }">
										 <label style="color: red; font-weight: 600">반려</label>
									 </c:if>
									  <c:if test="${plist.lostatus eq '선결' }">
										 <label style="color: red; font-weight: 600">선결</label>
									 </c:if>
								</div>
								</c:forEach>
								
							</div>
						</td>
					</tr>
					<tr>
						<td class="addtext4" colspan="1" >
							<div>결재의견</div>
						</td>
						<td colspan="10">
							<div id="content" style="padding: 10px; padding-bottom: 0px;">
								<c:forEach var="oplist" items="${payoplist}">
									<div><label style="font-weight: 600">${oplist.empName } ${oplist.jobName } </label> &nbsp; [<fmt:formatDate value="${oplist.opDate}" pattern="YYYY-MM-dd HH:mm:ss"/>]
										<c:if test="${loginUser.empName eq oplist.empName }">
											<button style="cursor:pointer; border: 1px solid red; color: red; background-color: white; font-size: 10px; height:16px; width: 30px;" class="delete" value=${oplist.opNo }>삭제</button>
										</c:if>
										<input type="hidden" class="delete" value="${oplist.opNo }">
									</div>
									<div style="margin-left:3px;margin-top:2px;">${oplist.opContent }</div>
									<br>
								</c:forEach>
							</div>
						</td>
					</tr>
					
					<tr>
						<td class="addtext4" style="height:50px;" colspan="1" >
							<div>의견작성</div>
						</td>
						<td colspan="10" style="border-bottom: 1px solid gray;">
							<div id="content" style="display: inline-block;">
								<input type="text" style="width:930px; height:35px; margin-left:5px;" id="Reply">
							</div>
								<button style="background-color: #27334C; color: white; width:52px; height:40px; border-radius: 3px;" onclick="insertReply();">등록</button>
						</td>
					</tr>
					
				</table>
				
				<c:if test="${not empty plist}">
					<c:forEach var="r" items="${plist}" varStatus="status">
						<c:if test="${r.empName eq loginUser.empName }">
							<c:set var="replist" value="${status.index - 1}"/>
							<c:set var="rp" value="${plist[replist] }"/>
						</c:if>
						<c:if test="${status.last }">
							<c:set var="replist2" value="${status.index}"/>
							<c:set var="rp2" value="${plist[replist2] }"/>
						</c:if>
					</c:forEach>
					<tr style="border: 0px; margin-top:10px;">
						<td colspan="11" style="height:30px; border: 1px solid white;  margin-left:0px;">
							<c:if test="${cdm.empNo eq loginUser.empNo && dr.drStatus eq '진행중'&& rp2.lostatus ne '선결'}">
								<div style="float: left; margin-right: 5px; margin-top: 5px; text-align: center; display: inline-block;">
									<button style="background-color: #27334C; color: white;" id="submitform3">수정</button>
								</div>
							</c:if>
							<div style="float: right; margin-right: 5px; margin-top: 5px; text-align: center; display: inline-block;">
								<c:forEach var="plist" items="${plist}" varStatus="status">
									<c:if test="${!status.last }">
										<c:if test="${plist.empName eq loginUser.empName }">
											<c:if test="${rp2.lostatus ne '선결'}">
												<c:if test="${rp.lostatus eq '승인' }">
													<c:if test="${plist.lostatus eq '미승인'}">
														<c:if test="${plist.loSort eq  '결재'}">
															<button class="btn" style="background-color: #27334C;">결재</button>
														</c:if>
														<c:if test="${plist.loSort eq  '협조'}">
															<button class="btn" style="background-color: #C95631;">협조</button>
														</c:if>
														<button class="btn" style="background-color: #FF3D3D;">반려</button>
													</c:if>
												</c:if>
											</c:if>
											<c:if test="${rp2.lostatus eq '선결' && plist.lostatus eq '미승인'}">
												<button class="btn" style="background-color: #27334C;">확인</button>
											</c:if>
										</c:if>
									</c:if>
									<c:if test="${status.last}">
										<c:if test="${plist.empName eq loginUser.empName}">
											<c:if test="${plist.lostatus eq '미승인'}">
											<c:if test="${plist.loSort eq  '결재'}">
													<button class="btn" style="background-color: #27334C;">결재</button>
												</c:if>
												<c:if test="${plist.loSort eq  '협조'}">
													<button class="btn" style="background-color: #C95631;">협조</button>
												</c:if>
											<button class="btn" style="background-color: #FF3D3D;">반려</button>
											<button class="btn" style="background-color: #009D9D;">선결</button>
										</c:if>
										</c:if>
									</c:if>
								</c:forEach>
							</div>
							
						</td>
					</tr>
					</c:if>
			</div>
		</div>
	</div>
	<br><br><br><br><br><br><br><br><br><br>

	<!-- The Modal2 -->
    <div id="myModal2" class="modal">
 
      <!-- Modal content -->
      <div class="modal-content2" style="">
            <p style="font-size: 17px;">직원목록</p>
            	<table style="font-size: 12px;">
            		<tr>
            			<td style="width: 200px;border: 1px solid black; height: 300px; padding-left:10px; padding-top:10px;" valign="top" >
            			
            			<button id="spread8" style="background-color: white;">
						<img id="img8"
							src="/opera/resources/icons/approvalImages/arrow.png">
						&nbsp;
							</button>OperaDemo
								<dl style="margin-left: 30px;" id="ddta3">
								<c:forEach var="deptList" items="${deptList }">
									<dt style="margin-top:2px;" class="deptList">${deptList.deptName}</dt>
								</c:forEach>
								</dl>
						</td>
						<td valign="top">
							<table style="margin-left:4px;">
								<tr>
									<td style="border: 1px solid #81848A; width:70px; height:30px; background-color: #B9BDC4; color: black; text-align: center;">성명</td>
									<td style="border: 1px solid #81848A; width:210px; height:30px; background-color: #B9BDC4; color: black;">
										<input onchange="print()" type="text" style="margin-left: 5px; height: 20px;" id="search2">
										<button style="width:40px; height:23px; color:white; background-color: #27334C; border-radius: 3px;" class="deptList">검색</button>
									</td>
								</tr>
							</table>
							<table  style="font-size: 10px; border-collapse: collapse; margin-left:5px;" id="modal2">
							<thead>
								<tr style=" text-align: center;">
									<td class="che" style="width:100px;height: 30px; border: 1px solid #81848A;">부서</td>
									<td class="che" style="width:100px; border: 1px solid #81848A;">성명</td>
									<td class="che" style="width:100px; border: 1px solid #81848A;">직위</td>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
							<tfoot>
								<tr style="text-align: center;">
									<td colspan="4" style="padding-top: 10px;">
										
									</td>
								</tr>
							</tfoot>
							</table>
						</td>
						<td>
							<button 
							style="border-radius:2px; color: white; background-color: #27334C; margin-bottom: 5px; font-size: 10px; width:50px; height: 20px;"
							onclick="addreceiver();"
							>
							추가
							</button>
						</td>
						<td valign="top">
							<table style="border-collapse: collapse; text-align: center; margin-left: 5px;" id="receivertable">
							<thead>
								<tr style="font-size: 10px; border: 1px solid #81848A;">
									<th style="border-right: 1px solid #81848A; width:30px; height:25px;">No</th>
									<th style="border-right: 1px solid #81848A; width:150px; height:20px;">받는이</th>
								</tr>
							<thead>
							</table>
						</td>
            		</tr>
            	</table>
            	<div style="float: right;">
            		<button style="width: 70px; height: 25px; font-size: 12px; background-color: #27334C; color: white; border-radius: 4px;" class="add_modal1" onclick="add2();">적용</button>
            		<button style="width: 70px; height: 25px; font-size: 12px;border: 1px solid #81848A; border-radius: 4px;"onClick="close_modal2();">닫기</button>
            	</div>
      </div>
 
    </div>
        <!--End Modal2-->
        
	<form method="post" action="updatePayLogOne.app" id="form">
		<input type="hidden" name="drNo" value="${plist[0].drNo}">
		<input type="hidden" name="lostatus" id="status">
		<input type="hidden" name="empNo" value="${loginUser.empNo }">
	</form>
	
	<form method="post" action="insertApprovalReply.app" id="form2">
		<input type="hidden" name="opContent" id="opContent"> 
		<input type="hidden" name="drNo" value="${plist[0].drNo }">
		<input type="hidden" name="empNo" value="${loginUser.empNo }">
	</form>
	<input type="hidden" id="drNo" value="${plist[0].drNo }">
	
	<form method="post" action="updateApproval.app" id="form3">
		<input type="hidden" name="receiver" id="receiver" value="${cdm.receiver }">
		<input type="hidden" name="summary" id="summary" value="${cdm.summary }">
		<input type="hidden" name="details" id="details" value="${cdm.details }">
		<input type="hidden" name="title" id="title" value="${cdm.title }">
		<input type="hidden" name="dNo" value="${cdm.dNo }">
	</form>
	
<script>
	$(function(){
		
	});
	function insertReply(){
		$("#opContent").val($("#Reply").val());
		$("#form2").submit();
	}
	
	$(document).on("click", ".delete",function(){
		console.log(this.value);
		location.href="deletePayOpinion.app?opNo=" + this.value + "&drNo=" +$("#drNo").val();
	});
	
	$(document).on("click", ".btn", function(){
		if(this.innerHTML == "결재" || this.innerHTML == "확인"){ 
			 if(this.innerHTML == "결재"){
				 if (confirm("문서를 결재 하시겠습니까?(Y/N)") == true){
						alert("결재 처리가 완료되었습니다.");
						$("#status").val("승인");
						$("#form").submit();
					 } else{
						 
					     return false;
					 }
			 } else {
				 if (confirm("확인처리 하시겠습니까?(Y/N)") == true){
						alert("확인처리가 완료되었습니다.");
						$("#status").val("승인");
						$("#form").submit();
					 } else{
						 
					     return false;
					 }
			 }
			
		} else if(this.innerHTML == "협조" ){
			if (confirm("문서를 협조 하시겠습니까?(Y/N)") == true){
				alert("협조 처리가 완료되었습니다.");
				$("#status").val("승인");
				$("#form").submit();
			 } else{
				 
			     return false;
			 }
		} else if(this.innerHTML == "반려" ){
			if (confirm("문서를 반려 하시겠습니까?(Y/N)") == true){
				alert("반려 처리가 완료되었습니다.");
				$("#status").val("반려");
				$("#form").submit();
			 } else{
				 
			     return false;
			 }
		} else if(this.innerHTML == "선결" ){
			if (confirm("문서를 선결 하시겠습니까?(Y/N)") == true){
				alert("선결 처리가 완료되었습니다.");
				$("#status").val("선결");
				$("#form").submit();
			 } else{
				 
			     return false;
			 }
		}
		
	});
	$(document).on("click", ".aaa", function(){
    	value = this.value * 1;
    	$("#modal1 tbody").empty();
    	$("#modal1 tfoot tr td").empty();
    	$("#modal2 tbody").empty();
    	$("#modal2 tfoot tr td").empty();
         $.ajax({
        	type:"POST",
        	url: "changeEmpList.app",
        	data: {
        		"dept" : dept,
        		"pageNum" : value
        	},
        	success: function(data){
        		for(var i = 0; i < data.length - 1; i++){
    			$("#modal1 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			$("#modal2 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			$("#modal3 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			} 
        		if(value - 1 == 0){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        		} else{
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        		}
        		
        		for(var i = 1; i <= data[data.length - 1].maxPage; i ++){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        		}
        		if(value == data[data.length - 1].maxPage){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        		} else{
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        		}
        	}
         });
         
    });
	function open_modal2(flag) {
        $('#myModal2').fadeIn(300);
        value = 1;
    	dept = "";
    	$("#modal1 tbody").empty();
    	$("#modal2 tbody").empty();
    	$("#modal3 tbody").empty();
    	
    	$("#modal1 tfoot tr td").empty();
    	$("#modal2 tfoot tr td").empty();
    	$("#modal3 tfoot tr td").empty();
    	
         $.ajax({
        	type:"POST",
        	url: "changeEmpList.app",
        	data: {
        		"dept" : dept,
        		"pageNum" : value
        	},
        	success: function(data){
        		for(var i = 0; i < data.length - 1; i++){
    			$("#modal2 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			} 
        		if(value - 1 == 0){
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        		} else{
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        		}
        		
        		for(var i = 1; i <= data[data.length - 1].maxPage; i ++){
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        		}
        		if(value == data[data.length - 1].maxPage){
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        		} else{
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        		}
        	}
         });
   };
	var receiverno = ${scount} + 1;
	console.log("receiverno : " + receiverno);
   function addreceiver(){
   	checking = 0;
   	$("#receivertable tr td").each(function(){
			if($(this).html() == (child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html() )){
				alert("중복하여 적용할 수 없습니다.");
				checking = 1;
			}
		});
		
		if(checking == 0){
			console.log("receiverno : " + receiverno);
			child.css({"color":"black","background-color":"white"});
			
			$("#receivertable").append(
					'<tr style="border: 1px solid #81848A;">' +
					'<td style="border-right: 1px solid #81848A; width:30px; height:25px;">' + receiverno +'</td>' +
					'<td style="border-right: 1px solid #81848A;">'+ child.find(':nth-child(2)').html()+child.find(':nth-child(3)').html() + '</td>' +
					'</tr>'
					);
			
			receiverno += 1;
			console.log("receiverno : " + receiverno);
		}
		console.log(1123123);
   }
   $(document).on("click",".ccc",function(){
		$(this).parent().children().css({"color":"black","background-color":"white"});
		$(this).css({"color":"white","background-color":"#27334C"});
		child = $(this);
	});
	function close_modal2(flag) {
       $('#myModal2').fadeOut(300);
   };
   
   var check = 1;
   var prolist = "";
   $(document).on("click",".removebtn",function(){
   	
   	$(this).parent().remove();
   	for(var i = 3; i < check; i ++){
   		if($(this).parent().text().substr(0,3) == $("#receivertable tr:nth-child("+ i +") td:nth-child(2)").text().substr(0,3)){
   			$("#receivertable tr:nth-child("+ i +")").remove();
   			receiverno -= 1;
   		}
   	}
   	for(var i = 0; i < receiverno; i ++){
   		$("#receivertable tr:nth-child("+(i+3)+")").find("td:nth-child(1)").text(i+1)
   	}
   	add2();
   });

   
   function add2(){
   	prolist = "";
   	check = 3;
   	$("#listpro").empty();
   		//$("#receivertable tr td:nth-child(2)")
   	$("#receivertable tr").each(function(index){
   		if(index == 0){
   			
   		}else if(index > 0){
   			$("#listpro").append(
               		'<div class="prolist">' +
           				'<label>'+$("#receivertable tr:nth-child("+ check +") td:nth-child(2)").text()+'</label>' +
           				'<button class="removebtn">X</button>' +
           			'</div>'	
               );
   			prolist += $("#receivertable tr:nth-child("+ check +") td:nth-child(2)").text() + ",";
   			check ++;
   		}
   	});
   	$("#receiver").val(prolist.substr(0,prolist.length-1));
   	$('#myModal2').fadeOut(300);
   }
</script>
    
	<!-- api -->
	<script type="text/javascript">
           var oEditors = [];
           nhn.husky.EZCreator.createInIFrame({
            oAppRef: oEditors,
            elPlaceHolder: "ir1",
            sSkinURI: "/opera/resources/se2/SmartEditor2Skin.html",
            //se2폴더 경로(프로젝트안에 넣어놓는게 좋음)
            fCreator: "createSEditor2"
           });
    </script>
    <script>
	    function submitContents(elClickedObj) {
		    // 에디터의 내용이 textarea에 적용된다.
		    oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
		    
		    // 에디터의 내용에 대한 값 검증은 이곳에서
		    // document.getElementById("ir1").value를 이용해서 처리한다.
		    
		    try {
		   		elClickedObj.form.submit();
		   		
		    } catch(e) {
		    	
		   	}
	    }
	</script>
	<script>
	$(document).on("click", "#submitform3", function(){
	    
	    if (confirm("문서를 수정 하시겠습니까?(Y/N)") == true){
	    	alert("수정이 완료되었습니다.");
			$("#title").val($("#faketitle").val()); // 제목
		    $("#summary").val($(".fakesummary").val());
		    oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
		    $("#details").val(document.getElementById("ir1").value);
		    $("#form3").submit();
		    
		 } else{
			 
		     return false;
		 }
	});
	function approvalDocuList(){
		location.href="selectDocuList.app";
	}
	
	$("#list tbody tr").click(function(){
		$("#oneDocu").val($(this).find(":nth-child(4)").text());
		$("#selectApprovalOne").submit();
	})
	$(document).on("click", ".ddtag1", function(){
		$("#status").val(this.innerHTML.slice(-2));
		console.log($("#status").val());
		$("#approvalInProgress").submit();
	});
	
	function downloadFile(da){
		var fileNum = da.childNodes[2].value;
		location.href="fileDownload.file?fileNo="+fileNum;
	}
	</script>
</body>
</html>