<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<style>
	body {
		font-family: Gothic A1;
	}
	
	.mainbutton {
		width: 200px;
		height: 49px;
		border-radius: 5px;
		color: white;
		background-color: #27334C;
		font-size: 22px;
	}
	
	.mainfont {
		font-size: 20px;
	}
	
	.mainbarfont {
		display: inline-block;
		width: 45%;
		font-size: 22px;
		font-weight: 600;
		padding-bottom: 5px;
	}
	
	.mainbarfont2 {
		display: inline-block;
		width: 45%;
		font-size: 14px;
		font-weight: 600;
	}
	
	.mainbarfont3 {
		display: inline-block;
		font-size: 18px;
		width: 60%;
	}
	
.ddtag1 {
	padding-top: 10px;
	padding-left: 25px;
	color: #27334C;
	font-weight: 300;
	font-size: 16px;
	cursor: pointer;
	width: 40px;
}
	
	button {
		border: 0;
		outline: 0;
	}
	.btn1{
		font-size: 10px;
		width: 50px;
		height: 23px;
		margin-left: 5px;
		color: white;
		background-color: #27334C;
		border-radius: 1px;
	}
	#list tr:hover{
		color: white;
		background-color: #27334C;
	}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/common/operaNav.jsp" />
	<jsp:include page="/WEB-INF/views/approval/cashNav.jsp" />
	
	<div style="padding-top: 70px; padding-left: 350px;">
		<img src="/opera/resources/icons/approvalImages/Doculist.png" style="width:25px;"><span
			style="font-weight: 600; font-size: 20px; padding-left: 10px;">양식관리</span><br><br>
			
			<table style="border: 0px solid #C4C4C4;">
			
			<tr style="height: 20px;">
				<td style=" padding-left:20px; padding-right:10px;border-right:1px solid #C4C4C4; font-size:12px; width:60px;">문서검색</td>
				<td style="width:340px; padding-left:20px; border-right:1px solid #C4C4C4;"> 
				<select style=" padding-left:5px;background-color: #27334C; border-radius: 1px; color:white;width:80px; height:23px; font-size: 10px;">
					<option>양식명</option>
					<option>약칭</option>
				</select>
				<input type="text" placeholder="검색명" style="padding-left:6px; height:19px;"> 
				<button class="btn1">검색</button>
				</td>
				<td>
					<a style="color: #E61358; font-weight: 600; font-size: 12px; padding-left:20px;">양식 관리</a>
				</td>
			</tr>
		</table>
		<br>
		<table id="list" style="border: 1px solid #C4C4C4; border-collapse: collapse; font-size: 11px;">
			<tr style="height: 35px; background-color: #27334C; color:white;">
				<th style="width:200px;">분류</th>
				<th style="width:200px;">사용여부</th>
				<th style="width:200px;">양식명</th>
				<th style="width:200px;">약칭</th>
				<th style="width:400px;">설명</th>
			</tr>
			<tr id="docu1" align="center" style="height: 30px;">
				<td>공통</td>
				<td>사용중</td>
				<td>지출결의서</td>
				<td>지결</td>
				<td>지출이 발생 시 회사 내부 관리상 지출 승인을 요청한다는 관리 서류</td>
			</tr>
			<tr id="docu2" align="center" style="height: 30px;">
				<td>공통</td>
				<td>사용중</td>
				<td>휴가신청서</td>
				<td>휴신</td>
				<td>휴가를 신청하기 위한 서식 문서</td>
			</tr>
			<tr id="docu3" align="center" style="height: 30px;">
				<td>인사</td>
				<td>사용중</td>
				<td>인사발령서</td>
				<td>인발</td>
				<td>보직에 변화가 있는 직원의 인사 이동을 공고하는 문서</td>
			</tr>
		
		</table><br>
		<div align="center">
		1
		</div>
		
	</div>
	
	<script>
		$(document).ready(function() {
			var flag = true;
			$("#spread").click(function() {
				if (!flag) {
					flag = true;
					$("#img1").css("transform", "rotate(0deg)");
					$("#ddta1").slideDown(500);
				} else {
					flag = false;
					$("#img1").css("transform", "rotate(270deg)");
					$("#ddta1").slideUp(500);

				}
			});

			var flag2 = true;
			$("#spread2").click(function() {
				if (!flag2) {
					flag2 = true;
					$("#img2").css("transform", "rotate(0deg)");
					$("#ddta2").slideDown(500);
				} else {
					flag2 = false;
					$("#img2").css("transform", "rotate(270deg)");
					$("#ddta2").slideUp(500);

				}
			});
		});
		
		function approvalDocuList(){
			location.href="selectDocuList.app";
		}
		
		$("#list tr").click(function(){
			console.log(this.id);
			location.href="selectApprovalDocu.app";
		});
		function approvalDocuList(){
			location.href="selectDocuList.app";
		}
		
		$(document).on("click", ".ddtag1", function(){
			$("#status").val(this.innerHTML);
			$("#approvalInProgress").submit();
		});
	</script>
</body>
</html>