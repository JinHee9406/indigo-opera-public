
/*설치한 express 모듈, socket.io, Node.js 기본내장모듈 불러오기 */
const express = require('express')
const socket = require('socket.io')
const http = require('http')
const fs = require('fs');
const multer = require('multer');
const multiparty = require('multiparty');

var upload =  multer({
    storage: multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null, 'static/resources/uploadFiles/');
      },
      filename: function (req, file, cb) {
        cb(null, file.originalname);
      }
    }),
  });
/*express 객체 생성*/
const app = express();

/*http 서버 생성*/
const server = http.createServer(app);
const io = socket(server);
// 미들웨어를 추가
app.use('/css', express.static('./static/css'));
//실행되는 서버코드 기준 디렉토리의
//static 폴더 안의 css 폴더는 외부 사용자들이 /css 경로로 엑세스 할수 있다.
app.use('/js', express.static('./static/js'));
app.use('/resources/images', express.static('./static/resources/images'));
app.use('/resources/uploadFiles', express.static('./static/resources/uploadFiles'));

let room = new Array();
let roomNum = 0;
let memberName;
//오라클 연동부분
const oracledb = require('oracledb');
const { response } = require('express');

oracledb.autoCommit = true;

var messengerName;


//---------------------------------------
/* Get 방식으로 / 경로에 접속하면 실행 됨 */
app.get('/', function(request, response) {  //fs모듈을  사용하여 index.html 파일을읽고 클라이언트 로 읽은 내용을 전달.
    fs.readFile('./static/index.jsp', function(err,data){
        if(err){
            response.send('에러')
        } else{
            response.writeHead(200, {'Content-Type':'text/html'})
            response.write(data)
            response.end()
        }
    })
})
//get(경로, 함수) 경로를 지정해주고 함수를 작성해야 합니다.
app.get("/download", function(req, res){

    console.log(req);
    // 요청시 해당 파일의 id값을 쿼리로 붙여서 전달합니다.(선택된 파일을 DB에서 찾기 위해)
    //  var filePath = __dirname + "/../upload/" + req.originalname; // 다운로드할 파일의 경로​     
    // console.log(filePath);
    //  var fileName =  req.originalname;// 원본파일명​
 
    //  // 응답 헤더에 파일의 이름과 mime Type을 명시한다.(한글&특수문자,공백 처리)
 
    //  res.setHeader("Content-Disposition", "attachment;filename=" + encodeURI(fileName));
 
    //  res.setHeader("Content-Type","binary/octet-stream");
 
    //  // filePath에 있는 파일 스트림 객체를 얻어온다.(바이트 알갱이를 읽어옵니다.)
 
    //  var fileStream = fs.createReadStream(filePath);
 
    //  // 다운로드 한다.(res 객체에 바이트알갱이를 전송한다)
 
    //  fileStream.pipe(res);
  });

//함수는 request와 response 객체를 받습니다.
io.on('connection',function(socket){//connection 이벤트 발생시 아래 실행
    
    app.post('/upload', function(req, res){
        var form = new multiparty.Form({
            autoFiles: false, // 요청이 들어오면 파일을 자동으로 저장할 것인가
            uploadDir: 'static/resources/uploadFiles', // 파일이 저장되는 경로(프로젝트 내의 temp 폴더에 저장됩니다.)
            maxFilesSize: 1024 * 1024 * 5 // 허용 파일 사이즈 최대치
        });
     
        form.parse(req, function (error, fields, files) {
            // 파일 전송이 요청되면 이곳으로 온다.
            // 에러와 필드 정보, 파일 객체가 넘어온다.
            var path = files.fileInput[0].path;
            res.send(path); // 파일과 예외 처리를 한 뒤 브라우저로 응답해준다.
        });
    })

    app.post('/upload2', upload.single('fileInput2'), function(req, res){
        console.log(req.file);
        res.send(req.file); 
    })


   

    socket.on('newUser', function(num, name, realName){
        //소켓에 이름 저장해두기
        socket.name = name
        socket.realName = realName;
        memberName = realName;
        console.log(realName);

        io.to(room[num]).emit('update',{type: 'connect', realName: 'SERVER', message: name+'님이 접속하였습니다.'})
        //console.log(room[num] + "  "+name+'님이 접속하였습니다.');
        //emit으로 다른 유저들에게도 알림
    })

    socket.on('disconnect', function(){
        socket.broadcast.to(room[roomNum]).emit('update',{type: 'disconnect', realName: 'SERVER', message: socket.name+'님이 나가셨습니다.'})
    })

    socket.on('loadUserList', function(){  
        oracledb.getConnection({
            user: "admin",
            password: "1q2w3e4r",
            connectString: "15.165.176.176/orcl"
        }, function(err, conn){
            if(err){
                console.log('접속실패', err);
                return;
            }
            var query = 'SELECT E.EMP_NO, E.EMP_NAME, D.DEPT_NAME, J.JOB_NAME FROM EMPLOYEE E JOIN DEPARTMENT D ON (E.EMP_DEPT = D.DEPT_CODE) JOIN JOB J ON(E.EMP_JOBCODE = J.JOB_CODE)';

            conn.execute(query,{},{outFormat:oracledb.OBJECT}, function (err, result) {  
                // Json 형태로 넘어오도록 설정
                    if(err) {
                        doRelease(conn);
                        return;
                    }

                    socket.emit('loadUserTree',result);
                    doRelease(conn, result);
            });
        });
        //--------오락굴연결종료-----------
        function doRelease(conn, roomlist){
            conn.close(function(err){
                if(err){
                    console.error(err.message);
                }            
            })
        }
    })

    //부서 목록을 불러온다.---------------------------
    socket.on('loadDeptList', function(){

        oracledb.getConnection({
            user: "admin",
            password: "1q2w3e4r",
            connectString: "15.165.176.176/orcl"
        }, function(err, conn){
            if(err){
                console.log('접속실패', err);
                return;
            }

            var query = 'SELECT * FROM DEPARTMENT';

            conn.execute(query,{},{outFormat:oracledb.OBJECT}, function (err, result) {  
                // Json 형태로 넘어오도록 설정
                    if(err) {
                        doRelease(conn);
                        return;
                    }
                    //console.log('query read success');
                    //dataStr = JSON.stringify(result);       
                   // var userNameAndJob = result.rows[0].EMP_NAME;

                   socket.emit('loadDeptTree',result);
                    doRelease(conn, result);
            });
        });
        //--------오락굴연결종료-----------
        function doRelease(conn, roomlist){
            conn.close(function(err){
                if(err){
                    console.error(err.message);
                }            
            })
        }
    })
    //---------------------------------------
    //메신저 첫실행시-------------------------
    socket.on('firstConn', (name, realName) => {
                oracledb.getConnection({
                    user: "admin",
                    password: "1q2w3e4r",
                    connectString: "15.165.176.176/orcl"
                }, function(err, conn){
                    if(err){
                        console.log('접속실패', err);
                        return;
                    }

                    var query = 'SELECT E.EMP_NAME, J.JOB_NAME FROM EMPLOYEE E JOIN JOB J ON (E.EMP_JOBCODE = J.JOB_CODE) WHERE E.EMP_NO = ' + name;
        
                    conn.execute(query,{},{outFormat:oracledb.OBJECT}, function (err, result) {  
                        // Json 형태로 넘어오도록 설정
                            if(err) {
                                doRelease(conn);
                                return;
                            }
                            //console.log('query read success');
                            //dataStr = JSON.stringify(result);       
                            var userNameAndJob = result.rows[0].EMP_NAME;
                            var userNameAndJob2 = result.rows[0].JOB_NAME;

                            realName = userNameAndJob + " " + userNameAndJob2;
                            socket.realName = realName;

                            socket.emit('checkUserName',realName);
                            doRelease(conn, result);
                    });
                });
                //--------오락굴연결종료-----------
                function doRelease(conn, roomlist){
                    conn.close(function(err){
                        if(err){
                            console.error(err.message);
                        }            
                    })
                }
                //---------------------------
                //대화방 조회 쿼리
                oracledb.getConnection({
                    user: "admin",
                    password: "1q2w3e4r",
                    connectString: "15.165.176.176/orcl"
                }, function(err, conn){
                    if(err){
                        console.log('접속실패', err);
                        return;
                    }

                    var query = 'SELECT * FROM CHATMEMBER CM JOIN CHATROOM CR ON ( CM.CHATROOM_NO = CR.CHATROOM_NO) WHERE CM.EMP_NO = ' + name +'AND CM.CHECK_ROOM != 3';
        
                    conn.execute(query,{},{outFormat:oracledb.OBJECT}, function (err, result) {  
                        // Json 형태로 넘어오도록 설정
                            if(err) {
                                doRelease(conn);
                                return;
                            }    
                            if( result.rows != ""){
                                var roomList = result.rows.toString();
                                var roomDetail = roomList.split(",");
                                
                                for(var i = 0; i < roomDetail.length; i++){

                                    room.push(result.rows[i].CHATROOM_NO);
                                }

                                socket.emit('callRoomList',result);
                            }
                            doRelease(conn, result);
                    });
                });
                //--------오락굴연결종료-----------
                function doRelease(conn, roomlist){
                    conn.close(function(err){
                        if(err){
                            console.error(err.message);
                        }            
                        //console.log(roomlist.length);
                        //response.send(roomlist);
                    })
                }
                //---------------------------

    })

    socket.on('outRoom', (num, name) => {
        socket.leave(num, () => {
            //console.log(name + ' leave a ' + room[num]);
            io.to(num).emit('leaveRoom,', num, name);

            oracledb.getConnection({
                user: "admin",
                password: "1q2w3e4r",
                connectString: "15.165.176.176/orcl"
            }, function(err, conn){
                if(err){
                    console.log('접속실패', err);
                    return;
                }
                var query = 'UPDATE CHATMEMBER SET CHECK_ROOM = 3 WHERE EMP_NO = ' + name + ' AND CHATROOM_NO = ' + num;       
            
                conn.execute(query, function (err, result) {  
                    // Json 형태로 넘어오도록 설정
                        if(err){
                            console.error(err.message);
                            doRelease(conn);
                            return;
                        }  
                        var query = 'SELECT * FROM CHATMEMBER CM JOIN CHATROOM CR ON ( CM.CHATROOM_NO = CR.CHATROOM_NO) WHERE CM.EMP_NO = ' + name +'AND CM.CHECK_ROOM != 3';
        
                        conn.execute(query,{},{outFormat:oracledb.OBJECT}, function (err, result) {  
                            // Json 형태로 넘어오도록 설정
                                if(err) {
                                    doRelease(conn);
                                    return;
                                }    
                                if( result.rows != ""){
                                    var roomList = result.rows.toString();
                                    var roomDetail = roomList.split(",");
                                    for(var i = 0; i < roomDetail.length; i++){
                                        room.push(result.rows[i].CHATROOM_NO);
                                    }
                                    socket.emit('callRoomList',result);
                                } else {
                                    socket.emit('removeRoomList');
                                }
                                io.to(num).emit('update',{type: 'disconnect', realName: 'SERVER', message: socket.realName+'님이 나가셨습니다.'})
                                doRelease(conn, result);
                        });
                });        
            })
    
            function doRelease(connection){
                connection.close(function(err){
                    if(err){
                        console.error(err.message);
                    }
                })
            }
        });
    });

    socket.on('makeRoom', (num, name) => {
        var userNum = num.split(',');
        oracledb.getConnection({
            user: "admin",
            password: "1q2w3e4r",
            connectString: "15.165.176.176/orcl"
          }, function(err, conn){
            if(err){
                console.log('접속실패', err);
                return;
            }
            var query = 'INSERT INTO CHATROOM VALUES (SEQ_CHATROOM_NO.NEXTVAL, :CHATROOM_HOST, SYSDATE)'
        
            var bindata = [
              name
            ];

            conn.execute(query, bindata, function (err, result) {  
                // Json 형태로 넘어오도록 설정
                    if(err){
                        console.error(err.message);
                        doRelease(conn);
                        return;
                    } 

                    for(var i = 0; i < userNum.length; i++){
                        var query = 'INSERT INTO CHATMEMBER VALUES (SEQ_CHATMEMBER_NO.NEXTVAL, SEQ_CHATROOM_NO.CURRVAL, :EMP_NO, DEFAULT)'
                    
                        var bindata = [
                            userNum[i]
                        ];
                    
                    
                        conn.execute(query, bindata, function (err, result) {  
                            // Json 형태로 넘어오도록 설정
                                if(err){
                                    console.error(err.message);
                                    doRelease(conn);
                                    return;
                                }  
                        });
                                    
                    }  

                    var query = 'SELECT SEQ_CHATROOM_NO.CURRVAL FROM DUAL'
             
                    conn.execute(query, function (err, result) {  
                        // Json 형태로 넘어오도록 설정
                            if(err){
                                console.error(err.message);
                                doRelease(conn);
                                return;
                            }  
                            socket.emit('moveNewRoom', result);
                    });
                        //console.log("방멤버: " + userNum[i])             
            });
        socket.broadcast.emit('checkRoomUpdate');
        })

    })

    socket.on('leaveRoom', (num, name) => {
        socket.leave(num, () => {
            //console.log(name + ' leave a ' + room[num]);
            io.to(num).emit('leaveRoom,', num, name);

            //console.log(socket.name + '님이 나가셨습니다.')
            //io.to(room[num]).emit('update',{type: 'disconnect', name: 'SERVER', message: socket.name+'님이 나가셨습니다.'})
        });
    });


    socket.on('joinRoom', (num, name) => {
        //대화내용 조회 쿼리
        console.log("방번호" + num);
        oracledb.getConnection({
            user: "admin",
            password: "1q2w3e4r",
            connectString: "15.165.176.176/orcl"
        }, function(err, conn){
            if(err){
                console.log('접속실패', err);
                return;
            }

            var query = 'SELECT * FROM CHAT WHERE CHATROOM_NO = ' + num + 'ORDER BY CHAT_TIME';

            conn.execute(query, {}, {outFormat:oracledb.OBJECT}, function (err, result) {  
                // Json 형태로 넘어오도록 설정
                    if(err) throw err; 
                    var messagesString = result.rows.toString();
                    var messagesLength = messagesString.split(",");

                    for(var i = 0; i <messagesLength.length; i++){
                        socket.emit('loadMessage', result.rows[i]);  
                    }
            });
        });
        //--------오락굴연결종료-----------
        function doRelease(connection, userlist){
            connection.close(function(err){
                if(err){
                    console.error(err.message);
                }

                response.send(userlist);
            })

        }
        //---------------------------*/

        socket.join(num, () => {
            //console.log(name + ' join a ' + num);
            // console.log(messengerName +'님이 접속하였습니다.');
            io.to(num).emit('joinRoom', num, name);

            roomNum = num;

            socket.name = name


            oracledb.getConnection({
                user: "admin",
                password: "1q2w3e4r",
                connectString: "15.165.176.176/orcl"
              }, function(err, conn){
                if(err){
                    console.log('접속실패', err);
                    return;
                }
                var query = 'SELECT * FROM CHATMEMBER WHERE EMP_NO = ' + name + ' AND CHATROOM_NO = ' + num;
            
                conn.execute(query, function (err, result) {  
                    // Json 형태로 넘어오도록 설정
                        if(err){
                            console.error(err.message);
                            doRelease(conn);
                            return;
                        }  

                        if(result.rows[0][3] == 0){
                            io.to(num).emit('update',{type: 'connect', realName: 'SERVER', message: socket.realName + '님이 접속하였습니다.'})
                               
                                oracledb.getConnection({
                                    user: "admin",
                                    password: "1q2w3e4r",
                                    connectString: "15.165.176.176/orcl"
                                }, function(err, conn){
                                    if(err){
                                        console.log('접속실패', err);
                                        return;
                                    }
                                    var query = 'UPDATE CHATMEMBER SET CHECK_ROOM = 1 WHERE EMP_NO = ' + name + ' AND CHATROOM_NO = ' + num;       
                                
                                    conn.execute(query, function (err, result) {  
                                        // Json 형태로 넘어오도록 설정
                                            if(err){
                                                console.error(err.message);
                                                doRelease(conn);
                                                return;
                                            }  
                                    });
                                })
                        
                                function doRelease(connection){
                                    connection.close(function(err){
                                        if(err){
                                            console.error(err.message);
                                        }
                                    })
                                }
                        }
                });
    
                
            //io.broadcast.to(room[data.num]).emit('update', data);
            })
    
            function doRelease(connection, userlist){
                connection.close(function(err){
                    if(err){
                        console.error(err.message);
                    }
    
                    response.send(userlist);
                })
    
            }



       
        });

    });

    
    socket.on('message', function(data){
        data.name = socket.name
        console.log(data);
        //io.to(room[data.num]).broadcast.emit('update', data);
        //io.sockes.emit() 모든유저 - 본인포함
        // socket.broadcast.emit 본인을 제외한나머지모두
        data.realName = socket.realName;

        socket.broadcast.to(data.num).emit('update', data);    

        oracledb.getConnection({
            user: "admin",
            password: "1q2w3e4r",
            connectString: "15.165.176.176/orcl"
          }, function(err, conn){
            if(err){
                console.log('접속실패', err);
                return;
            }
            var query = 'INSERT INTO CHAT VALUES (SEQ_CHAT_NO.NEXTVAL, :CHAT_DETAIL, SYSTIMESTAMP, :CHATROOM_NO, :CHAT_EMPNO, :CHAT_EMPNAME, :CHAT_TYPE)'
        
            var bindata = [
              data.message,
              data.num,
              data.name,
              data.realName,
              data.type
            ];
        
        
            conn.execute(query, bindata, function (err, result) {  
                // Json 형태로 넘어오도록 설정
                    if(err){
                        console.error(err.message);
                        doRelease(conn);
                        return;
                    }  
            });

            
        //io.broadcast.to(room[data.num]).emit('update', data);
        })

        function doRelease(connection, userlist){
            connection.close(function(err){
                if(err){
                    console.error(err.message);
                }

                response.send(userlist);
            })

        }
    })
})

server.listen(8080,function(){
    console.log('서버 실행중..')

})