<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
   <title>chatting</title>
   <link rel="stylesheet" href="/css/index.css">
   <link rel="stylesheet" href="/css/treemenu.css">
   <link rel="stylesheet" href="/css/mainStyle.css">
   <script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script src="/socket.io/socket.io.js"></script>
   <style>
     body{
       overflow: hidden;
     }
   </style>
</head>
<body>
  <script src="/js/index.js"></script>
  <div id="loading" style="width: 400px; height: 600px; font-weight: bold; font-size: 2em; color: white; padding: 0px; margin: 0px;">
    <div style="padding-top: 200px; padding-left: 52px;">
      <img width="200px" height="60px" src="/resources/images/operaTitle.jpg">
    </div>
  </div>
  <div id="main" style="display: none;">
      <div id="mainHeader" style="width: 100%; height: 70px; background: #27334C; margin-top: -10px;">
        <div style="margin-left: -195px; padding-top: 15px;"><img width="170px" height="47px" src="/resources/images/operaTitle.jpg"></div>
      </div>
      <div id="headerMenu">
        <table>
          <tr>
            <td><div id="person" class="selectBtn" onclick="selectMenu('person');"><img id="iconImg1" width="20px" height="20px" src="/resources/images/person_white.png"></div></td>
            <td><div id="talk" onclick="selectMenu('talk');"><img id="iconImg2" width="20px" height="20px" src="/resources/images/talk.png"></div></td>
            <td><div id="vector" onclick="selectMenu('vector');"><img id="iconImg3" width="20px" height="20px" src="/resources/images/Vector.png"></div></td>
          </tr>
        </table>
      </div>

      <div id="messengerMain" style="display: none; width: 300px; height: 500px; margin-left: 5px;">
        <div id="loginUserCon"></div>
        <div>
             <!-- 트리메뉴-->
          <div class="memCon">
            <!-- 마지막 리스트부분에 class="last",class="end" 넣어주세요 -->
            <div class="tree_box"  style="margin-left: -20px;">
                <div class="con">
                    <ul id="tree_menu" class="tree_menu" style="margin-top: -30px;">
                        <li class="depth_1"><strong style="cursor: pointer; margin-left: 40px;">전체직원</strong>
                            <ul class="depth_2" id="treeDept">
                                <li>
                                    <a href="#none"><em>폴더</em>경영진</a>
                                    <ul class="depth_3" id="lanker">
                                    </ul>
                                </li>
                                <li>
                                    <a href="#none" ><em>폴더</em>부서별직원</a>
                                    <ul class="depth_3" id="inner">
                                    </ul>
                                </li>
                                <li class="last">
                                    <a href="#none"><em>폴더</em>외부직원</a>
                                    <ul class="depth_3" id="outer">
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <button id="createRoomBtn" onclick="makeRoom();">
              채팅방 개설하기
            </button>
          </div>
            <!--//트리메뉴-->	
        </div>
      </div>

      <div id="chatroomCont"  style="display: none;">
        <div id="chatroomLists" style="width: 300px; height: 500px; margin-left: 5px;">
        </div>
          <div id="backBtn" onclick="out()"><img style="margin-top: 12px;" width="25px" height="25px" src="/resources/images/back.png"></div>
          <div id="outBtn" onclick="outForever()"><img style="margin-top: 12px;" width="25px" height="25px" src="/resources/images/logout.png"></div>
          <div id="chat" style="display: none;">
              <!-- 채팅 메시지 영역 -->
          </div>
          <div id="inputChat" style="display: none; margin-left: -180px;">
            <div id="addFilesBar">
              <button onclick="closeBar();" style="float: right; border: none; margin-right: 100px; background: none; outline: none; color: rgb(255, 255, 255)">▼</button>
              <div style="text-align: left;">
                <form id="fileForm" action="/upload" enctype="multipart/form-data" method="post">
                  <div class="filebox bs3-primary preview-image" style="float: left; margin-top: 5px; margin-left: 5px;">
                    <label for="fileInput" style="width: 50px; height: 50px;"><img width="80%" height="80%" style="margin-left: 5px; margin-top: 5px;" src="/resources/images/imgUpload.png"></label> 
                    <input id="fileInput" type="file" name="fileInput" class="upload-hidden"/>
                  </div>
                  <div class="filebox bs3-primary preview-image" style="float: left; margin-top: 5px; margin-left: 5px;">
                    <label for="fileInput2" style="width: 50px; height: 50px;"><img width="80%" height="80%" style="margin-left: 5px; margin-top: 5px;" src="/resources/images/document.png"></label> 
                    <input id="fileInput2" type="file" name="fileInput2" class="upload-hidden2"/>
                  </div>
                </form>
              </div>
            </div>
            <input type="text" id="test" onkeypress="press();" placeholder="메시지를 입력해주세요..">
            <button onclick="send()">전송</button>
            <button style="margin-top: 5px; background:none; border: none; outline: none; color: rgb(144, 144, 144)" onclick="openBar();">▲</button>
          </div>
      </div>

      <div id="vectorView" style="display: none; width: 300px; height: 500px; margin-left: 5px;">

      </div>
  
    </div>
    <script>
    var Sendname;
      function openBar(){
        $("#addFilesBar").animate({
          bottom: 80 
        })
      }
      function closeBar(){
        $("#addFilesBar").animate({
          bottom: 0
        })
      }
      $(document).ready(function(){
        var fileTarget = $('.filebox .upload-hidden');

        var fileTarget2 = $('.filebox .upload-hidden2');
        //-------------------------------------------------------
          fileTarget.on('change', function(){
              if(window.FileReader){
                  // 파일명 추출
                  var filename = $(this)[0].files[0].name;
              } 

              else {
                  // Old IE 파일명 추출
                  var filename = $(this).val().split('/').pop().split('\\').pop();
              };

              $(this).siblings('.upload-name').val(filename);
          });

          //preview image 
          var imgTarget = $('.preview-image .upload-hidden');

          imgTarget.on('change', function(){
              var parent = $(this).parent();
              parent.children('.upload-display').remove();
              sendFile();
              if(window.FileReader){
                  //image 파일만
                  if (!$(this)[0].files[0].type.match(/image\//)) return;
                  
                  var reader = new FileReader();
                  reader.onload = function(e){
                      var src = e.target.result;
                      //parent.prepend('<div class="upload-display"><div class="upload-thumb-wrap"><img src="'+src+'" class="upload-thumb"></div></div>');
                  }
                  reader.readAsDataURL($(this)[0].files[0]);
              }

              else {
                  $(this)[0].select();
                  $(this)[0].blur();
                  var imgSrc = document.selection.createRange().text;
                 // parent.prepend('<div class="upload-display"><div class="upload-thumb-wrap"><img class="upload-thumb"></div></div>');

                  var img = $(this).siblings('.upload-display').find('img');
                  img[0].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enable='true',sizingMethod='scale',src=\""+imgSrc+"\")";        
              }
          });
          //-------------------------------------------------------

          //-------------------------------------------------------
          fileTarget2.on('change', function(){
              if(window.FileReader){
                  var filename = $(this)[0].files[0].name;
              } 
              else {
                  var filename = $(this).val().split('/').pop().split('\\').pop();
              };

              $(this).siblings('.upload-name').val(filename);
              Sendname = filename;
          });
          //sendFile2(Sendname);
          //preview image 
          var imgTarget = $('.preview-image .upload-hidden2');
          imgTarget.on('change', function(){
              var parent = $(this).parent();
              parent.children('.upload-display').remove();
              sendFile2(Sendname);
              if(window.FileReader){
                  //image 파일만
                  if (!$(this)[0].files[0].type.match(/image\//)) return;
                  
                  var reader = new FileReader();
                  reader.onload = function(e){
                      var src = e.target.result;
                      //parent.prepend('<div class="upload-display"><div class="upload-thumb-wrap"><img src="'+src+'" class="upload-thumb"></div></div>');
                  }
                  reader.readAsDataURL($(this)[0].files[0]);
              }

              else {
                  $(this)[0].select();
                  $(this)[0].blur();
                  var imgSrc = document.selection.createRange().text;
                 // parent.prepend('<div class="upload-display"><div class="upload-thumb-wrap"><img class="upload-thumb"></div></div>');

                  var img = $(this).siblings('.upload-display').find('img');
                  img[0].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enable='true',sizingMethod='scale',src=\""+imgSrc+"\")";        
              }
          });
      });

    </script>
    <script> 
      function press(){ 
        if(window.event.keyCode == 13){ 
          send();
        }
      }
    </script>

    <script>
      let room = new Array();
      let num;
      var parent = window.parent; 
      var realName = "";
      var selectedMember= "";
      var selectedMemberName= "";
      var selectedRoom = false;

      window.addEventListener("message", postMessageController, true);
      function postMessageController(e){
        name = e.data;
        connection();
      }

      function connection(){
        if(name == ""){
          name = prompt("반갑습니다!", '');
        }
       // document.getElementById("userId").value = name;
        $("#loading").fadeOut(1000);
        setTimeout(function(){
          $("#loading").css("display","none");
          $("#main").fadeIn(1000);
        },1000);
        setTimeout(function(){
          $("#main").css("display","block");
          $("#messengerMain").css("display","block");
         /*socket.on('connect', function(){
                socket.emit('newUser', num, name)
          })  */

          socket.emit('firstConn', name, realName);
          selectedMember += name +",";
          socket.emit('loadUserList');
        },1000);
      }

      setTimeout(function(){
        if(name == "") {
          connection();
        }
      },2000);
      // socket.emit('joinRoom', num, name);
      // console.log(name);
//메뉴를 고를때 발생
      function selectMenu(menuBtn){
        switch(menuBtn){
          case "person":
            $("#iconImg1").attr("src","/resources/images/person_white.png")
            $("#person").attr("class","selectBtn")

            $("#iconImg2").attr("src","/resources/images/talk.png")
            $("#talk").removeAttr("class");

            $("#iconImg3").attr("src","/resources/images/Vector.png")
            $("#vector").removeAttr("class");

            $("#messengerMain").css("display","block");
            $("#chatroomCont").css("display","none");
            $("#vectorView").css("display","none");
            $('#backBtn').css("display","none");
            $('#outBtn').css("display","none");

          break;
          case "talk":
            $("#iconImg1").attr("src","/resources/images/person.png")
            $("#person").removeAttr("class")

            $("#iconImg2").attr("src","/resources/images/talk_white.png")
            $("#talk").attr("class","selectBtn");

            $("#iconImg3").attr("src","/resources/images/Vector.png")
            $("#vector").removeAttr("class");

            $("#messengerMain").css("display","none");
            $("#chatroomCont").css("display","block");
            $("#vectorView").css("display","none");

            if(selectedRoom == false){
              $('#backBtn').css("display","none");
              $('#outBtn').css("display","none");
            } else {
              $('#backBtn').css("display","block");
              $('#outBtn').css("display","block");
            }
          break;
          case "vector":
            $("#iconImg1").attr("src","/resources/images/person.png")
            $("#person").removeAttr("class")

            $("#iconImg2").attr("src","/resources/images/talk.png")
            $("#talk").removeAttr("class");

            $("#iconImg3").attr("src","/resources/images/Vector_white.png")
            $("#vector").attr("class","selectBtn");

            $("#messengerMain").css("display","none");
            $("#chatroomCont").css("display","none");
            $("#vectorView").css("display","block");
            $('#backBtn').css("display","none");
            $('#outBtn').css("display","none");

          break;
        }

      }

      function changeNewRoom(roomNum){
        selectMenu('talk');
        selectRoom(roomNum);
      }

//방을 변경할때 발생
      function selectRoom(roomNum){
          selectedRoom = true;
          $("#chatroomLists").css("display","none");
          $("#inputChat").css("display","block");
          $('#chat').empty();
          $('#chat').css("display","block");

          $('#backBtn').css("display","block");
          $('#outBtn').css("display","block");

          socket.emit('joinRoom', roomNum, name);
          roomNumber = roomNum;
          num = roomNum;
      }

      function out(){
          selectedRoom = false;
          $('#backBtn').css("display","none");
          $('#outBtn').css("display","none");

          socket.emit('leaveRoom', num, name);
          $("#chatroomLists").css("display","block");
          $('#chat').css("display","none");
          $("#inputChat").css("display","none");
      }

      function outForever(){
            selectedRoom = false;
            $('#backBtn').css("display","none");
            $('#outBtn').css("display","none");

            socket.emit('outRoom', num, name);
            $("#chatroomLists").css("display","block");
            $('#chat').css("display","none");
            $("#inputChat").css("display","none");
      }

        function getContextPath(){
          var hostIndex = location.href.indexOf(location.host) + location.host.length;
          return location.href.substring(hostIndex, location.href.indexOf('/', hostIndex + 1));
        }
    </script>

    <script>


      function tree_menu() {
        socket.emit('loadDeptList');
          $('ul.depth_2 >li > a').click(function(e) {
            console.log("상위메뉴클릭");
            var temp_el = $(this).next('ul');
            var depth_3 = $('.depth_3');

            // 처음에 모두 슬라이드 업 시켜준다.
            depth_3.slideUp(300);
            // 클릭한 순간 모두 on(-)을 제거한다.// +가 나오도록
            depth_3.parent().find('em').removeClass('on');

            if (temp_el.is(':hidden')) {
              temp_el.slideDown(300);
              console.log($('ul.depth_3 >li > a'))
              $(this).find('em').addClass('on').html('하위폴더 열림');
              sideMenu();
            } else {
              temp_el.slideUp(300);
              $(this).find('em').removeClass('on').html('하위폴더 닫힘');
            }
            return false;
          })
          console.log($('ul.depth_2 >li > a'))
   
        }
      tree_menu();

      function sideMenu(){
        $('ul.depth_3 >li a').click(function(e) {
                var temp_el2 = $(this).next('ul');
                var depth_4 = $('.depth_4');
                if (temp_el2.is(':hidden')) {
                  temp_el2.slideDown(300);
                  $(this).find('em').addClass('on').html('하위폴더 열림');
                }
              return false;
          });
      }

      function makeRoom(){

        $('input:checkbox[name="checkEmp"]:checked').each(function (index) {

          var memberData = $(this).val();
          var memberString = memberData.split(",");


        if (index != 0) {
              selectedMember += ',';
              selectedMemberName += ',';
            }
            selectedMember += memberString[0];
            selectedMemberName += (memberString[1] +" "+ memberString[2]);
        });
 
        console.log(selectedMember+" "+selectedMemberName);

        socket.emit('makeRoom', selectedMember, selectedMemberName);

        selectedMember= "" + name +",";
        selectedMemberName= "" + realName;

        $('input:checkbox[name="checkEmp"]:checked').prop('checked', false);
      }
      

    </script>
</body>
</html>