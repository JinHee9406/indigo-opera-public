package com.indigo.opera.member.model.dao;

import org.mybatis.spring.SqlSessionTemplate;

import com.indigo.opera.member.model.exception.LoginFailedException;
import com.indigo.opera.member.model.vo.Member;


public interface MemberDao {
	Member loginCheck(SqlSessionTemplate sqlSession, Member m) throws LoginFailedException;

	Member selectMember(SqlSessionTemplate sqlSession, Member m);

	String selectJobName(SqlSessionTemplate sqlSession, Member m);
	
	int insertMember(SqlSessionTemplate sqlSession, Member m);

	Member checkPass(SqlSessionTemplate sqlSession, Member m);

}
