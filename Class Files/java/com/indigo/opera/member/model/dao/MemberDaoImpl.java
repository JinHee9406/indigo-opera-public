package com.indigo.opera.member.model.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.member.model.exception.LoginFailedException;
import com.indigo.opera.member.model.vo.Member;

@Repository
public class MemberDaoImpl implements MemberDao{

	@Override
	public Member loginCheck(SqlSessionTemplate sqlSession, Member m) throws LoginFailedException {
		Member loginUser = sqlSession.selectOne("Member.loginCheck", m);
		
		if(loginUser == null) {
			throw new LoginFailedException("로그인 실패!");
		}
		
		return loginUser;
	}

	@Override
	public Member selectMember(SqlSessionTemplate sqlSession, Member m) {
		return sqlSession.selectOne("Member.selectLoginUser", m.getEmpNo());
	}

	@Override
	public String selectJobName(SqlSessionTemplate sqlSession, Member m) {
		
		System.out.println("직업검색매개: " + m);
		String job = sqlSession.selectOne("Member.jobCheck", m.getEmpNo());
		System.out.println("직업검색: "+ job);
		return job;
	}

	@Override
	public int insertMember(SqlSessionTemplate sqlSession, Member m) {
		System.out.println("멤버: "+ m);
		
		int result = sqlSession.insert("Member.insertMember", m);
		
		return result;
		
	}

	@Override
	public Member checkPass(SqlSessionTemplate sqlSession, Member m) {

		Member result = sqlSession.selectOne("Member.checkPass", m);
		
		return result;
	}

}
