package com.indigo.opera.member.model.service;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indigo.opera.member.model.dao.MemberDao;
import com.indigo.opera.member.model.exception.LoginFailedException;
import com.indigo.opera.member.model.vo.Member;

@Service
public class MemberServiceImpl implements MemberService{
	@Autowired
	private SqlSessionTemplate sqlSession; //어노테이션 생성= xml, ioc 컨테이너 설정
	
	@Autowired
	private MemberDao md;
	
	@Override
	public Member loginMember(Member m) throws LoginFailedException {
		Member loginUser = null;
		
		System.out.println("loginMember 실행");
		
		loginUser = md.selectMember(sqlSession, m);
		
		return loginUser;
	}

	@Override
	public String jobName(Member m) {
		
		String job = md.selectJobName(sqlSession, m);
		
		return job;
	}

	@Override
	public int insertMember(Member m) {
		
		int result = md.insertMember(sqlSession, m);
		
		return result;
	}

	@Override
	public Member checkPass(Member m) {
		Member result = md.checkPass(sqlSession, m);
		
		return result;
	}


}
