package com.indigo.opera.member.model.service;

import com.indigo.opera.member.model.exception.LoginFailedException;
import com.indigo.opera.member.model.vo.Member;

public interface MemberService {
	Member loginMember(Member m) throws LoginFailedException;

	String jobName(Member m);

	int insertMember(Member m);

	Member checkPass(Member m);
}
