package com.indigo.opera.member.model.vo;

import java.sql.Date;

public class Member implements java.io.Serializable {
	private String empNo;				//사번
	private String empPwd;			//비밀번호
	private String empName;		//성명
	private String empNumber;		//주민번호
	private Date empJoinDate;		//입사일
	private String empAff;			//소속
	private String empDept;		//부서
	private String empJobCode;		//직급
	private String empGender;		//성별
	private String empBankName;	//은행명
	private String empBankNumber;	//계좌번호
	private String empDipositName;	//예금주명
	private String empHomePhone;	//전화번호
	private String empPhone;		//핸드폰번호
	private String empAddress;		//주소
	private String empEmail;		//이메일
	private String empEngName;		//영문성명
	private String empMstatus;		//결혼여부
	private String empRperson;		//등록자	
	private String empNation;		//국적
	private String status;			//재직여부
	private String firstLogin;		//최초로그인
	private String fileCode;		//직원프로필사진파일코드
	private String empJobName;		//직원 직급명
	
	
	public Member() {}

	public Member(String empNo, String empPwd, String empName, String empNumber, Date empJoinDate, String empAff,
			String empDept, String empJobCode, String empGender, String empBankName, String empBankNumber,
			String empDipositName, String empHomePhone, String empPhone, String empAddress, String empEmail,
			String empEngName, String empMstatus, String empRperson, String empNation, String status, String firstLogin,
			String fileCode) {
		super();
		this.empNo = empNo;
		this.empPwd = empPwd;
		this.empName = empName;
		this.empNumber = empNumber;
		this.empJoinDate = empJoinDate;
		this.empAff = empAff;
		this.empDept = empDept;
		this.empJobCode = empJobCode;
		this.empGender = empGender;
		this.empBankName = empBankName;
		this.empBankNumber = empBankNumber;
		this.empDipositName = empDipositName;
		this.empHomePhone = empHomePhone;
		this.empPhone = empPhone;
		this.empAddress = empAddress;
		this.empEmail = empEmail;
		this.empEngName = empEngName;
		this.empMstatus = empMstatus;
		this.empRperson = empRperson;
		this.empNation = empNation;
		this.status = status;
		this.firstLogin = firstLogin;
		this.fileCode = fileCode;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEmpPwd() {
		return empPwd;
	}

	public void setEmpPwd(String empPwd) {
		this.empPwd = empPwd;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpNumber() {
		return empNumber;
	}

	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}

	public Date getEmpJoinDate() {
		return empJoinDate;
	}

	public void setEmpJoinDate(Date empJoinDate) {
		this.empJoinDate = empJoinDate;
	}

	public String getEmpAff() {
		return empAff;
	}

	public void setEmpAff(String empAff) {
		this.empAff = empAff;
	}

	public String getEmpDept() {
		return empDept;
	}

	public void setEmpDept(String empDept) {
		this.empDept = empDept;
	}

	public String getEmpJobCode() {
		return empJobCode;
	}

	public void setEmpJobCode(String empJobCode) {
		this.empJobCode = empJobCode;
	}

	public String getEmpGender() {
		return empGender;
	}

	public void setEmpGender(String empGender) {
		this.empGender = empGender;
	}

	public String getEmpBankName() {
		return empBankName;
	}

	public void setEmpBankName(String empBankName) {
		this.empBankName = empBankName;
	}

	public String getEmpBankNumber() {
		return empBankNumber;
	}

	public void setEmpBankNumber(String empBankNumber) {
		this.empBankNumber = empBankNumber;
	}

	public String getEmpDipositName() {
		return empDipositName;
	}

	public void setEmpDipositName(String empDipositName) {
		this.empDipositName = empDipositName;
	}

	public String getEmpHomePhone() {
		return empHomePhone;
	}

	public void setEmpHomePhone(String empHomePhone) {
		this.empHomePhone = empHomePhone;
	}

	public String getEmpPhone() {
		return empPhone;
	}

	public void setEmpPhone(String empPhone) {
		this.empPhone = empPhone;
	}

	public String getEmpAddress() {
		return empAddress;
	}

	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	public String getEmpEngName() {
		return empEngName;
	}

	public void setEmpEngName(String empEngName) {
		this.empEngName = empEngName;
	}

	public String getEmpMstatus() {
		return empMstatus;
	}

	public void setEmpMstatus(String empMstatus) {
		this.empMstatus = empMstatus;
	}

	public String getEmpRperson() {
		return empRperson;
	}

	public void setEmpRperson(String empRperson) {
		this.empRperson = empRperson;
	}

	public String getEmpNation() {
		return empNation;
	}

	public void setEmpNation(String empNation) {
		this.empNation = empNation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(String firstLogin) {
		this.firstLogin = firstLogin;
	}

	public String getFileCode() {
		return fileCode;
	}

	public void setFileCode(String fileCode) {
		this.fileCode = fileCode;
	}

	@Override
	public String toString() {
		return "Member [empNo=" + empNo + ", empPwd=" + empPwd + ", empName=" + empName + ", empNumber=" + empNumber
				+ ", empJoinDate=" + empJoinDate + ", empAff=" + empAff + ", empDept=" + empDept + ", empJobCode="
				+ empJobCode + ", empGender=" + empGender + ", empBankName=" + empBankName + ", empBankNumber="
				+ empBankNumber + ", empDipositName=" + empDipositName + ", empHomePhone=" + empHomePhone
				+ ", empPhone=" + empPhone + ", empAddress=" + empAddress + ", empEmail=" + empEmail + ", empEngName="
				+ empEngName + ", empMstatus=" + empMstatus + ", empRperson=" + empRperson + ", empNation=" + empNation
				+ ", status=" + status + ", firstLogin=" + firstLogin + ", fileCode=" + fileCode + "]";
	}

	public String getEmpJobName() {
		return empJobName;
	}

	public void setEmpJobName(String empJobName) {
		this.empJobName = empJobName;
	}

}
