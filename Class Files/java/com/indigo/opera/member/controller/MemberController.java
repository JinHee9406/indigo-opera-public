package com.indigo.opera.member.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.indigo.opera.attendance.model.service.AttendService;
import com.indigo.opera.attendance.model.vo.Attendance;
import com.indigo.opera.common.CommonUtils;
import com.indigo.opera.member.model.exception.LoginFailedException;
import com.indigo.opera.member.model.service.MemberService;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.reservation.model.service.ReservationService;

@SessionAttributes("loginUser")
@Controller
public class MemberController {

	@Autowired
	private AttendService as;

	@Autowired //하위 타입에 맞는 빈을 변수에 삽입
	private MemberService ms;

	@Autowired //하위 타입에 맞는 빈을 변수에 삽입
	private ReservationService rs;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	//메신저 페이지 호출 ---------------------------------
	@RequestMapping("messenger.me")
	public String showMessenger(Model model, String loginUser) {
		System.out.println(loginUser);

		Member messengerUser = rs.getUserData(loginUser);

		model.addAttribute("messengerUser", messengerUser);

		System.out.println(messengerUser);

		return "common/messenger";
	}
	//---------------------------------------------
	@RequestMapping("login.me")
	public String loginCheck(Model model, Member m, HttpServletRequest request) {

		//m.setUserPwd(passwordEncoder.encode(m.getUserPwd()));
		//System.out.println("멤버: " + m);
		Member mm = null;
		Member loginUser = null;
		System.out.println("Member m : " + m);
		
		Member checkPass = ms.checkPass(m);
		
		if(checkPass != null) {
			if(request.getSession().getAttribute("mm") != null) {
				mm = (Member)request.getSession().getAttribute("mm");
			}
			try {
				if(m.getEmpNo() != null) {
					loginUser = ms.loginMember(m);
				} else if(m.getEmpNo() == null) {
					loginUser = ms.loginMember(mm);
				}
				int check = 0;
				request.getSession().setAttribute("mm", loginUser);
				String loginUserJobName = ms.jobName(loginUser);

				loginUser.setEmpJobName(loginUserJobName);

				model.addAttribute("loginUser", loginUser);
				ArrayList<Object> alist = as.selectAttendanceList(loginUser.getEmpNo());
				if(!alist.isEmpty()) {
					
					Attendance a = (Attendance)alist.get(alist.size()-1);
					SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
					Date d = new Date();
					String checkDate = f.format(d);
					String myDate = f.format(a.getWorkTime());
					int cint = Integer.parseInt(checkDate);
					int mint = Integer.parseInt(myDate);

					if(cint != mint) {
						check = 1;
						request.getSession().setAttribute("attendanceChecklist", null);
					}
					if(alist.isEmpty()) {
						check = 1;
						request.getSession().setAttribute("attendanceChecklist", null);
					}
					
					
				}
				if(!alist.isEmpty()) {
					request.getSession().setAttribute("attendanceChecklist", alist);
				}
				if(alist.isEmpty()) {
					check = 1;
				}
				request.getSession().setAttribute("attendanceCheck", check);
				System.out.println("check : " + check);
				return "main/mainPage";
			} catch(LoginFailedException e) {
				model.addAttribute("msg", e.getMessage());

				System.out.println("로그인실패");

				return "redirect:index.jsp";
			}
		} else {
			return "login/loginError";
		}

	}

	@RequestMapping(value="insert1.me")
	public String insertMember(Model model, Member m,
			HttpServletRequest request, MultipartFile photo) {

		if(m.getStatus().equals("재직")) {
			m.setStatus("Y");
		} else {
			m.setStatus("N");
		}
		
		if(m.getEmpMstatus().equals("미혼")) {
			m.setEmpMstatus("N");
		} else {
			m.setEmpMstatus("Y");
		}
		
		
		m.setFirstLogin("N");
		m.setFileCode("DED");
		
		String root = request.getSession().getServletContext().getRealPath("resource");

		//String filePath = root + "\\uploadFiles";

		//String originFileName = photo.getOriginalFilename();
		//String ext = originFileName.substring(originFileName.lastIndexOf("."));
		//String changeName = CommonUtils.getRandomString();
		Member mb = (Member) request.getSession().getAttribute("loginUser");
		m.setEmpRperson(mb.getEmpName());
		int result = ms.insertMember(m);
		try {
			//photo.transferTo(new File(filePath + "\\" + changeName + ext));

			m.setEmpPwd(passwordEncoder.encode(m.getEmpPwd()));

			if(m.getEmpGender().equals("1") || m.getEmpGender().equals("3")) {
				m.setEmpGender("M");
			} else {
				m.setEmpGender("F");
			}

		} catch (Exception e) {
			//new File(filePath + "\\" + changeName + ext).delete();
			model.addAttribute("msg", "등록 실패!");
		}
		return "redirect:index.jsp";
	}

	@RequestMapping("duplicationCheck.me")
	public void duplicationCheck(@RequestParam String empNo,
			HttpServletResponse response) {
		Member m = new Member();
		m.setEmpNo(empNo);

		ObjectMapper mapper = new ObjectMapper();

		try {
			response.getWriter().print(mapper.writeValueAsString(m));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@RequestMapping("insertForm.me")
	public String insertForm() {

		return "hrm/signUp";
	}
	@RequestMapping("wait.me")
	public String waitForm() {
		return "hrm/wait";
	}
	
	@RequestMapping("logout.me")
	public String logout(HttpServletRequest request) {
		request.getSession().invalidate();
		
		return "redirect:index.jsp";
	}

}












