package com.indigo.opera.main.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;

import com.indigo.opera.main.model.exception.mainException;
import com.indigo.opera.main.model.vo.MainApp;
import com.indigo.opera.main.model.vo.MainKnow;
import com.indigo.opera.main.model.vo.MainRes;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.notice.model.vo.Board;

public interface MainPageDao {

	int updateUserFile(SqlSessionTemplate sqlSession, Member param) throws mainException;

	ArrayList<MainKnow> loadKnowledge(SqlSessionTemplate sqlSession);

	ArrayList<MainRes> selectResList(SqlSessionTemplate sqlSession, HashMap<String, String> param);

	ArrayList<Board> loadNoticeList(SqlSessionTemplate sqlSession);

	ArrayList<MainApp> selectAppList(SqlSessionTemplate sqlSession, String loginUser);

}
