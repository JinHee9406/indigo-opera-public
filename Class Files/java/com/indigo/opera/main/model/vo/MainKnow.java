package com.indigo.opera.main.model.vo;

public class MainKnow {
	private String knowNo;
	private String knowType;
	private String knowTitle;
	
	public MainKnow() {}

	public MainKnow(String knowNo, String knowType, String knowTitle) {
		super();
		this.knowNo = knowNo;
		this.knowType = knowType;
		this.knowTitle = knowTitle;
	}

	public String getKnowNo() {
		return knowNo;
	}

	public void setKnowNo(String knowNo) {
		this.knowNo = knowNo;
	}

	public String getKnowType() {
		return knowType;
	}

	public void setKnowType(String knowType) {
		this.knowType = knowType;
	}

	public String getKnowTitle() {
		return knowTitle;
	}

	public void setKnowTitle(String knowTitle) {
		this.knowTitle = knowTitle;
	}

	@Override
	public String toString() {
		return "MainKnow [knowNo=" + knowNo + ", knowType=" + knowType + ", knowTitle=" + knowTitle + "]";
	}
	
	
}
