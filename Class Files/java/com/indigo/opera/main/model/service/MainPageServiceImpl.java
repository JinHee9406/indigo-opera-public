package com.indigo.opera.main.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indigo.opera.main.model.dao.MainPageDao;
import com.indigo.opera.main.model.exception.mainException;
import com.indigo.opera.main.model.vo.MainApp;
import com.indigo.opera.main.model.vo.MainKnow;
import com.indigo.opera.main.model.vo.MainRes;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.notice.model.vo.Board;

@Service
public class MainPageServiceImpl implements MainPageService{

	@Autowired
	private MainPageDao mainDao;
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Override
	public int updateUserFile(Member param) throws mainException {

		int result = mainDao.updateUserFile(sqlSession, param);
		
		return result;
	}

	@Override
	public ArrayList<MainKnow> loadKnowledge() {
		
		ArrayList<MainKnow> result = mainDao.loadKnowledge(sqlSession);
		
		return result;
	}

	@Override
	public ArrayList<MainRes> selectResList(HashMap<String, String> param) {
		ArrayList<MainRes> result = mainDao.selectResList(sqlSession, param);
		return result;
	}

	@Override
	public ArrayList<Board> loadNoticeList() {
		ArrayList<Board> result = mainDao.loadNoticeList(sqlSession);
		
		return result;
	}

	@Override
	public ArrayList<MainApp> selectAppList(String loginUser) {
		 ArrayList<MainApp> result = mainDao.selectAppList(sqlSession, loginUser);
		 return result;
	}

}
