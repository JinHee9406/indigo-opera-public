package com.indigo.opera.common;

import com.indigo.opera.knowledge.model.vo.PageInfo;

public class Pagination {
	public static PageInfo getPageInfo(int currentPage, int listCount) {
		PageInfo pi = null;
		
		int limit = 20;
		int buttonCount = 20;
		int maxPage;
		int startPage;
		int endPage;
		
		maxPage = (int)(((double) listCount/buttonCount) + 1);//istCount < 10 = +1, listCount > 10 , listCount > 20 - 1
		
//		if(maxPage < 0) maxPage = 1;
//		else {
//			maxPage -= (int) ((double) listCount/buttonCount);
//		}
		
		
		startPage = (((int) ((double) currentPage/buttonCount)));
		
		if(startPage < 1) startPage = 1;
		
		endPage = startPage + buttonCount -1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		int firstEleNum = listCount - (limit * (currentPage-1));
		int endEleNum = (listCount - limit) - ((currentPage-1) * limit) + 1;
		
		if(endEleNum < 1) endEleNum = 1;
		
		pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage, firstEleNum, endEleNum);
		
		return pi;
	}
}
