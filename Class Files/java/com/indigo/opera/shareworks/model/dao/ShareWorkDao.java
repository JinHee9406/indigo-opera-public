package com.indigo.opera.shareworks.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;

import com.indigo.opera.department.model.vo.Department;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.shareworks.model.vo.ShareMember;
import com.indigo.opera.shareworks.model.vo.Work;
import com.indigo.opera.shareworks.model.vo.WorkNotice;
import com.indigo.opera.shareworks.model.vo.WorkReport;

public interface ShareWorkDao {

	ArrayList<Member> memberList(SqlSessionTemplate sqlSession, String deptCode);

	ArrayList<Member> memberList(SqlSessionTemplate sqlSession);
	
	ArrayList<Department> deptList(SqlSessionTemplate sqlSession);

	int addShareWork(SqlSessionTemplate sqlSession, Work work);

	ArrayList<Work> selectWorkList(SqlSessionTemplate sqlSession, String loginUser);

	ArrayList<Work> selectMyList(SqlSessionTemplate sqlSession, String loginUser);
	
	ArrayList<Work> selectShareList(SqlSessionTemplate sqlSession, String loginUser);

	Work selectWorkOne(SqlSessionTemplate sqlSession, String workNum);

	ArrayList<ShareMember> selectListMember(SqlSessionTemplate sqlSession, String workNum);

	Member loadJoindMember(SqlSessionTemplate sqlSession, String memberNo);

	int addWorkNotice(SqlSessionTemplate sqlSession, WorkNotice workNotice);

	ArrayList<WorkNotice> loadShareNotice(SqlSessionTemplate sqlSession, String workNo);

	WorkNotice noticeDetail(SqlSessionTemplate sqlSession, String noticeNo);

	int removeNotice(SqlSessionTemplate sqlSession, String noticeNum);

	int updateCnt(SqlSessionTemplate sqlSession, WorkNotice notice);

	int updateNotice(SqlSessionTemplate sqlSession, WorkNotice notice);

	int addWorkReport(SqlSessionTemplate sqlSession, WorkReport report);

	ArrayList<WorkReport> workReportList(SqlSessionTemplate sqlSession, HashMap<String, String> param);

	int updateShareWork(SqlSessionTemplate sqlSession, Work work);



}
