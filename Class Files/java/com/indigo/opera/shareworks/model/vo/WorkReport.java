package com.indigo.opera.shareworks.model.vo;

public class WorkReport {
	private String reportNo;
	private String reportDetail;
	private String reportDate;
	private String reportType;
	private String reportFile;
	private String reportFileName;
	private String workNo;
	private String empNo;
	private String empName;
	private String jobName;
	
	public WorkReport() {}

	public WorkReport(String reportNo, String reportDetail, String reportDate, String reportType, String reportFile,
			String workNo, String empNo) {
		super();
		this.reportNo = reportNo;
		this.reportDetail = reportDetail;
		this.reportDate = reportDate;
		this.reportType = reportType;
		this.reportFile = reportFile;
		this.workNo = workNo;
		this.empNo = empNo;
	}

	@Override
	public String toString() {
		return "WorkReport [reportNo=" + reportNo + ", reportDetail=" + reportDetail + ", reportDate=" + reportDate
				+ ", reportType=" + reportType + ", reportFile=" + reportFile + ", workNo=" + workNo + ", empNo="
				+ empNo + "]";
	}

	public String getReportNo() {
		return reportNo;
	}

	public void setReportNo(String reportNo) {
		this.reportNo = reportNo;
	}

	public String getReportDetail() {
		return reportDetail;
	}

	public void setReportDetail(String reportDetail) {
		this.reportDetail = reportDetail;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getReportFile() {
		return reportFile;
	}

	public void setReportFile(String reportFile) {
		this.reportFile = reportFile;
	}

	public String getWorkNo() {
		return workNo;
	}

	public void setWorkNo(String workNo) {
		this.workNo = workNo;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getReportFileName() {
		return reportFileName;
	}

	public void setReportFileName(String reportFileName) {
		this.reportFileName = reportFileName;
	}

	
}
