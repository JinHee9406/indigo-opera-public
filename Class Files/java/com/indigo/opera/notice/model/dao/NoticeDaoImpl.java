package com.indigo.opera.notice.model.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.fileList.model.vo.FileList;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.notice.model.vo.Board;
import com.indigo.opera.notice.model.vo.Notice;
import com.indigo.opera.notice.model.vo.Reply;

@Repository
public class NoticeDaoImpl implements NoticeDao {

   @Override
   public int getListSize(SqlSessionTemplate sqlSession, String boardType) {
      int result = sqlSession.selectOne("Notice.listSize", boardType);

      return result;
   }

   @Override
   public ArrayList<Board> showAllNoticeList(SqlSessionTemplate sqlSession, PageInfo pi) {

      ArrayList<Board> list = null;

      int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

      RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

      list = (ArrayList) sqlSession.selectList("Notice.selectAllList", null, rowBounds);

      if (list == null) {
         // throw new NoticeListException("게시판 조회 실패");
      }

      return list;
   }

   @Override
   public ArrayList<Board> showCommonNoticeList(SqlSessionTemplate sqlSession, PageInfo pi) {
      ArrayList<Board> list = null;

      int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

      RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

      list = (ArrayList) sqlSession.selectList("Notice.selectCommonList", null, rowBounds);

      if (list == null) {
         // throw new NoticeListException("게시판 조회 실패");
      }

      return list;
   }

   @Override
   public ArrayList<Board> showDeptNoticeList(SqlSessionTemplate sqlSession, PageInfo pi, String deptCode) {
      ArrayList<Board> list = null;

      int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

      RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

      list = (ArrayList) sqlSession.selectList("Notice.selectDeptList",deptCode, rowBounds);

      if (list == null) {
         // throw new NoticeListException("게시판 조회 실패");
      }

      return list;
   }

   @Override
   public Board showCommonDetail(SqlSessionTemplate sqlSession, String boardNo) {
      
      
       Board board = sqlSession.selectOne("Notice.selectCommonDetail",boardNo);
      
      if(board == null) {
         
      }
      
      return board;
   }

   @Override
   public int insertCommon(SqlSessionTemplate sqlSession, Board board) {
      
      System.out.println("board in insertCommmn : ::" + board);
      return sqlSession.insert("Notice.insertCommonPage",board);
      
      //asd
   }

   @Override
   public ArrayList<Object> selectFileList(SqlSessionTemplate sqlSession, String fileCode) {
       ArrayList<Object> fileList = new ArrayList<Object>();
         String[] filenameList = fileCode.split(",");
         
         for(int i = 0 ; i < filenameList.length; i ++) {
            FileList f = sqlSession.selectOne("Notice.selectFileOne", filenameList[i]);
            
            fileList.add(f);
         }
         System.out.println("FILELIST DAO : " + fileList);
         
         return fileList;
   }

   @Override
   public int insertReply(SqlSessionTemplate sqlSession, Reply reply, String empNo, String boardNo) {
      reply.setEmpNo(empNo);
      reply.setBoardNo(boardNo);
      return sqlSession.insert("Notice.insertReply",reply);
   }

   @Override
   public int getReplyListSize(SqlSessionTemplate sqlSession, String boardNum) {
      
      int boardNo = Integer.parseInt(boardNum);
      
      return sqlSession.selectOne("Notice.replyListSize", boardNo);
   }

   @Override
   public ArrayList<Reply> showReplyList(SqlSessionTemplate sqlSession, String boardNo, PageInfo pi) {
      
      ArrayList<Reply> list = null;

      int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

      RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

      list = (ArrayList) sqlSession.selectList("Notice.selectReplyList", boardNo, rowBounds);

      if (list == null) {
         // throw new NoticeListException("게시판 조회 실패");
      }

      return list;
   }

   @Override
   public int deleteReply(SqlSessionTemplate sqlSession, String replyNo) {
      return sqlSession.update("Notice.deleteReply",replyNo);
   }

   @Override
   public int insertDept(SqlSessionTemplate sqlSession, Board board) {
      
      return sqlSession.insert("Notice.insertDeptPage",board);
   }

   @Override
   public int increaseReplyCnt(SqlSessionTemplate sqlSession, String boardNo) {
      
      return sqlSession.update("Notice.increaseReplyCnt",boardNo);
   }

   @Override
   public int decreaseReplyCnt(SqlSessionTemplate sqlSession, String boardNo) {
      
      return sqlSession.update("Notice.decreaseReplyCnt",boardNo);
   }

   @Override
   public int increaseBoardCnt(SqlSessionTemplate sqlSession, String boardNo) {
      return sqlSession.update("Notice.increaseBoardCnt",boardNo);
   }

   @Override
   public int getDeptListSize(SqlSessionTemplate sqlSession, String boardType, String deptCode) {
      
      Board board = new Board();
      board.setDeptCode(deptCode);
      board.setBoardType(boardType);

      int result = sqlSession.selectOne("Notice.deptListSize", board);
      
      return result;
   }
}