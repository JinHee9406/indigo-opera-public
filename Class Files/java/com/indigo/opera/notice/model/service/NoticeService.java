package com.indigo.opera.notice.model.service;

import java.util.ArrayList;

import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.notice.model.vo.Board;
import com.indigo.opera.notice.model.vo.Notice;
import com.indigo.opera.notice.model.vo.Reply;

public interface NoticeService {

   int getListSize(String boardType);

   ArrayList<Board> showAllNoticeList(PageInfo pi);

   ArrayList<Board> showCommonNoticeList(PageInfo pi);

   ArrayList<Board> showDeptNoticeList(PageInfo pi, String loginUser);

   Board showCommonDetail(String boardNo);

   int insertCommon(Board boar);

   ArrayList<Object> selectFileList(String fileCode);

   int insertReply(Reply reply, String empNo, String boardNo);

   int getReplyListSize(String boardNo);

   ArrayList<Reply> showReply(String boardNo, PageInfo pi);

   int deleteReply(String replyNo);

   int insertDept(Board board);

   int increaseReplyCnt(String boardNo);

   int decreaseReplyCnt(String boardNo);

   int increaseBoardCnt(String boardNo);

   int getDeptListSize(String string, String loginUser);
}