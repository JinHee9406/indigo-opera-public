package com.indigo.opera.notice.model.vo;

import java.sql.Date;

public class Board  implements java.io.Serializable{
	
//	BOARD_NO
//	BOARD_TITLE
//	BOARD_DATE
//	BOARD_CNT
//	BOARD_TYPE
//	BOARD_STATUS
//	DEPT_CODE
//	EMP_NO
//	FILE_CODE
//	ALERT_NUM
	
	private String boardNo;
	private String boardTitle;
	private Date boardDate;
	private int boardCnt;
	private String boardType;
	private String boardStatus;
	private String deptCode;
	private String empNo;
	private String fileCode;
	private String alertNum;
	private String empName;
	private int replyCnt;
	private String boardContent;
	
	public Board() {
		// TODO Auto-generated constructor stub
	}

	public Board(String boardNo, String boardTitle, Date boardDate, int boardCnt, String boardType, String boardStatus,
			String deptCode, String empNo, String fileCode, String alertNum, String empName, int replyCnt,
			String boardContent) {
		super();
		this.boardNo = boardNo;
		this.boardTitle = boardTitle;
		this.boardDate = boardDate;
		this.boardCnt = boardCnt;
		this.boardType = boardType;
		this.boardStatus = boardStatus;
		this.deptCode = deptCode;
		this.empNo = empNo;
		this.fileCode = fileCode;
		this.alertNum = alertNum;
		this.empName = empName;
		this.replyCnt = replyCnt;
		this.boardContent = boardContent;
	}

	public String getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(String boardNo) {
		this.boardNo = boardNo;
	}

	public String getBoardTitle() {
		return boardTitle;
	}

	public void setBoardTitle(String boardTitle) {
		this.boardTitle = boardTitle;
	}

	public Date getBoardDate() {
		return boardDate;
	}

	public void setBoardDate(Date boardDate) {
		this.boardDate = boardDate;
	}

	public int getBoardCnt() {
		return boardCnt;
	}

	public void setBoardCnt(int boardCnt) {
		this.boardCnt = boardCnt;
	}

	public String getBoardType() {
		return boardType;
	}

	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}

	public String getBoardStatus() {
		return boardStatus;
	}

	public void setBoardStatus(String boardStatus) {
		this.boardStatus = boardStatus;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getFileCode() {
		return fileCode;
	}

	public void setFileCode(String fileCode) {
		this.fileCode = fileCode;
	}

	public String getAlertNum() {
		return alertNum;
	}

	public void setAlertNum(String alertNum) {
		this.alertNum = alertNum;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getReplyCnt() {
		return replyCnt;
	}

	public void setReplyCnt(int replyCnt) {
		this.replyCnt = replyCnt;
	}

	public String getBoardContent() {
		return boardContent;
	}

	public void setBoardContent(String boardContent) {
		this.boardContent = boardContent;
	}

	@Override
	public String toString() {
		return "Board [boardNo=" + boardNo + ", boardTitle=" + boardTitle + ", boardDate=" + boardDate + ", boardCnt="
				+ boardCnt + ", boardType=" + boardType + ", boardStatus=" + boardStatus + ", deptCode=" + deptCode
				+ ", empNo=" + empNo + ", fileCode=" + fileCode + ", alertNum=" + alertNum + ", empName=" + empName
				+ ", replyCnt=" + replyCnt + ", boardContent=" + boardContent + "]";
	}

	

	
	
	

	
	
	

}
