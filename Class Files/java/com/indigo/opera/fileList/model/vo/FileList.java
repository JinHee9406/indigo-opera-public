package com.indigo.opera.fileList.model.vo;

public class FileList implements java.io.Serializable{

	private String fileCode;
	private String originName;
	private String changeName;
	private String filePath;
	private String fileSize;
	
	public FileList() {}

	public FileList(String fileCode, String originName, String changeName, String filePath, String fileSize) {
		super();
		this.fileCode = fileCode;
		this.originName = originName;
		this.changeName = changeName;
		this.filePath = filePath;
		this.fileSize = fileSize;
	}

	public String getFileCode() {
		return fileCode;
	}

	public void setFileCode(String fileCode) {
		this.fileCode = fileCode;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	@Override
	public String toString() {
		return "FileList [fileCode=" + fileCode + ", originName=" + originName + ", changeName=" + changeName
				+ ", filePath=" + filePath + ", fileSize=" + fileSize + "]";
	}
	
	
	
}
