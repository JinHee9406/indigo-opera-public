package com.indigo.opera.fileList.model.service;

import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indigo.opera.fileList.model.dao.FileListDao;
import com.indigo.opera.fileList.model.vo.FileList;

@Service
public class FileListServiceImpl implements FileListService{
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private FileListDao fileDao;
	
	@Override
	public int addFile(FileList imgFile) {
		
		int result = fileDao.addFile(sqlSession, imgFile);
		
		return result;
	}

	@Override
	public FileList roomLoad(String roomNumber) {
		
		FileList result = fileDao.roomLoad(sqlSession, roomNumber);
		
		return result;
	}

	@Override
	public FileList userLoad(String userEmpNo) {
		
		FileList result = fileDao.userLoad(sqlSession, userEmpNo);
		
		return result;
	}

	@Override
	public FileList selectOneFile(String fileNo) {
		
		FileList result = fileDao.selectOneFile(sqlSession, fileNo);
		
		return result;
	}

}
