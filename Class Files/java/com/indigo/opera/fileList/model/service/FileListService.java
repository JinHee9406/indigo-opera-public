package com.indigo.opera.fileList.model.service;

import java.util.HashMap;

import com.indigo.opera.fileList.model.vo.FileList;

public interface FileListService {

	int addFile(FileList imgFile);

	FileList roomLoad(String roomNumber);

	FileList userLoad(String userEmpNo);

	FileList selectOneFile(String fileNo);

}
