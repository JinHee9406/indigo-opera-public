package com.indigo.opera.attendance.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.indigo.opera.attendance.model.service.AttendService;
import com.indigo.opera.attendance.model.vo.Attendance;
import com.indigo.opera.attendance.model.vo.DeptAttend;
import com.indigo.opera.member.controller.MemberController;
import com.indigo.opera.member.model.vo.Member;

@Controller
public class AttendanceController {
	
	@Autowired
	MemberController mc;
	
	@Autowired
	private AttendService as;
	
	@RequestMapping("attendMain.att")
	public String showAttendMainPage(Model model,String date, String empNo) {
		System.out.println(date);
		
		String day = date.substring(0,6);
		
		System.out.println(day);
		if(date == null) {}
		Attendance att = new Attendance();
		
		att.setEmpNo(empNo);
		att.setEmpName(day);
		
		ArrayList<Attendance> list = as.showAttendList(att);
		
		System.out.println(list);
		
		model.addAttribute("attendance",list);
		model.addAttribute("month",day);
		model.addAttribute("aside","1");
		
		return "attendance/attendanceRecord";
	}

	@RequestMapping("attendWorkingHours.att")
	public String attendHoursPage() {
		return "attendance/affiliatedWorkingHours";
	}
	
	@RequestMapping("attendStatistics.att")
	public String attendStatisticsPage(Model model,String date,String deptCode) {
		System.out.println(date + " " + deptCode);
		
		ArrayList<DeptAttend> list = new ArrayList<DeptAttend>();
		list = as.selectDeptAttend(date,deptCode);
		
		System.out.println("sta list : "+list);
		
		model.addAttribute("deptAtt",list);
		model.addAttribute("month",date);
		model.addAttribute("aside","2");
		
		return "attendance/affiliatedAttendanceStatistics";
	}
	
	@RequestMapping("vacationMain.att")
	public String vacationStatisticsPage() {
		return "attendance/vacationUsageStatus";
	}
	
	@RequestMapping("vacationUsage.att")
	public String vacationUsagePage() {
		return "attendance/affiliatedVacationStatistics";
	}
	@ResponseBody
	@RequestMapping("insertAttend.att")
	public void insertAttend(String empNo, HttpServletRequest request) {
		ArrayList<Object> alist = as.selectAttendanceList(empNo);
		int check = 0;
		if(alist.isEmpty()) {
			int result = as.insertAttend(empNo);
			System.out.println(" result : " + result);
			alist = as.selectAttendanceList(empNo);
			request.getSession().setAttribute("attendanceChecklist", alist);
		}
		if(!alist.isEmpty()) {
			Attendance a = (Attendance)alist.get(alist.size() - 1);
			SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
			Date d = new Date();
			String checkDate = f.format(d);
			String myDate = f.format(a.getWorkTime());
			int cint = Integer.parseInt(checkDate);
			int mint = Integer.parseInt(myDate);
			
			if(cint != mint) {
				check = 1;
			}
			
			if(check == 1 || alist.isEmpty()) {
				int result = as.insertAttend(empNo);
				System.out.println(" result : " + result);
				alist = as.selectAttendanceList(empNo);
			}
		}
		if(!alist.isEmpty()) {
			request.getSession().setAttribute("attendanceChecklist", alist);
		}
		request.getSession().setAttribute("attendanceCheck", check);
		
	}
	
	@RequestMapping("updateAttend.att")
	public String updateAttend(Model m, String empNo, HttpServletRequest request) {
		Member mm = (Member)request.getSession().getAttribute("loginUser");
		if(empNo != null) {
			int result = as.updateAttend(empNo);
		}
		if(empNo == null) {
			int result = as.updateAttend(mm.getEmpNo());
		}
		request.getSession().setAttribute("attendanceCheck", 1);
		return mc.loginCheck(m, (Member)request.getSession().getAttribute("loginUser"), request);
	}
}
