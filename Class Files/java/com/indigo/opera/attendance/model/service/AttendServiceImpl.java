package com.indigo.opera.attendance.model.service;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indigo.opera.attendance.model.dao.AttendDao;
import com.indigo.opera.attendance.model.vo.Attendance;
import com.indigo.opera.attendance.model.vo.DeptAttend;

@Service
public class AttendServiceImpl implements AttendService{
	@Autowired
	private AttendDao ad;
	
	@Autowired
	private SqlSessionTemplate sqlSession;

	@Override
	public int insertAttend(String empNo) {
		return ad.insertAttend(sqlSession,empNo);
	}

	@Override
	public int updateAttend(String empNo) {
		return ad.updateAtten(sqlSession,empNo);
	}

	@Override
	public ArrayList<Attendance> showAttendList(Attendance att) {
		
		return ad.showAttendList(sqlSession,att);
	}

	@Override
	public ArrayList<DeptAttend> selectDeptAttend(String date, String deptCode) {
		
		ArrayList<DeptAttend> list = ad.showDeptEmp(sqlSession,deptCode);

		for(DeptAttend d : list) {
			int attCount = ad.showAttCount(sqlSession,date,d);
			int lateCount = ad.showLateCount(sqlSession,date,d);
			int absCount = ad.showAbsCount(sqlSession,date,d);
			d.setAttCnt(attCount);
			d.setLateCnt(lateCount);
			d.setAbsCnt(absCount);
		}
		
		System.out.println(list);
		
		return list;
	}

	@Override
	public ArrayList<Object> selectAttendanceList(String empNo) {
		ArrayList<Object> alist = ad.selectAttendanceList(sqlSession, empNo);
		return alist;
	}

}
