package com.indigo.opera.attendance.model.service;

import java.util.ArrayList;

import com.indigo.opera.attendance.model.vo.Attendance;
import com.indigo.opera.attendance.model.vo.DeptAttend;

public interface AttendService {

	int insertAttend(String empNo);

	int updateAttend(String empNo);

	ArrayList<Attendance> showAttendList(Attendance att);

	ArrayList<DeptAttend> selectDeptAttend(String date, String deptCode);

	ArrayList<Object> selectAttendanceList(String empNo);

}
