package com.indigo.opera.attendance.model.vo;

public class DeptAttend {
	private String empNo;
	private String empName;
	private String deptName;
	private int attCnt;
	private int lateCnt;
	private int absCnt;
	private String date;
	
	public DeptAttend() {
		// TODO Auto-generated constructor stub
	}

	public DeptAttend(String empNo, String empName, String deptName, int attCnt, int lateCnt, int absCnt, String date) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.deptName = deptName;
		this.attCnt = attCnt;
		this.lateCnt = lateCnt;
		this.absCnt = absCnt;
		this.date = date;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public int getAttCnt() {
		return attCnt;
	}

	public void setAttCnt(int attCnt) {
		this.attCnt = attCnt;
	}

	public int getLateCnt() {
		return lateCnt;
	}

	public void setLateCnt(int lateCnt) {
		this.lateCnt = lateCnt;
	}

	public int getAbsCnt() {
		return absCnt;
	}

	public void setAbsCnt(int absCnt) {
		this.absCnt = absCnt;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "DeptAttend [empNo=" + empNo + ", empName=" + empName + ", deptName=" + deptName + ", attCnt=" + attCnt
				+ ", lateCnt=" + lateCnt + ", absCnt=" + absCnt + ", date=" + date + "]";
	}

	
	

}
