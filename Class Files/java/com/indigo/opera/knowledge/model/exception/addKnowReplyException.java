package com.indigo.opera.knowledge.model.exception;

public class addKnowReplyException extends Exception{
	public addKnowReplyException(String msg) {
		super(msg);
	}
}
