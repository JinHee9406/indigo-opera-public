package com.indigo.opera.knowledge.model.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.knowledge.model.exception.addKnowReplyException;
import com.indigo.opera.knowledge.model.exception.addKnowledgeException;
import com.indigo.opera.knowledge.model.exception.fixKnowledgeException;
import com.indigo.opera.knowledge.model.exception.knowledgeListException;
import com.indigo.opera.knowledge.model.exception.knowledgeOneException;
import com.indigo.opera.knowledge.model.vo.Knowledge;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.knowledge.model.vo.Reply;
import com.indigo.opera.knowledge.model.vo.SearchCondition;
@Repository
public class KnowledgeDaoImpl implements KnowledgeDao{
	
	@Override
	public int getListSize(SqlSessionTemplate sqlSession) {
		
		int result = sqlSession.selectOne("Knowledge.listSize");
		
		return result;
	}
	
	@Override
	public int getListSize(SqlSessionTemplate sqlSession, String knowType) {
		
		int result = sqlSession.selectOne("Knowledge.listSizeType", knowType);
		
		return result;
	}

	@Override
	public int getListSize(SqlSessionTemplate sqlSession, SearchCondition searchCondition) {
		
		int result = sqlSession.selectOne("Knowledge.searchListSize", searchCondition);
		
		return result;
	}

	
	@Override
	public ArrayList<Knowledge> showKnowledgeList(SqlSessionTemplate sqlSession, PageInfo pi) throws knowledgeListException {
		 
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		
		RowBounds rowBounds = new RowBounds(offset,pi.getLimit());
		
		ArrayList<Knowledge> knowledgeList = (ArrayList) sqlSession.selectList("Knowledge.selectList", null, rowBounds);
		
		if(knowledgeList == null) {
			throw new knowledgeListException("리스트조회 실패");
		}
		
		return knowledgeList;
	}
	
	@Override
	public ArrayList<Knowledge> typeKnowledgeList(SqlSessionTemplate sqlSession, PageInfo pi, String knowType) throws knowledgeListException {
		
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		
		RowBounds rowBounds = new RowBounds(offset,pi.getLimit());
		
		ArrayList<Knowledge> knowledgeList = (ArrayList) sqlSession.selectList("Knowledge.selectListType", knowType, rowBounds);
		
		if(knowledgeList == null) {
			throw new knowledgeListException("리스트조회 실패");
		}
		
		return knowledgeList;
	}

	@Override
	public ArrayList<Knowledge> searchKnowledgeList(SqlSessionTemplate sqlSession, PageInfo pi, SearchCondition sc) throws knowledgeListException {
			
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		
		RowBounds rowBounds = new RowBounds(offset,pi.getLimit());
		
		ArrayList<Knowledge> knowledgeList = (ArrayList) sqlSession.selectList("Knowledge.selectListSearch", sc, rowBounds);
		
		if(knowledgeList == null) {
			throw new knowledgeListException("리스트조회 실패");
		}
		
		return knowledgeList;
	}

	
	@Override
	public int addKnowledge(SqlSessionTemplate sqlSession, Knowledge know) throws addKnowledgeException {
		
		int result = sqlSession.insert("Knowledge.insertKnowledge", know);
		
		if(result == 0) {
			throw new addKnowledgeException("글작성 실패");
		}
		return result;
	}

	@Override
	public Knowledge selectOneKnowledge(SqlSessionTemplate sqlSession, String knowledgeNum) throws knowledgeOneException {

		Knowledge result = (Knowledge) sqlSession.selectOne("Knowledge.selectOneKnowledge", knowledgeNum);
		
		if(result == null) {
			
			throw new knowledgeOneException("글조회 실패");
		}
		
		return result;
	}

	@Override
	public int fixKnowledge(SqlSessionTemplate sqlSession, Knowledge know) throws fixKnowledgeException {
		
		int result = sqlSession.update("Knowledge.fixKnowledge", know);
		
		if(result == 0) {
			throw new fixKnowledgeException("글 수정 실패");
		}
		return result;
	}

	@Override
	public ArrayList<Reply> selectReplyListOne(SqlSessionTemplate sqlSession, String knowledgeNum) {
		
		ArrayList<Reply> list = (ArrayList) sqlSession.selectList("Knowledge.selectReplyList", knowledgeNum);
		
		return list;
	}

	@Override
	public int addKnowledgeReply(SqlSessionTemplate sqlSession, Reply reply) throws addKnowReplyException {

		int result = sqlSession.insert("Knowledge.addReply", reply);
		
		if(result == 0) {
			throw new addKnowReplyException("댓글작성 실패");
		}
		
		return result;
	}

	@Override
	public int updateViewCount(SqlSessionTemplate sqlSession, Knowledge knowledgeDetail) {
		
		int result = sqlSession.update("Knowledge.viewCountUp", knowledgeDetail);
		
		return result;
	}

	@Override
	public int removeKnowledge(SqlSessionTemplate sqlSession, String knowNum) {
	
		int result = sqlSession.delete("Knowledge.removeKnow" , knowNum);
		
		return result;
	}






}
