package com.indigo.opera.knowledge.model.vo;

import java.sql.Date;

public class Reply {
	private String replyNum;
	private Date replyDate;
	private String knowledgeNum;
	private String userNum;
	private String userName;
	private String replyContent;
	
	public Reply() {}

	public Reply(String replyNum, Date replyDate, String knowledgeNum, String userNum, String userName,
			String replyContent) {
		super();
		this.replyNum = replyNum;
		this.replyDate = replyDate;
		this.knowledgeNum = knowledgeNum;
		this.userNum = userNum;
		this.userName = userName;
		this.replyContent = replyContent;
	}

	public String getReplyNum() {
		return replyNum;
	}

	public void setReplyNum(String replyNum) {
		this.replyNum = replyNum;
	}

	public Date getReplyDate() {
		return replyDate;
	}

	public void setReplyDate(Date replyDate) {
		this.replyDate = replyDate;
	}

	public String getKnowledgeNum() {
		return knowledgeNum;
	}

	public void setKnowledgeNum(String knowledgeNum) {
		this.knowledgeNum = knowledgeNum;
	}

	public String getUserNum() {
		return userNum;
	}

	public void setUserNum(String userNum) {
		this.userNum = userNum;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	@Override
	public String toString() {
		return "Reply [replyNum=" + replyNum + ", replyDate=" + replyDate + ", knowledgeNum=" + knowledgeNum
				+ ", userNum=" + userNum + ", userName=" + userName + ", replyContent=" + replyContent + "]";
	}

	
}

