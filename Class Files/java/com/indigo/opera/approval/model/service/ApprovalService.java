package com.indigo.opera.approval.model.service;

import java.util.ArrayList;

import com.indigo.opera.approval.model.vo.CashDisbursement;
import com.indigo.opera.approval.model.vo.CheckClient;
import com.indigo.opera.approval.model.vo.CheckDeptPi;
import com.indigo.opera.approval.model.vo.Client;
import com.indigo.opera.approval.model.vo.DraftList;
import com.indigo.opera.approval.model.vo.PayLine;
import com.indigo.opera.approval.model.vo.PayLineList;
import com.indigo.opera.approval.model.vo.PayLog;
import com.indigo.opera.approval.model.vo.PayOpinion;
import com.indigo.opera.approval.model.vo.ProVision;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.member.model.vo.Member;

public interface ApprovalService {

	int insertCashDis(CashDisbursement cdm);

	int insertPayLog(ArrayList<PayLog> paylogList);

	ArrayList<Object> selectApprovalList(Member m, String status, int pageNum);

	CashDisbursement selectApprovalOne(String oneDocu);

	ArrayList<Object> selectApprovalOnePayLogList(String oneDocu);

	int updatePayLogOne(PayLog p);

	int selectCountApprovalList(Member member, String status);

	int insertOpinion(PayOpinion p);

	ArrayList<Object> selectPayOpinionList(String oneDocu);

	int deletePayOpinion(PayOpinion p);

	int updateApproval(CashDisbursement cdm);

	DraftList selectDraft(String docu);

	ArrayList<Object> selectDeptList();

	ArrayList<Object> selectEmpList();

	int selectCountEmpList(CheckDeptPi cp);

	ArrayList<Object> selectEmpList(PageInfo pi, CheckDeptPi cp);

	int insertPayLine(PayLine p);

	int insertPayLineList(ArrayList<PayLineList> plList);

	ArrayList<Object> selectPersonalApprovalList(String empNo);

	ArrayList<Object> selectPersonalApprovalOne(String plNo);

	int insertProVision(ProVision pv);

	ArrayList<Object> selectProvisionList(ProVision pv);

	int insertClient(Client cl);

	ArrayList<Object> selectClientList(CheckClient ck, PageInfo pi);

	Client selectClientOne(String cliNo);

	ArrayList<Object> selectFileList(String filecode);

	String selectDeptName(String empDept);

	int selectCountClientList(CheckClient ck);

	
}
