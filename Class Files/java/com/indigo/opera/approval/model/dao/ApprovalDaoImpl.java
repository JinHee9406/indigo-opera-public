package com.indigo.opera.approval.model.dao;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.approval.model.vo.CashDisbursement;
import com.indigo.opera.approval.model.vo.CheckClient;
import com.indigo.opera.approval.model.vo.CheckDeptPi;
import com.indigo.opera.approval.model.vo.Client;
import com.indigo.opera.approval.model.vo.DraftList;
import com.indigo.opera.approval.model.vo.PayLine;
import com.indigo.opera.approval.model.vo.PayLineList;
import com.indigo.opera.approval.model.vo.PayLog;
import com.indigo.opera.approval.model.vo.PayOpinion;
import com.indigo.opera.approval.model.vo.ProVision;
import com.indigo.opera.common.Pagination;
import com.indigo.opera.fileList.model.vo.FileList;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.member.model.vo.Member;

@Repository
public class ApprovalDaoImpl implements ApprovalDao{

	@Override
	public int insertCashDis(SqlSessionTemplate sqlSession, CashDisbursement cdm) {
		
		int insertdraftlistresult = sqlSession.insert("Approval.insertDraftList",cdm.getEmpNo());
		int insertCashDisresult = sqlSession.insert("Approval.insertCashDisbursement", cdm);
		
		return insertCashDisresult;
	}

	@Override
	public int insertPayLog(SqlSessionTemplate sqlSession, ArrayList<PayLog> paylogList) {
		int result = 0;
		for(int i = 0 ; i < paylogList.size(); i++) {
			if(i == 0) {
				paylogList.get(i).setLostatus("승인");
				paylogList.get(i).setLoDate(java.sql.Timestamp.valueOf(LocalDateTime.now()));
			} else {
				paylogList.get(i).setLostatus("미승인");
			}
			int insertPayLogResult = sqlSession.insert("Approval.insertPayLog",paylogList.get(i));
		}
		
		
		return result;
	}

	@Override
	public ArrayList<Object> selectApprovalList(SqlSessionTemplate sqlSession, Member m, String status, int pageNum) {
		m.setEmpAddress(status);
		ArrayList<Object> fakedrNoList = (ArrayList<Object>) sqlSession.selectList("Approval.selectDrNoList", m);
		PageInfo pi = Pagination.getPageInfo(pageNum, fakedrNoList.size());
		
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset,pi.getLimit());
		System.out.println("m dept : " + m.getEmpDept());
		ArrayList<Object> drNoList = (ArrayList<Object>) sqlSession.selectList("Approval.selectDrNoList", m, rowBounds);
		System.out.println("DRNO LIST : " + drNoList);
		
		ArrayList<Object> approvalList = null;
		if(drNoList.size() != 0) {
			approvalList = (ArrayList<Object>)sqlSession.selectList("Approval.selectApprovalList", drNoList); 
		}
		
		return approvalList;
	}

	@Override
	public CashDisbursement selectApprovalOne(String oneDocu, SqlSessionTemplate sqlSession) {
		CashDisbursement cdm = (CashDisbursement) sqlSession.selectOne("Approval.selectApprovalOne", oneDocu);
		
		return cdm;
	}

	@Override
	public ArrayList<Object> selectApprovalOnePayLogList(SqlSessionTemplate sqlSession, String oneDocu) {
		ArrayList<Object> plist = (ArrayList<Object>) sqlSession.selectList("Approval.selectApprovalOnePayLog", oneDocu);
		return plist;
	}

	@Override
	public int updatePayLogOne(SqlSessionTemplate sqlSession, PayLog p) {
		int result = sqlSession.update("Approval.updatePayLogOne", p);
		
		ArrayList<Object> plist = (ArrayList<Object>) sqlSession.selectList("Approval.selectApprovalOnePayLog", p.getDrNo());
		PayLog rp = (PayLog)plist.get(plist.size() - 1);
		if(p.getEmpNo().equals(rp.getEmpNo()) && p.getDrNo().equals(rp.getDrNo())) {
			if(p.getLostatus().equals("승인")) {
				rp.setLostatus("승인완료");
			} else if(p.getLostatus().equals("반려")){
				rp.setLostatus("승인반려");
			} else if(p.getLostatus().equals("선결")) {
				rp.setLostatus("선결완료");
			}
			int updateCashresult = sqlSession.update("Approval.updateCashDisbursement", rp);	
		}
		int change = 0;
		PayLog pp = new PayLog();
		for(int i = 0; i < plist.size() - 1; i++) {
			pp = (PayLog)plist.get(i);
			if(!pp.getLostatus().equals("승인")) {
				change += 1;
			}
		}
		
		if(change == 0 && rp.getLostatus().equals("선결")) {
			rp.setLostatus("승인완료");
			int updateCashresult = sqlSession.update("Approval.updateCashDisbursement", rp);
		}
		return result;
	}

	@Override
	public int selectCountApprovalList(SqlSessionTemplate sqlSession, Member m, String status) {
		m.setEmpAddress(status);
		System.out.println(" M : " + m );
		int result = sqlSession.selectOne("Approval.selectCountApprovalList", m);
		return result;
	}

	@Override
	public int insertOpinion(SqlSessionTemplate sqlSession, PayOpinion p) {
		int result = sqlSession.insert("Approval.insertPayOpinion", p);
		return result;
	}

	@Override
	public ArrayList<Object> selectPayOpinionList(SqlSessionTemplate sqlSession, String oneDocu) {
		ArrayList<Object> oplist = (ArrayList<Object>)sqlSession.selectList("Approval.selectPayOpinionList", oneDocu);
		return oplist;
	}

	@Override
	public int deletePayOpinion(SqlSessionTemplate sqlSession, PayOpinion p) {
		int result = sqlSession.delete("Approval.deletePayOpinion", p.getOpNo());
		return result;
	}

	@Override
	public int updateApproval(SqlSessionTemplate sqlSession, CashDisbursement cdm) {
		int result = sqlSession.update("Approval.updateApproval", cdm);
		System.out.println("result DAO : " + result);
		return result;
	}

	@Override
	public DraftList selectDraft(SqlSessionTemplate sqlSession, String docu) {
		DraftList dr = sqlSession.selectOne("Approval.selectDraft", docu);
		System.out.println("dr : " + dr);
		return dr;
	}

	@Override
	public ArrayList<Object> selectDeptList(SqlSessionTemplate sqlSession) {
		ArrayList<Object> deptList = (ArrayList<Object>)sqlSession.selectList("Approval.selectDeptList");
		return deptList;
	}

	@Override
	public ArrayList<Object> selectEmpList(SqlSessionTemplate sqlSession) {
		ArrayList<Object> empList = (ArrayList<Object>) sqlSession.selectList("Approval.selectEmpList");
		return empList;
	}

	@Override
	public int selectCountEmpList(SqlSessionTemplate sqlSession, CheckDeptPi cp) {
		int result = sqlSession.selectOne("Approval.selectCountEmpList", cp);
		return result;
	}
                         
	@Override
	public ArrayList<Object> selectEmpList(SqlSessionTemplate sqlSession, PageInfo pi, CheckDeptPi cp) {
		
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset,pi.getLimit());
		
		ArrayList<Object> empList = (ArrayList<Object>) sqlSession.selectList("Approval.selectEmpList2", cp, rowBounds);
		
		System.out.println("empList : " + empList);
		
		return empList;
	}

	@Override
	public int insertPayLine(SqlSessionTemplate sqlSession, PayLine p) {
		int result = sqlSession.insert("Approval.insertPayLine", p);
		
		System.out.println("RESULT DAO : "+ result);
		return result;
	}

	@Override
	public int insertPayLineList(SqlSessionTemplate sqlSession, ArrayList<PayLineList> plList) {
		int result = 0;
		for(int i = 0; i < plList.size(); i ++) {
			int result2 = sqlSession.insert("Approval.insertPayLineList", plList.get(i));
			result = 1;
		}
		return result;
	}

	@Override
	public ArrayList<Object> selectPersonalApprovalList(SqlSessionTemplate sqlSession, String empNo) {
		ArrayList<Object> paList = (ArrayList<Object>)sqlSession.selectList("Approval.selectPersonalApprovalList", empNo);
		return paList;
	}

	@Override
	public ArrayList<Object> selectPersonalApprovalOne(SqlSessionTemplate sqlSession, String plNo) {
		ArrayList<Object> pllList = (ArrayList<Object>) sqlSession.selectList("Approval.selectPersonalApprovalOne", plNo);
		return pllList;
	}

	@Override
	public int insertProVision(SqlSessionTemplate sqlSession, ProVision pv) {
		int result = sqlSession.insert("Approval.insertProVision", pv);
		return result;
	}

	@Override
	public ArrayList<Object> selectProvisionList(SqlSessionTemplate sqlSession, ProVision pv) {
		ArrayList<Object> pvlist = (ArrayList<Object>)sqlSession.selectList("Approval.selectProvisionList", pv);
		return pvlist;
	}

	@Override
	public int insertClient(SqlSessionTemplate sqlSession, Client cl) {
		int result = sqlSession.insert("Approval.insertClient", cl);
		return result;
	}

	@Override
	public ArrayList<Object> selectClientList(SqlSessionTemplate sqlSession, CheckClient ck, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset,pi.getLimit());
		
		ArrayList<Object> clList = (ArrayList<Object>)sqlSession.selectList("Approval.selectClientList", ck, rowBounds);
		return clList;
	}

	@Override
	public Client selectClientOne(SqlSessionTemplate sqlSession, String cliNo) {
		Client cl = (Client) sqlSession.selectOne("Approval.selectClientOne", cliNo);
		return cl;
	}

	@Override
	public ArrayList<Object> selectFileList(SqlSessionTemplate sqlSession, String filecode) {
		ArrayList<Object> fileList = new ArrayList<Object>();
		String[] filenameList = filecode.split(",");
		
		for(int i = 0 ; i < filenameList.length; i ++) {
			FileList f = sqlSession.selectOne("Approval.selectFileOne", filenameList[i]);
			
			fileList.add(f);
		}
		System.out.println("FILELIST DAO : " + fileList);
		return fileList;
	}

	@Override
	public String selectDeptName(SqlSessionTemplate sqlSession, String empDept) {
		String empDeptName = sqlSession.selectOne("Approval.selectDeptName", empDept);
		return empDeptName;
	}

	@Override
	public int selectCountClientList(SqlSessionTemplate sqlSession, CheckClient ck) {
		int result = sqlSession.selectOne("Approval.selectCountClientList", ck);
		return result;
	}

}
