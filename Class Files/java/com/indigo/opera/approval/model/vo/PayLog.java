package com.indigo.opera.approval.model.vo;

import java.sql.Timestamp;

public class PayLog implements java.io.Serializable{
	private String loNo;
	private Timestamp loDate;
	private String lostatus;
	private String empNo;
	private String drNo;
	private String loSort;
	private String empName;
	private String empJobName;
	private String deptName;
	
	public PayLog() {}

	public PayLog(String loNo, Timestamp loDate, String lostatus, String empNo, String drNo, String loSort,
			String empName, String empJobName, String deptName) {
		super();
		this.loNo = loNo;
		this.loDate = loDate;
		this.lostatus = lostatus;
		this.empNo = empNo;
		this.drNo = drNo;
		this.loSort = loSort;
		this.empName = empName;
		this.empJobName = empJobName;
		this.deptName = deptName;
	}

	public String getLoNo() {
		return loNo;
	}

	public void setLoNo(String loNo) {
		this.loNo = loNo;
	}

	public Timestamp getLoDate() {
		return loDate;
	}

	public void setLoDate(Timestamp loDate) {
		this.loDate = loDate;
	}

	public String getLostatus() {
		return lostatus;
	}

	public void setLostatus(String lostatus) {
		this.lostatus = lostatus;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getDrNo() {
		return drNo;
	}

	public void setDrNo(String drNo) {
		this.drNo = drNo;
	}

	public String getLoSort() {
		return loSort;
	}

	public void setLoSort(String loSort) {
		this.loSort = loSort;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpJobName() {
		return empJobName;
	}

	public void setEmpJobName(String empJobName) {
		this.empJobName = empJobName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	@Override
	public String toString() {
		return "PayLog [loNo=" + loNo + ", loDate=" + loDate + ", lostatus=" + lostatus + ", empNo=" + empNo + ", drNo="
				+ drNo + ", loSort=" + loSort + ", empName=" + empName + ", empJobName=" + empJobName + ", deptName="
				+ deptName + "]\n";
	}
	
	
	
	
}
