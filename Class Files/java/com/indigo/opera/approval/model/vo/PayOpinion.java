package com.indigo.opera.approval.model.vo;

import java.sql.Timestamp;

public class PayOpinion implements java.io.Serializable{
	private String opNo;
	private String empNo;
	private Timestamp opDate;
	private String opContent;
	private String drNo;
	private String empName;
	private String jobName;
	
	
	public PayOpinion() {}


	public PayOpinion(String opNo, String empNo, Timestamp opDate, String opContent, String drNo, String empName,
			String jobName) {
		super();
		this.opNo = opNo;
		this.empNo = empNo;
		this.opDate = opDate;
		this.opContent = opContent;
		this.drNo = drNo;
		this.empName = empName;
		this.jobName = jobName;
	}


	public String getOpNo() {
		return opNo;
	}


	public void setOpNo(String opNo) {
		this.opNo = opNo;
	}


	public String getEmpNo() {
		return empNo;
	}


	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}


	public Timestamp getOpDate() {
		return opDate;
	}


	public void setOpDate(Timestamp opDate) {
		this.opDate = opDate;
	}


	public String getOpContent() {
		return opContent;
	}


	public void setOpContent(String opContent) {
		this.opContent = opContent;
	}


	public String getDrNo() {
		return drNo;
	}


	public void setDrNo(String drNo) {
		this.drNo = drNo;
	}


	public String getEmpName() {
		return empName;
	}


	public void setEmpName(String empName) {
		this.empName = empName;
	}


	public String getJobName() {
		return jobName;
	}


	public void setJobName(String jobName) {
		this.jobName = jobName;
	}


	@Override
	public String toString() {
		return "PayOpinion [opNo=" + opNo + ", empNo=" + empNo + ", opDate=" + opDate + ", opContent=" + opContent
				+ ", drNo=" + drNo + ", empName=" + empName + ", jobName=" + jobName + "]";
	}

	
	
}
