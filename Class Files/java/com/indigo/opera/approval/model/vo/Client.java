package com.indigo.opera.approval.model.vo;

public class Client implements java.io.Serializable{
	private String cliNo;
	private String cliName;
	private String cliAddress;
	private String empNo;
	private String cliLinumber;
	private String cliBankNumber;
	private String cliBankName;
	private String cliDepositName;
	
	public Client() {}

	public Client(String cliNo, String cliName, String cliAddress, String empNo, String cliLinumber,
			String cliBankNumber, String cliBankName, String cliDepositName) {
		super();
		this.cliNo = cliNo;
		this.cliName = cliName;
		this.cliAddress = cliAddress;
		this.empNo = empNo;
		this.cliLinumber = cliLinumber;
		this.cliBankNumber = cliBankNumber;
		this.cliBankName = cliBankName;
		this.cliDepositName = cliDepositName;
	}

	public String getCliNo() {
		return cliNo;
	}

	public void setCliNo(String cliNo) {
		this.cliNo = cliNo;
	}

	public String getCliName() {
		return cliName;
	}

	public void setCliName(String cliName) {
		this.cliName = cliName;
	}

	public String getCliAddress() {
		return cliAddress;
	}

	public void setCliAddress(String cliAddress) {
		this.cliAddress = cliAddress;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getCliLinumber() {
		return cliLinumber;
	}

	public void setCliLinumber(String cliLinumber) {
		this.cliLinumber = cliLinumber;
	}

	public String getCliBankNumber() {
		return cliBankNumber;
	}

	public void setCliBankNumber(String cliBankNumber) {
		this.cliBankNumber = cliBankNumber;
	}

	public String getCliBankName() {
		return cliBankName;
	}

	public void setCliBankName(String cliBankName) {
		this.cliBankName = cliBankName;
	}

	public String getCliDepositName() {
		return cliDepositName;
	}

	public void setCliDepositName(String cliDepositName) {
		this.cliDepositName = cliDepositName;
	}

	@Override
	public String toString() {
		return "Client [cliNo=" + cliNo + ", cliName=" + cliName + ", cliAddress=" + cliAddress + ", empNo=" + empNo
				+ ", cliLinumber=" + cliLinumber + ", cliBankNumber=" + cliBankNumber + ", cliBankName=" + cliBankName
				+ ", cliDepositName=" + cliDepositName + "]";
	}
	
}
