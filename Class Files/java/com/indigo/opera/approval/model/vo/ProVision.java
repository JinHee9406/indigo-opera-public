package com.indigo.opera.approval.model.vo;

public class ProVision implements java.io.Serializable{
	private String proNo;
	private String proCardDiv;
	private String proBankDiv;
	private String proCardNumber;
	
	public ProVision() {}

	public ProVision(String proNo, String proCardDiv, String proBankDiv, String proCardNumber) {
		super();
		this.proNo = proNo;
		this.proCardDiv = proCardDiv;
		this.proBankDiv = proBankDiv;
		this.proCardNumber = proCardNumber;
	}

	public String getProNo() {
		return proNo;
	}

	public void setProNo(String proNo) {
		this.proNo = proNo;
	}

	public String getProCardDiv() {
		return proCardDiv;
	}

	public void setProCardDiv(String proCardDiv) {
		this.proCardDiv = proCardDiv;
	}

	public String getProBankDiv() {
		return proBankDiv;
	}

	public void setProBankDiv(String proBankDiv) {
		this.proBankDiv = proBankDiv;
	}

	public String getProCardNumber() {
		return proCardNumber;
	}

	public void setProCardNumber(String proCardNumber) {
		this.proCardNumber = proCardNumber;
	}

	@Override
	public String toString() {
		return "ProVision [proNo=" + proNo + ", proCardDiv=" + proCardDiv + ", proBankDiv=" + proBankDiv
				+ ", proCardNumber=" + proCardNumber + "]";
	}
	
}
