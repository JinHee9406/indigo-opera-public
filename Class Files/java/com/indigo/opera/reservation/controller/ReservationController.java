package com.indigo.opera.reservation.controller;

import java.io.File;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.indigo.opera.common.CommonUtils;
import com.indigo.opera.common.Pagination;
import com.indigo.opera.fileList.model.service.FileListService;
import com.indigo.opera.fileList.model.vo.FileList;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.reservation.model.service.ReservationService;
import com.indigo.opera.reservation.model.vo.MeetRoom;
import com.indigo.opera.reservation.model.vo.Reservation;


@Controller
public class ReservationController {
	
	@Autowired
	private ReservationService resService;
	
	@Autowired
	private FileListService fileService;
	
	@RequestMapping("reservationMain.res")
	public String showReservationPage(Model model) {
		ArrayList<MeetRoom> roomList = null;	
		roomList = resService.showRoomList();
		
		if(roomList != null) {
			model.addAttribute("meetingRoom", roomList);
		}
		
		return "reservation/reservationMain";
	}
	
	@RequestMapping("loadReservationMain.res")
	public ModelAndView loadReservMain(String loginUser, String roomNo, ModelAndView mv) {
		
		Reservation load = new Reservation();
		
		load.setEmpNo(loginUser);
		load.setRoomNo(roomNo);
		
		ArrayList<Reservation> resList = resService.userListReservation(load);
		
		mv.addObject("Reservation", resList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("loadRoomInfo.res")
	public ModelAndView roomInfoLoad(String roomNumber, ModelAndView mv) {
		
		MeetRoom roomData = resService.roomInfoLoad(roomNumber);

		mv.addObject("Room", roomData);
		mv.setViewName("jsonView");
		return mv;
	}
	
	
	@RequestMapping("reservationAdd.res")
	public String addReservationPage(Model model) {
		
		ArrayList<MeetRoom> roomList = null;
		
		roomList = resService.showRoomList();
		
		if(roomList != null) {
			model.addAttribute("meetingRoom", roomList);
		}
		
		return "reservation/reservationAdd";
	}
	
	@RequestMapping("reservationPut.res")
	public String putReservation(Model model, Reservation reservation) {
		
		int result = resService.putReservation(reservation);
		
		String message = "에약을 신청했습니다!";
		String key = "addReservation";
		
		model.addAttribute("message", message);
		model.addAttribute("key", key);
		
		return "reservation/reservationResult";
	}
	
	@RequestMapping("reservationStatus.res")
	public ModelAndView reservationStatus(ModelAndView mv, String dayString) {
		
			ArrayList<Reservation> resList = resService.listReservation(dayString);
			
			mv.addObject("resList", resList);
			mv.setViewName("jsonView");
			return mv;
			
	}
	
	@RequestMapping("reservationDetail.res")
	public ModelAndView reservationDetail(ModelAndView mv, String resNo) {
		
			Reservation result = resService.reservationDetail(resNo);
			
			mv.addObject("detail", result);
			mv.setViewName("jsonView");
			return mv;
			
	}
	
	@RequestMapping("cencelReservation.res")
	public String removeReservation(Model model, String resNo) {
	
		int result = resService.removeReservation(resNo);
		
		String message = "예약이 취소되었습니다!";
		String key = "removeReservation";
		
		model.addAttribute("message", message);
		model.addAttribute("key", key);
		
		return "reservation/reservationResult";
	}

	
	
	@RequestMapping("reservationList.res")
	public String reservationListPage(Model model, String loginUser, String pageNum) {
		
		int currentPage;
		if(pageNum == null) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(pageNum);
		}
		
		int listCount = resService.getListSize(loginUser);
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		ArrayList<Reservation> resList = resService.reslistPage(loginUser, pi);
		
		int postNumber = pi.getCurFirstNum();
		
		for(int i=0; i < resList.size(); i++) {
			resList.get(i).setResDay(resList.get(i).getResDay().substring(0,10));
			resList.get(i).setListNum(postNumber--);
		}
		
		model.addAttribute("resList", resList);
		model.addAttribute("pi", pi);
		
		return "reservation/reservationList";
	}
	
	
	@RequestMapping("insertMeetRoom.res")
	public String addMeetingRoom(Model model, HttpServletRequest request, MultipartFile photo, MeetRoom room) {
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		
//		String root2 = request.getSession().getServletContext().getRealPath("messengerView");
//		메신저 폴더 저장용 경로
//		String filePath2 = root2 + "\\static\\resources\\uploadFiles";
		
		String originFileName = photo.getOriginalFilename();
		String ext = originFileName.substring(originFileName.lastIndexOf("."));
		String changeName = CommonUtils.getRandomString();
		
		try {
			photo.transferTo(new File(filePath + "\\" + changeName + ext));
			
			FileList imgFile = new FileList();
			imgFile.setFilePath("\\uploadFiles");
			imgFile.setOriginName(originFileName);
			imgFile.setChangeName(changeName + ext);
			imgFile.setFileSize(String.valueOf(photo.getSize()));
			
			int fileResult = fileService.addFile(imgFile);
			room.setRoomFile(String.valueOf(fileResult));
			
			int result = resService.addMeetRoom(room);
			
		} catch (Exception e) {
			new File(filePath + "\\" + changeName + ext).delete();
			
			String message = "회의실 추가 오류";
			model.addAttribute("message", message);
			return "reservation/reservationResult";
			
		}
		String message = "회의실이 추가되었습니다!";
		String key = "addmeetRoom";
		
		model.addAttribute("message", message);
		model.addAttribute("key", key);
		
		return "reservation/reservationResult";
	}
	

}
