package com.indigo.opera.reservation.model.dao;

import java.sql.Date;
import java.util.ArrayList;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.main.model.exception.mainException;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.reservation.model.exception.reservationException;
import com.indigo.opera.reservation.model.vo.MeetRoom;
import com.indigo.opera.reservation.model.vo.Reservation;

@Repository
public class ReservationDaoImpl implements ReservationDao{

	@Override
	public int addMeetRoom(SqlSessionTemplate sqlSession, MeetRoom room) throws reservationException {
		
		int result = sqlSession.insert("Reservation.addMeetRoom",room);
		
		if(result == 0) {
			throw new reservationException("회의실 추가오류");
		}
		
		return result;
	}

	@Override
	public ArrayList<Reservation> userListReservation(SqlSessionTemplate sqlSession, Reservation load) {
		 
		ArrayList<Reservation> result = (ArrayList) sqlSession.selectList("Reservation.userReservation", load);
		 
		 return result;
	}
	
	@Override
	public ArrayList<MeetRoom> showRoomList(SqlSessionTemplate sqlSession) {
		
		ArrayList<MeetRoom> resultList = (ArrayList) sqlSession.selectList("Reservation.selectMeetRoom");
		
		return resultList;
	}

	@Override
	public MeetRoom roomInfoLoad(SqlSessionTemplate sqlSession, String roomNumber) {
		
		MeetRoom roomInfo = sqlSession.selectOne("Reservation.selectRoomInfo", roomNumber);
		
		return roomInfo;
	}

	@Override
	public int putReservation(SqlSessionTemplate sqlSession, Reservation reservation) {
		
		int reservResult = sqlSession.insert("Reservation.putReservation", reservation);
		return reservResult;
	}

	@Override
	public ArrayList<Reservation> listReservation(SqlSessionTemplate sqlSession, String day) {
		
		 ArrayList<Reservation> result = (ArrayList) sqlSession.selectList("Reservation.listReservation", day);
		 
		 return result;
	}

	@Override
	public ArrayList<Reservation> reslistPage(SqlSessionTemplate sqlSession, String loginUser, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		
		RowBounds rowBounds = new RowBounds(offset,pi.getLimit());
		
		
		ArrayList<Reservation> result = (ArrayList) sqlSession.selectList("Reservation.reslistUser", loginUser, rowBounds);
		return result;
	}

	@Override
	public int getListSize(SqlSessionTemplate sqlSession, String loginUser) {
		
		int result = sqlSession.selectOne("Reservation.listSize", loginUser);
		
		return result;
	}

	@Override
	public Reservation reservationDetail(SqlSessionTemplate sqlSession, String resNo) {
		
		Reservation result = sqlSession.selectOne("Reservation.reservationDetail", resNo);
		
		return result;
	}

	@Override
	public int removeReservation(SqlSessionTemplate sqlSession, String resNo) {
		
		int result = sqlSession.delete("Reservation.removeReservation", resNo);
		
		return result;
	}

	@Override
	public Member getUserData(SqlSessionTemplate sqlSession, String loginUser) {

		Member result = sqlSession.selectOne("Member.selectMemberByNo", loginUser);
		
		return result;
	}
}
