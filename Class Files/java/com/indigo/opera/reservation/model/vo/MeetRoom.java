package com.indigo.opera.reservation.model.vo;

public class MeetRoom {
	private String roomNo;
	private String roomName;
	private String roomInfo;
	private String roomFile;
	
	public MeetRoom() {}

	public MeetRoom(String roomNo, String roomName, String roomInfo, String roomFile) {
		super();
		this.roomNo = roomNo;
		this.roomName = roomName;
		this.roomInfo = roomInfo;
		this.roomFile = roomFile;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomInfo() {
		return roomInfo;
	}

	public void setRoomInfo(String roomInfo) {
		this.roomInfo = roomInfo;
	}

	public String getRoomFile() {
		return roomFile;
	}

	public void setRoomFile(String roomFile) {
		this.roomFile = roomFile;
	}

	@Override
	public String toString() {
		return "MeetRoom [roomNo=" + roomNo + ", roomName=" + roomName + ", roomInfo=" + roomInfo + ", roomFile="
				+ roomFile + "]";
	}
	
	
}
