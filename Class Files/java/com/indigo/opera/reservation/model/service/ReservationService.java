package com.indigo.opera.reservation.model.service;

import java.sql.Date;
import java.util.ArrayList;

import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.reservation.model.exception.reservationException;
import com.indigo.opera.reservation.model.vo.MeetRoom;
import com.indigo.opera.reservation.model.vo.Reservation;

public interface ReservationService {

	int addMeetRoom(MeetRoom room) throws reservationException;

	ArrayList<MeetRoom> showRoomList();

	MeetRoom roomInfoLoad(String roomNumber);

	int putReservation(Reservation reservation);

	ArrayList<Reservation> listReservation(String day);

	ArrayList<Reservation> userListReservation(Reservation load);

	ArrayList<Reservation> reslistPage(String loginUser, PageInfo pi);

	int getListSize(String loginUser);

	Reservation reservationDetail(String resNo);

	int removeReservation(String resNo);

	Member getUserData(String loginUser);

}
