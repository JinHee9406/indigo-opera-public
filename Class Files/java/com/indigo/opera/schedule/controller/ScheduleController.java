package com.indigo.opera.schedule.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.indigo.opera.knowledge.model.exception.addKnowledgeException;
import com.indigo.opera.reservation.model.vo.MeetRoom;
import com.indigo.opera.schedule.model.exception.addScheduleException;
import com.indigo.opera.schedule.model.service.ScheduleService;
import com.indigo.opera.schedule.model.vo.DeptSchedule;
import com.indigo.opera.schedule.model.vo.PerSchedule;

@Controller
public class ScheduleController {
	
	@Autowired
	private ScheduleService schService;
	
	
	@RequestMapping("scheduleMain.sch")
	public String showSchedulePage() {
		return "schedule/scheduleMain";
	}
	@RequestMapping("insertSchedule.sch")
	public String showScheduleAddPage() {
		
		return "schedule/insertSchedule";
	}
	
	@RequestMapping("insertPerSchedule.sch")
	public String addPerScheduleResult(Model model, PerSchedule psche) {
		
		try {
			int result = schService.addPerSchedule(psche);
			
			String message = "일정추가를 완료했습니다!";
			String key = "addSchedule";
			
			model.addAttribute("message", message);
			model.addAttribute("key", key);
			return "knowledge/knowledgeResult";
		} catch (addScheduleException e) {
			e.printStackTrace();
			return " ";
		}
	}
	
	@RequestMapping("insertDeptSchedule.sch")
	public String addDeptScheduleResult(Model model, DeptSchedule dsche) {
		
		try {
			int result = schService.addDeptSchedul(dsche);
			
			String message = "일정추가를 완료했습니다!";
			String key = "addSchedule";
			
			model.addAttribute("message", message);
			model.addAttribute("key", key);
			return "knowledge/knowledgeResult";
		} catch (addScheduleException e) {
			e.printStackTrace();
			return " ";
		}
	}
	
	
	@RequestMapping("insertType.sch")
	public String showInsertTypePage() {
		return "schedule/insertType";
	}
	@RequestMapping("updateDeptType.sch")
	public String showUpdateDeptTypePage() {
		return "schedule/updateDeptType";
	}
	@RequestMapping("updatePersType.sch")
	public String showUpdatePersTypePage() {
		return "schedule/updatePersType";
	}
	
	
}
