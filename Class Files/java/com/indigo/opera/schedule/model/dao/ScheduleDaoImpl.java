package com.indigo.opera.schedule.model.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.schedule.model.exception.addScheduleException;
import com.indigo.opera.schedule.model.vo.DeptSchedule;
import com.indigo.opera.schedule.model.vo.PerSchedule;

@Repository
public class ScheduleDaoImpl implements ScheduleDao{

	@Override
	public int addPerSchedule(SqlSessionTemplate sqlSession, PerSchedule psche) throws addScheduleException {

		int result = sqlSession.insert("Schedule.insertPerSchedule", psche);
		
		if(result == 0) {
			throw new addScheduleException("일정추가 실패");
		}
		return result;
	}

	@Override
	public int addDeptSchedule(SqlSessionTemplate sqlSession, DeptSchedule dsche) throws addScheduleException {

		int result = sqlSession.insert("Schedule.insertDeptSchedule", dsche);
		
		if(result == 0) {
			throw new addScheduleException("일정추가 실패");
		}
		return result;
	}

}
