package com.indigo.opera.schedule.model.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.schedule.model.exception.addScheduleException;
import com.indigo.opera.schedule.model.vo.DeptSchedule;
import com.indigo.opera.schedule.model.vo.PerSchedule;

public interface ScheduleDao {

	int addPerSchedule(SqlSessionTemplate sqlSession, PerSchedule psche)  throws addScheduleException;

	int addDeptSchedule(SqlSessionTemplate sqlSession, DeptSchedule dsche)  throws addScheduleException;

}
