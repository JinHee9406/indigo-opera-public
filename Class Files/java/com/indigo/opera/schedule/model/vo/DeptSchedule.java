package com.indigo.opera.schedule.model.vo;

import java.io.Serializable;
import java.sql.Date;

public class DeptSchedule implements Serializable{
	
	private String deptCalNum;
	private Date dStartDate;
	private Date dEndDate;
	private String deptCalDetail;
	private String deptCalWriteDate;
	private String deptCalAlert;
	private String deptCalLocation;
	private String deptCalTitle;
	private String deptCode;
	private String empNo;
	
	public DeptSchedule() {}

	public DeptSchedule(String deptCalNum, Date dStartDate, Date dEndDate, String deptCalDetail,
			String deptCalWriteDate, String deptCalAlert, String deptCalLocation, String deptCalTitle, String deptCode,
			String empNo) {
		super();
		this.deptCalNum = deptCalNum;
		this.dStartDate = dStartDate;
		this.dEndDate = dEndDate;
		this.deptCalDetail = deptCalDetail;
		this.deptCalWriteDate = deptCalWriteDate;
		this.deptCalAlert = deptCalAlert;
		this.deptCalLocation = deptCalLocation;
		this.deptCalTitle = deptCalTitle;
		this.deptCode = deptCode;
		this.empNo = empNo;
	}

	public String getDeptCalNum() {
		return deptCalNum;
	}

	public void setDeptCalNum(String deptCalNum) {
		this.deptCalNum = deptCalNum;
	}

	public Date getdStartDate() {
		return dStartDate;
	}

	public void setdStartDate(Date dStartDate) {
		this.dStartDate = dStartDate;
	}

	public Date getdEndDate() {
		return dEndDate;
	}

	public void setdEndDate(Date dEndDate) {
		this.dEndDate = dEndDate;
	}

	public String getDeptCalDetail() {
		return deptCalDetail;
	}

	public void setDeptCalDetail(String deptCalDetail) {
		this.deptCalDetail = deptCalDetail;
	}

	public String getDeptCalWriteDate() {
		return deptCalWriteDate;
	}

	public void setDeptCalWriteDate(String deptCalWriteDate) {
		this.deptCalWriteDate = deptCalWriteDate;
	}

	public String getDeptCalAlert() {
		return deptCalAlert;
	}

	public void setDeptCalAlert(String deptCalAlert) {
		this.deptCalAlert = deptCalAlert;
	}

	public String getDeptCalLocation() {
		return deptCalLocation;
	}

	public void setDeptCalLocation(String deptCalLocation) {
		this.deptCalLocation = deptCalLocation;
	}

	public String getDeptCalTitle() {
		return deptCalTitle;
	}

	public void setDeptCalTitle(String deptCalTitle) {
		this.deptCalTitle = deptCalTitle;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	@Override
	public String toString() {
		return "DeptSchedule [deptCalNum=" + deptCalNum + ", dStartDate=" + dStartDate + ", dEndDate=" + dEndDate
				+ ", deptCalDetail=" + deptCalDetail + ", deptCalWriteDate=" + deptCalWriteDate + ", deptCalAlert="
				+ deptCalAlert + ", deptCalLocation=" + deptCalLocation + ", deptCalTitle=" + deptCalTitle
				+ ", deptCode=" + deptCode + ", empNo=" + empNo + "]";
	}

	

	
	

}
