package com.indigo.opera.schedule.model.vo;

import java.io.Serializable;
import java.sql.Date;

public class PerSchedule implements Serializable{
	
	private String perCalNo;
	private String empNo;
	private Date pStartDate;
	private Date pEndDate;
	private String perCalDetail;
	private Date perCalWriteDate;
	private String perCalAlert;
	private String perCalLocation;
	private String perCalTitle;
	private String perCalShere;
	
	public PerSchedule() {}

	public PerSchedule(String perCalNo, String empNo, Date pStartDate, Date pEndDate, String perCalDetail,
			Date perCalWriteDate, String perCalAlert, String perCalLocation, String perCalTitle, String perCalShere) {
		super();
		this.perCalNo = perCalNo;
		this.empNo = empNo;
		this.pStartDate = pStartDate;
		this.pEndDate = pEndDate;
		this.perCalDetail = perCalDetail;
		this.perCalWriteDate = perCalWriteDate;
		this.perCalAlert = perCalAlert;
		this.perCalLocation = perCalLocation;
		this.perCalTitle = perCalTitle;
		this.perCalShere = perCalShere;
	}

	public String getPerCalNo() {
		return perCalNo;
	}

	public void setPerCalNo(String perCalNo) {
		this.perCalNo = perCalNo;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public Date getpStartDate() {
		return pStartDate;
	}

	public void setpStartDate(Date pStartDate) {
		this.pStartDate = pStartDate;
	}

	public Date getpEndDate() {
		return pEndDate;
	}

	public void setpEndDate(Date pEndDate) {
		this.pEndDate = pEndDate;
	}

	public String getPerCalDetail() {
		return perCalDetail;
	}

	public void setPerCalDetail(String perCalDetail) {
		this.perCalDetail = perCalDetail;
	}

	public Date getPerCalWriteDate() {
		return perCalWriteDate;
	}

	public void setPerCalWriteDate(Date perCalWriteDate) {
		this.perCalWriteDate = perCalWriteDate;
	}

	public String getPerCalAlert() {
		return perCalAlert;
	}

	public void setPerCalAlert(String perCalAlert) {
		this.perCalAlert = perCalAlert;
	}

	public String getPerCalLocation() {
		return perCalLocation;
	}

	public void setPerCalLocation(String perCalLocation) {
		this.perCalLocation = perCalLocation;
	}

	public String getPerCalTitle() {
		return perCalTitle;
	}

	public void setPerCalTitle(String perCalTitle) {
		this.perCalTitle = perCalTitle;
	}

	public String getPerCalShere() {
		return perCalShere;
	}

	public void setPerCalShere(String perCalShere) {
		this.perCalShere = perCalShere;
	}

	@Override
	public String toString() {
		return "PerSchedule [perCalNo=" + perCalNo + ", empNo=" + empNo + ", pStartDate=" + pStartDate + ", pEndDate="
				+ pEndDate + ", perCalDetail=" + perCalDetail + ", perCalWriteDate=" + perCalWriteDate
				+ ", perCalAlert=" + perCalAlert + ", perCalLocation=" + perCalLocation + ", perCalTitle=" + perCalTitle
				+ ", perCalShere=" + perCalShere + "]";
	}

	
	
	
	
	
	
	

}
