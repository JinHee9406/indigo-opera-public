package com.indigo.opera.schedule.model.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.indigo.opera.schedule.model.exception.addScheduleException;
import com.indigo.opera.schedule.model.vo.DeptSchedule;
import com.indigo.opera.schedule.model.vo.PerSchedule;

public interface ScheduleService {

	int addPerSchedule(PerSchedule psche) throws addScheduleException;

	int addDeptSchedul(DeptSchedule dsche) throws addScheduleException;

}
